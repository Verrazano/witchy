uniform sampler2D texture;
uniform vec4 pColor;
uniform vec4 cColor;

void main()
{
	vec4 r;
	r.r = 109.0/255.0;
	r.g = 170.0/255.0;
	r.b = 44.0/255.0;

	vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

	if(pixel.r == cColor.r && pixel.g == cColor.g && pixel.b == cColor.b)
	{
		pixel = pColor;

	}

	gl_FragColor = pixel;

}
