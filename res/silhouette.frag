uniform sampler2D texture;

void main()
{
	vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

	if(pixel.a == 1.0)
	{
		pixel.r = 0.0;
		pixel.g = 0.0;
		pixel.b = 0.0;

	}

	gl_FragColor = pixel;

}
