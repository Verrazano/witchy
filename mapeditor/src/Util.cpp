#include "Util.h"
#include <sstream>
#include "imgui/imgui.h"

std::string toString(int i)
{
	std::stringstream ss;
	ss << i;
	std::string str;
	ss >> str;
	return str;
}

int toInt(std::string str)
{
	std::stringstream ss;
	ss << str;
	int i;
	ss >> i;
	return i;

}

std::string trim(std::string str)
{
    int first = str.find_first_not_of(' ');
    if (first == std::string::npos)
        return "";
    int last = str.find_last_not_of(' ');
    return str.substr(first, (last-first+1));

}

sf::IntRect getFrame(unsigned int frame,
	sf::Vector2u frameSize, sf::Vector2u size)
{
	sf::IntRect rect(0, 0, frameSize.x, frameSize.y);

	if(frameSize.x == 0 || frameSize.y == 0
		|| size.x == 0 || size.y == 0)
	{
		return rect;

	}

	unsigned fx = size.x/frameSize.x;
	unsigned fy = size.y/frameSize.y;

	unsigned int count = fx*fy;

	if(frame > count)
	{
		return rect;

	}

	unsigned int x = frame%fx;
	unsigned int y = frame/fx;

	x *= frameSize.x;
	y *= frameSize.y;

	rect.left = x;
	rect.top = y;

	return rect;

}

sf::IntRect tileFrame(unsigned int frame)
{
	return getFrame(frame, sf::Vector2u(8, 8), sf::Vector2u(80, 256));

}

void makeModal(std::string title, std::function<void(void)> content, bool* unopen)
{
  makeModal(title, content, [](){}, unopen);
}

void makeModal(std::string title, std::function<void(void)> content,
	std::function<void(void)> ok, bool* unopen)
{
  makeModal(title, content, ok, [](){}, unopen);

}

void makeModal(std::string title, std::function<void(void)> content,
	std::function<void(void)> ok, std::function<void(void)> cancel, bool* unopen)
{
	if(ImGui::BeginPopupModal(title.c_str(), unopen, ImGuiWindowFlags_AlwaysAutoResize))
	{
		content();

		ImGui::Separator();
		ImGui::NewLine();
		if(ImGui::Button("OK"))
		{
			ok();
			ImGui::CloseCurrentPopup();

		}

        ImGui::SameLine();
		ImGui::Indent(220);

        if(ImGui::Button("Cancel"))
        {
        	cancel();
        	ImGui::CloseCurrentPopup();

        }
        ImGui::EndPopup();

	}

}
