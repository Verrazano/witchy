#ifndef UTIL_H_
#define UTIL_H_

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <functional>

std::string toString(int i);

int toInt(std::string str);

std::string trim(std::string str);

sf::IntRect getFrame(unsigned int frame,
	sf::Vector2u frameSize, sf::Vector2u size);

sf::IntRect tileFrame(unsigned int frame);

void makeModal(std::string title, std::function<void(void)> content, bool* unopen);

void makeModal(std::string title, std::function<void(void)> content,
	std::function<void(void)>, bool* unopen);

void makeModal(std::string title, std::function<void(void)> content,
	std::function<void(void)> ok = [](){}, std::function<void(void)> cancel = [](){}, bool* unopen = NULL);

#endif /*UTIL*/
