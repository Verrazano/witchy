#pragma once
#include "imgui.h"

namespace ImGui {

IMGUI_API bool Tab(unsigned int index, const char* label, int* selected)
{
    ImGuiStyle& style = ImGui::GetStyle();
    ImVec2 itemSpacing = style.ItemSpacing;
    ImVec4 color = style.Colors[ImGuiCol_Button];
    ImVec4 colorActive = style.Colors[ImGuiCol_ButtonActive];
    ImVec4 colorHover = style.Colors[ImGuiCol_ButtonHovered];
    style.ItemSpacing.x = 1;

    if (index > 0)
        ImGui::SameLine();

    // push the style
    if (index == *selected)
    {
        style.Colors[ImGuiCol_Button] = colorActive;
        style.Colors[ImGuiCol_ButtonActive] = colorActive;
        style.Colors[ImGuiCol_ButtonHovered] = colorActive;
    }
    else
    {
        style.Colors[ImGuiCol_Button] = color;
        style.Colors[ImGuiCol_ButtonActive] = colorActive;
        style.Colors[ImGuiCol_ButtonHovered] = colorHover;
    }

    // Draw the button
    if (ImGui::Button(label))
        *selected = index;

    // Restore the style
    style.Colors[ImGuiCol_Button] = color;
    style.Colors[ImGuiCol_ButtonActive] = colorActive;
    style.Colors[ImGuiCol_ButtonHovered] = colorHover;
    style.ItemSpacing = itemSpacing;

    return *selected == index;
}

} //namespace imgui