#ifndef EDITORUI_H_
#define EDITORUI_H_

#include "imgui/imgui.h"
#include "imgui/imgui-SFML.h"
#include <SFML/Graphics.hpp>
#include <functional>

class EntityDef
{
public:
	EntityDef(std::string name, int frame);

	std::string name;
	int frame;

	std::vector<std::string> params;
	std::function<sf::RectangleShape(sf::Vector2f,std::fstream&,std::vector<std::string>&)> func;
  bool hasFunc;

};

class EditorUI
{
public:
	EditorUI(sf::Texture* worldTexture,
		sf::Texture* editTexture,
    sf::Texture* toolTexture);

	void loadPrefs();
	void savePrefs();

	void updateMap();
	void update();

	void setModalTemps(std::string modal);
	void handleModals();

  void setPath(std::string path);

  void setTool(int ntool, bool force = false);

  void makeButtonGroup(std::vector<int> tiles);

  bool inited;

	sf::Texture* worldTexture;
	sf::Texture* entityTexture;
  sf::Texture* toolTexture;

	std::vector<EntityDef> entities;

	int selectedTab;
	int selectedTile;
	int selectedEntity;

	int potentialIndex;

  std::string mapsDir;

	std::string openModal;
	int modalWidth;
	int modalHeight;
	char modalPath[256];
	std::vector<char*> modalParams;
  std::vector<std::string> entityParams;
  std::string entityName;
  int editingEntity;

  int currentIndex;
  std::vector<std::string> files;

  bool dropdownOpen;
  bool modalOpen;

	int width;
	int height;

	int tileCount;

	char path[256];

	bool hoveringMap;

	char defaultPath[256];

	//preferences
	float speed;
	int newWidth;
	int newHeight;
	int newTile;
	int windowWidth;
	int windowHeight;
	bool openOnStart;
  std::string lastOpen;

	sf::Vector2i mPos;

	sf::Vector2i gotoTile;
	bool prefOpen;

	int event;
	//0 is new event

  int entityEvent;

  int oldTab;
  int oldTool;
  int currentTool; //0 = paint, 1 = fill, 2 = select, 3 = erase, 4 entity-pencil
  sf::RectangleShape cursor;


};

#endif /*EDITORUI*/
