#include "EditorUI.h"
#include "imgui/tab.h"
#include "imgui/imgui_internal.h"
#include "Util.h"
#include <string.h>
#include <fstream>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

namespace ImGui
{
static auto vector_getter = [](void* vec, int idx, const char** out_text)
{
    auto& vector = *static_cast<std::vector<std::string>*>(vec);
    if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
    *out_text = vector.at(idx).c_str();
    return true;
};

bool Combo(const char* label, int* currIndex, std::vector<std::string>& values)
{
    if (values.empty()) { return false; }
    return Combo(label, currIndex, vector_getter,
        static_cast<void*>(&values), values.size());
}

bool ListBox(const char* label, int* currIndex, std::vector<std::string>& values)
{
    if (values.empty()) { return false; }
    return ListBox(label, currIndex, vector_getter,
        static_cast<void*>(&values), values.size());
}

}

EntityDef::EntityDef(std::string name, int frame) :
	name(name),
	frame(frame)
{
	func = [](sf::Vector2f, std::fstream&, std::vector<std::string>&){ return sf::RectangleShape(); };
  hasFunc = false;


}

EditorUI::EditorUI(sf::Texture* worldTexture,
	sf::Texture* entityTexture,
  sf::Texture* toolTexture) :
	worldTexture(worldTexture),
	entityTexture(entityTexture),
  toolTexture(toolTexture),
	selectedTab(0),
	selectedTile(1),
	selectedEntity(1)
{
  inited = false;

  oldTab = selectedTab;
	strcpy(path, "map.txt");
	strcpy(modalPath, "map.txt");
	speed = 10.0f;
	prefOpen = false;

  currentTool = oldTool =0;
  cursor = sf::RectangleShape(sf::Vector2f(32, 32));
  cursor.setOrigin(-16, -16);
  cursor.setTexture(toolTexture);
  setTool(0, true);

	newWidth = 25;
	newHeight = 25;
	newTile = 1;

	windowWidth = 800;
	windowHeight = 600;

	hoveringMap = false;

	tileCount = 96;
	openOnStart = false;

  mapsDir = "maps/";

  lastOpen = "map.txt";

	strcpy(defaultPath, "map.txt");

	loadPrefs();

	width = newWidth;
	height = newHeight;

	strcpy(path, "Untitled");
	strcpy(modalPath, "map.txt");

	if(openOnStart)
		strcpy(path, lastOpen.c_str());

	potentialIndex = -1;
  currentIndex = -1;

	entities.push_back(EntityDef("player", 7));
  entities.back().params.push_back("abilities:yes");
	entities.push_back(EntityDef("dahlia", 0));
  entities.push_back(EntityDef("bones", 1));
  entities.push_back(EntityDef("bluebell", 2));
	entities.push_back(EntityDef("nightshade", 10));
  entities.push_back(EntityDef("hive", 11));
  entities.push_back(EntityDef("honeycomb", 12));
  entities.push_back(EntityDef("furrow", 13));
	entities.push_back(EntityDef("daisy", 20));

  entities.push_back(EntityDef("map_teleport", 49));
  entities.back().params.push_back("map:current");
	entities.back().params.push_back("dest x");
	entities.back().params.push_back("dest y");
  entities.back().params.push_back("tag required:none");
	entities.back().func = [](sf::Vector2f pos, std::fstream& f, std::vector<std::string>& params)
	{

    std::string map;
		int x = 0;
		int y = 0;
    std::string tag;

    if(params.size() > 0)
    {
      map = params[0];
      x = toInt(params[1]);
      y = toInt(params[2]);
      tag = params[3];

    }
    else
    {
      f >> map >> x >> y >> tag;
      params.push_back(map);
      params.push_back(toString(x));
      params.push_back(toString(y));
      params.push_back(tag);

    }

    if(map == "current")
    {
      sf::RectangleShape endpoint(sf::Vector2f(20, 20));
      endpoint.setOrigin(10, 10);
      endpoint.setFillColor(sf::Color(0, 0, 100));
      endpoint.setPosition(x*34 + 17, y*34 + 17);
      return endpoint;

    }

    sf::RectangleShape r;
    return r;


	};
  entities.back().hasFunc = true;

	entities.push_back(EntityDef("zone", 19));
	entities.back().params.push_back("width:9");
	entities.back().params.push_back("height:9");
	entities.back().params.push_back("enter script:nothing");
	entities.back().params.push_back("leave script:nothing");
  entities.back().params.push_back("script data:nothing");

	static int nextZoneColor = 0;
	static std::vector<sf::Color> zoneColors = {sf::Color(255, 0, 0, 100),
		sf::Color(0, 255, 0, 100), sf::Color(0, 0, 255, 100), sf::Color(255, 255, 0, 100),
		sf::Color(255, 0, 255, 100), sf::Color(0, 255, 255, 100)};

	entities.back().func = [](sf::Vector2f pos, std::fstream& f, std::vector<std::string>& params)
	{
		int w = 9;
		int h = 9;

    std::string enterScript = "nothing";
    std::string leaveScript = "nothing";
    std::string scriptData = "nothing";

    if(params.size() > 0)
    {
      w = toInt(params[0]);
      h = toInt(params[1]);
      enterScript = params[2];
      leaveScript = params[3];
      scriptData = params[4];

    }
    else
    {
      f >> w >> h >> enterScript >> leaveScript;
      std::getline(f, scriptData);

      params.push_back(toString(w));
      params.push_back(toString(h));
      params.push_back(enterScript);
      params.push_back(leaveScript);
      params.push_back(scriptData);

    }

		sf::RectangleShape boundaries(sf::Vector2f(w*34, h*34));
		boundaries.setPosition(pos);
		boundaries.setFillColor(zoneColors[nextZoneColor%zoneColors.size()]);
		nextZoneColor++;

		return boundaries;

	};
  entities.back().hasFunc = true;

	entities.push_back(EntityDef("marker", 28));
	entities.back().params.push_back("tags");

	entities.push_back(EntityDef("sign", 9));
	entities.back().params.push_back("text");
  entities.back().func = [](sf::Vector2f pos, std::fstream& f, std::vector<std::string>& params)
  {
    std::string text = "";

    if(params.size() > 0)
    {
      text = params[0];

    }
    else
    {
      std::getline(f, text);
      params.push_back(text);

    }

    sf::RectangleShape r;
    return r;

  };
  entities.back().hasFunc = true;

  entities.push_back(EntityDef("statue", 53));
  entities.back().params.push_back("index:0");
	entities.back().params.push_back("text");
  entities.back().func = [](sf::Vector2f pos, std::fstream& f, std::vector<std::string>& params)
  {
    int index = 0;
    std::string text = "";

    if(params.size() > 0)
    {
      index = toInt(params[1]);
      text = params[0];

    }
    else
    {
      f >> index;
      params.push_back(toString(index));

      std::getline(f, text);
      params.push_back(text);

    }

    sf::RectangleShape r;
    return r;

  };
  entities.back().hasFunc = true;

	entities.push_back(EntityDef("puzzle_block", 17));
	entities.push_back(EntityDef("gate", 47));
	entities.push_back(EntityDef("rock", 27));
	entities.push_back(EntityDef("boulder", 36));
	entities.push_back(EntityDef("slime", 40));
	entities.push_back(EntityDef("slime_cube", 41));
	entities.push_back(EntityDef("turtle", 80));
	entities.push_back(EntityDef("crystal", 90));

	entities.push_back(EntityDef("small_chest", 38));
	entities.back().params.push_back("item");

	entities.push_back(EntityDef("big_chest", 39));
	entities.back().params.push_back("item");

	entities.push_back(EntityDef("locked_door", 35));

	entities.push_back(EntityDef("npc", 43));
	entities.back().params.push_back("name");

  entities.push_back(EntityDef("frog", 120));
  entities.push_back(EntityDef("fence", 44));
  entities.push_back(EntityDef("pot", 45));
  entities.push_back(EntityDef("torch", 54));
  entities.push_back(EntityDef("pressure_plate", 55));
  entities.push_back(EntityDef("gnome", 56));
  entities.push_back(EntityDef("mushroom", 57));
  entities.push_back(EntityDef("beetle", 58));
  entities.push_back(EntityDef("staff", 59));
  entities.push_back(EntityDef("bush", 63));

  entityEvent = 0;

}

void EditorUI::setPath(std::string npath)
{
  strcpy(path, npath.c_str());
  strcpy(modalPath, npath.c_str());
}

void EditorUI::loadPrefs()
{
	std::fstream f;
	f.open("editor.cfg");
	if(f)
	{
		f >> speed;
		f >> newWidth;
		f >> newHeight;
		f >> newTile;
		f >> windowWidth;
		f >> windowHeight;
    f >> mapsDir;
		f >> openOnStart;
    f >> lastOpen;

	}

}

void EditorUI::savePrefs()
{
	std::ofstream f;
	f.open("editor.cfg");
	if(f)
	{
		f << speed << "\n";
		f << newWidth << "\n";
		f << newHeight << "\n";
		f << newTile << "\n";
		f << windowWidth << "\n";
		f << windowHeight << "\n";

    f << mapsDir << "\n";
		f << openOnStart << "\n";
    f << lastOpen;

	}

}

void EditorUI::updateMap()
{
	hoveringMap = false;
	ImGuiIO& io = ImGui::GetIO();
	ImGui::SetNextWindowSize(sf::Vector2f(io.DisplaySize.x, io.DisplaySize.y));
	ImGui::SetNextWindowPos(sf::Vector2f(0, 0));

	ImGui::Begin("clickablearea", NULL, sf::Vector2f(800, 600), 0.0f,
		ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoInputs);

	ImGui::End();

}

void EditorUI::update()
{
  if(!inited)
  {
    std::cout << "init\n";
    inited = true;
    ImGui::SetNextTreeNodeOpen(true);
    //ImGui::GetStateStorage()->SetInt(ImGui::GetID("Basic"), 1);

  }


	updateMap();

	event = -1;

  dropdownOpen = false;

	if(ImGui::BeginMainMenuBar())
	{
		if(ImGui::BeginMenu("File"))
		{
      dropdownOpen = true;
			if(ImGui::MenuItem("New", "Ctrl+N"))
				openModal = "New";

			if(ImGui::MenuItem("Open", "Ctrl+O"))
				openModal = "Open";

			if(ImGui::MenuItem("Save", "Ctrl+S"))
			{
				if(std::string(path) == "Untitled")
				{
					openModal = "Save As";
				}
				else
				{
					event = 2;

				}

			}


			if(ImGui::MenuItem("Save As.."))
				openModal = "Save As";

			ImGui::EndMenu();

		}

		if(ImGui::BeginMenu("Edit"))
		{
      dropdownOpen = true;

			ImGui::MenuItem("Undo", "Ctrl+Z", false, false);

			if(ImGui::MenuItem("Resize"))
				openModal = "Resize";

			ImGui::EndMenu();
		}

		if(ImGui::BeginMenu("View"))
		{
      dropdownOpen = true;

			if(ImGui::MenuItem("Go to", NULL, false, false))
				openModal = "Go to";

			if(ImGui::MenuItem("Preferences"))
				prefOpen = true;

			ImGui::EndMenu();

		}

		std::string title = std::string(path) + " - " + toString(width) + " x " + toString(height) + " - (" + toString(mPos.x) + ", " + toString(mPos.y) + ")";
		ImGui::MenuItem(title.c_str(), NULL, false, false);

		ImGui::EndMainMenuBar();
	}

	if(openModal != "")
	{
		setModalTemps(openModal);
		ImGui::OpenPopup(openModal.c_str());
		openModal = "";

	}

	handleModals();

	ImGui::Begin("Tools");

 	if(ImGui::Tab(0, "Tiles", &selectedTab))
  {
    setTool(currentTool);

  }

 	if(ImGui::Tab(1, "Entities", &selectedTab))
  {
    setTool(currentTool);

  }

 	ImGui::Tab(2, "Tools", &selectedTab);

	ImGuiStyle& style = ImGui::GetStyle();

	sf::Vector2f buttonSize(32, 32);

	int across = (ImGui::GetWindowContentRegionWidth()/(buttonSize.x + 4));
	if(across <= 0)
		across = 1;

 	if(selectedTab == 0)
 	{
    if(ImGui::CollapsingHeader("Basic"))
    {
      makeButtonGroup({0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 17, 18, 22, 36, 37, 40, 50, 51, 52, 53, 74, 75, 76, 90, 96, 97, 98});

    }

    if(ImGui::CollapsingHeader("Building"))
    {
      makeButtonGroup({12, 13, 14, 15, 16, 19, 20, 21, 23, 24, 27, 28, 29, 30, 32, 33, 34, 35, 41, 42, 43, 44, 45, 46, 47, 48, 49, 54, 55, 77, 78, 91, 92, 93, 94, 95});

    }

    if(ImGui::CollapsingHeader("Dungeon"))
    {
      makeButtonGroup({25, 26, 27, 38, 39, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89});

    }

 	}
 	else if(selectedTab == 1)
 	{
 		float originalYSpacing = style.ItemSpacing.y;
 		style.ItemSpacing.y = 0.0f;

 		int across = (ImGui::GetWindowContentRegionWidth()/(buttonSize.x + 4));
 		if(across <= 0)
 			across = 1;

 		for(unsigned int i = 0; i < entities.size();)
 		{

 			for(unsigned int j = 0; j < across && i < entities.size(); j++, i++)
 			{
 				if(j > 0)
 					ImGui::SameLine(0.0f, 0.0f);

 				ImGui::PushID(i);
 				if(selectedEntity == i)
 				{
 					ImGui::ImageButton(*entityTexture, buttonSize, tileFrame(entities[i].frame), 2, sf::Color::Transparent,
 						sf::Color(128, 128, 128));

 				}
 				else
 				{
 					if(ImGui::ImageButton(*entityTexture, buttonSize, tileFrame(entities[i].frame), 2))
 					{
 						selectedEntity = i;

 					}

 				}

				if(ImGui::IsItemHovered())
				{
					ImGui::SetTooltip(entities[i].name.c_str());

				}

 				ImGui::PopID();

 			}

 		}

 		style.ItemSpacing.y = originalYSpacing;

 	}
  else if(selectedTab == 2)
  {
    ImGui::PushID(0);
    //paint brush tool
    if(ImGui::ImageButton(*toolTexture, buttonSize, sf::IntRect(16, 16, 16, 16), 2))
    {
      setTool(0, true);

    }

    if(ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("paint brush (P)");

    }

		ImGui::PopID();

    ImGui::PushID(1); //fill tool
    if(ImGui::ImageButton(*toolTexture, buttonSize, sf::IntRect(16, 0, 16, 16), 2))
    {
      setTool(1, true);

    }

    if(ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("fill bucket (B)");
    }


    ImGui::PopID();

    ImGui::PushID(2); //select tool
    if(ImGui::ImageButton(*toolTexture, buttonSize, sf::IntRect(0, 16, 16, 16), 2))
    {
      setTool(2, true);

    }

    if(ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("selection (R)");
    }

    ImGui::PopID();

    ImGui::PushID(3); //erase tool
    if(ImGui::ImageButton(*toolTexture, buttonSize, sf::IntRect(0, 32, 16, 16), 2))
    {
      setTool(3, true);

    }

    if(ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("erase (E)");
    }

    ImGui::PopID();

  }

    ImGui::End();

    if(prefOpen)
    {
		ImGui::Begin("Preferences", &prefOpen, sf::Vector2f(100, 100), 0.8, ImGuiWindowFlags_AlwaysAutoResize);
    	ImGui::SliderFloat("scroll speed", &speed, 5.0f, 20.0f);
		if(ImGui::InputInt("new map width", &newWidth, 1, 10))
		{
			if(newWidth < 9)
				newWidth = 9;

		}

		if(ImGui::InputInt("new map height", &newHeight, 1, 10))
		{
			if(newHeight < 9)
				newHeight = 9;

		}

		ImGui::Image(*worldTexture, buttonSize, sf::FloatRect(tileFrame(newTile)));
		ImGui::SameLine();
		if(ImGui::InputInt("new map tile", &newTile, 1, 1))
		{
			if(newTile < 0)
				newTile = 0;
			if(newTile >= tileCount)
				newTile = tileCount-1;

		}

		if(ImGui::InputInt("window width", &windowWidth, 10, 100))
		{
			if(windowWidth < 400)
				windowWidth = 400;

		}

		if(ImGui::InputInt("window height", &windowHeight, 10, 100))
		{
			if(windowHeight < 300)
				windowHeight = 300;

		}

		ImGui::Checkbox("open default path on start", &openOnStart);

    	ImGui::End();
    }

    //used to decide if we can interact with the sfml part of the ui
    ImGuiContext& g = *GImGui;
    for (int i = g.Windows.Size-1; i >= 0; i--)
    {
        ImGuiWindow* window = g.Windows[i];
        if (!window->Active)
            continue;

        ImRect bb(window->WindowRectClipped.Min - g.Style.TouchExtraPadding,
        	window->WindowRectClipped.Max + g.Style.TouchExtraPadding);
        if (std::string(window->Name) != "Debug" && bb.Contains(g.IO.MousePos))
       	{
       		hoveringMap = std::string(window->Name) == "clickablearea";
            break;

        }

    }

}

void EditorUI::setModalTemps(std::string modal)
{
	if(modal == "New")
	{
		modalWidth = newWidth;
		modalHeight = newHeight;

	}
	else if(modal == "Resize")
	{
		modalWidth = width;
		modalHeight = height;

	}
	else if(modal == "Save As")
	{
		strcpy(modalPath, "map.txt");


	}
  else if(modal == "Open")
  {
    strcpy(modalPath, path);
    currentIndex = -1;
    files.clear();
    for (auto & p : fs::directory_iterator(mapsDir))
    {
      files.push_back(p.path());

    }

  }
	else if(modal == "Parameters")
	{
		for(int i = 0; i < modalParams.size(); i++)
		{
			delete[] modalParams[i];

		}

		modalParams.clear();

    editingEntity = selectedEntity;
    bool editing = false;
    if(!entityParams.empty())
    {
    //  std::cout << "entityName: " << entityName << "\n";
      for(int i = 0; i < entities.size(); i++)
      {
        if(entities[i].name == entityName)
        {
        //  std::cout << "found: " << i << "\n";
          editingEntity = i;
          editing = true;
          break;

        }
      }

    }

    EntityDef& e = entities[editingEntity];
    for(int i = 0; i < e.params.size(); i++)
    {
      std::string defaultParam;
      if(editing)
      {
        defaultParam = entityParams[i];
      //  std::cout << "param: " << defaultParam << "\n";

      }
      else if(e.params[i].find(":") != std::string::npos)
      {
        defaultParam = e.params[i];
        defaultParam = defaultParam.substr(defaultParam.find(":")+1);

      }

      char* c = new char[256];
      strcpy(c, defaultParam.c_str());
      modalParams.push_back(c);
      //std::cout << "modalParam: " << modalParams[modalParams.size()-1]<< "\n";

    }

	}

  modalOpen = true;

}

void EditorUI::handleModals()
{
	makeModal("New", [&]() //0
	{
		if(ImGui::InputInt("width", &modalWidth, 1, 10))
			if(modalWidth < 9)
				modalWidth = 9;
		if(ImGui::InputInt("height", &modalHeight, 1, 10))
			if(modalHeight < 9)
				modalHeight = 9;
	},
	[&]() //ok clicked
	{
		strcpy(path, "Untitled");
		width = modalWidth;
		height = modalHeight;
		event = 0;

	}, &modalOpen);

	makeModal("Open", [&]() //1
	{
		ImGui::InputText("Path", modalPath, 256);

    //std::vector<std::string> files = {"thing", "thing2", "thing3"};
    if(ImGui::ListBox("Folders", &currentIndex, files))
    {
      std::string p = files[currentIndex];
      strcpy(modalPath, p.c_str());

    }


	},
	[&]() //ok clicked
	{
		event = 1;
		strcpy(path, modalPath);
    lastOpen = std::string(path);

	}, &modalOpen);

	makeModal("Save As", [&]() //2
	{
		ImGui::InputText("Path", modalPath, 256);

	},
	[&]() //ok clicked
	{
		event = 2;
		strcpy(path, modalPath);
    lastOpen = std::string(path);

	}, &modalOpen);

	makeModal("Resize", [&]() //3
	{
		if(ImGui::InputInt("width", &modalWidth, 1, 10))
			if(modalWidth < 9)
				modalWidth = 9;
		if(ImGui::InputInt("height", &modalHeight, 1, 10))
			if(modalHeight < 9)
				modalHeight = 9;
	},
	[&]() //ok clicked
	{
		event = 3;

	}, &modalOpen);

	makeModal("Go to", [&]() //4
	{
		if(ImGui::InputInt("x", &gotoTile.x, 1, 10))
		{
			if(gotoTile.x < 0)
				gotoTile.x = 0;
			if(gotoTile.x > width)
				gotoTile.x = width;

		}

		if(ImGui::InputInt("y", &gotoTile.y, 1, 10))
		{
			if(gotoTile.y < 0)
				gotoTile.y = 0;
			if(gotoTile.y > width)
				gotoTile.y = width;

		}

	}, &modalOpen);

	makeModal("Parameters", [&]() //5
	{
		EntityDef& e = entities[editingEntity];
		ImGui::Text(e.name.c_str());

		for(int i = 0; i < e.params.size(); i++)
		{
      std::string param = e.params[i];
      if(param.find(":") != std::string::npos)
      {
        param = param.substr(0, param.find(":"));

      }

			ImGui::InputText(param.c_str(), modalParams[i], 256);

		}

	},
	[&]()
	{
		event = 5;

	}, &modalOpen);

}

void EditorUI::setTool(int ntool, bool force)
{
  if(!force)
  {
    if(selectedTab == oldTab)
      return;

  }

  oldTab = selectedTab;

  //0 = paint, 1 = fill, 2 = select, 3 = erase, 4 pencil
  currentTool = ntool;
  if(currentTool == 0) //paint
  {
    oldTool = currentTool;
    if(selectedTab == 1)
    {
      setTool(4, true);
      return;

    }

    cursor.setTextureRect(sf::IntRect(16, 16, 16, 16));

  }
  else if(currentTool == 1) //fill
  {
    oldTool = currentTool;
    if(selectedTab == 1)
    {
      setTool(4, true);
      return;

    }

    cursor.setTextureRect(sf::IntRect(16, 0, 16, 16));

  }
  else if(currentTool == 2) //select
  {
    cursor.setTextureRect(sf::IntRect(0, 16, 16, 16));

  }
  else if(currentTool == 3) //erase
  {
    cursor.setTextureRect(sf::IntRect(0, 32, 16, 16));

  }
  else if(currentTool == 4) //pencil
  {
    if(selectedTab == 0)
    {
      int temp = oldTool;
      oldTool = currentTool;
      setTool(temp, true);
      return;

    }

    cursor.setTextureRect(sf::IntRect(0, 0, 16, 16));

  }

}

void EditorUI::makeButtonGroup(std::vector<int> tiles)
{
  sf::Vector2f buttonSize(32, 32);

  int across = ((ImGui::GetWindowContentRegionWidth())/(buttonSize.x + 4));
  if(across <= 0)
    across = 1;

  ImGuiStyle& style = ImGui::GetStyle();

  float originalYSpacing = style.ItemSpacing.y;
  style.ItemSpacing.y = 0.0f;

  for(unsigned int i = 0; i < tiles.size();)
  {
    for(unsigned int j = 0; j < across && i < tiles.size(); j++, i++)
    {
      if(j > 0)
        ImGui::SameLine(0.0f, 0.0f);

      int t = tiles[i];

      ImGui::PushID(t);
      if(selectedTile == t)
      {
        ImGui::ImageButton(*worldTexture, buttonSize, tileFrame(t), 2, sf::Color::Transparent,
          sf::Color(128, 128, 128));

      }
      else
      {
        if(ImGui::ImageButton(*worldTexture, buttonSize, tileFrame(t), 2))
        {
          selectedTile = t;

        }

      }

      if(ImGui::IsItemHovered())
      {
        ImGui::SetTooltip(toString(t).c_str());

      }

      ImGui::PopID();

    }

  }
  style.ItemSpacing.y = originalYSpacing;

}
