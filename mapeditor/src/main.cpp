#include <SFML/Graphics.hpp>
#include "EditorUI.h"
#include "Util.h"
#include <fstream>
#include <string.h>

sf::Texture worldTexture;
sf::Texture entityTexture;
sf::Texture toolTexture;

class Tile
{
public:
	Tile(){}
	Tile(unsigned int frame, sf::Vector2f pos) :
		frame(frame)
	{
		sprite.setSize(sf::Vector2f(32, 32));
		sprite.setPosition(pos);
		sprite.setOutlineThickness(1);
		sprite.setOutlineColor(sf::Color::Black);
		sprite.setTexture(&worldTexture);
		sprite.setTextureRect(tileFrame(frame));
		this->frame = frame;

	}

	void update(sf::Vector2f mpos, bool canHover)
	{
		hovered = false;
		sprite.setOutlineColor(sf::Color::Black);
		if(canHover && sprite.getGlobalBounds().contains(mpos))
		{
			sprite.setOutlineColor(sf::Color::Yellow);
			hovered = true;

		}

	}

	bool hovered;
	sf::RectangleShape sprite;
	unsigned int frame;

};

class Entity
{
public:
	Entity(){}
	Entity(std::string name,
		unsigned int frame, sf::Vector2f pos) :
		frame(frame),
		name(name)
	{
		sprite.setSize(sf::Vector2f(32, 32));
		sprite.setPosition(pos);
		sprite.setTexture(&entityTexture);
		sprite.setTextureRect(tileFrame(frame));
		this->frame = frame;

	}

	sf::RectangleShape sprite;
	unsigned int frame;
	std::string name;
	std::vector<std::string> params;
	sf::RectangleShape special;

};

std::vector<Tile> newMap(int width, int height, int tile)
{
	std::vector<Tile> tiles;
	for(int j = 0; j < height; j++)
	{
		for(int i = 0; i < width; i++)
		{
			sf::Vector2f p(i*34.0f, j*34.0f);
			Tile t(tile, p);
			tiles.push_back(t);

		}

	}
	return tiles;

}

std::vector<Tile> resize(std::vector<Tile> tiles, int height, int width,
	int newWidth, int newHeight, int tile)
{
	std::vector<Tile> n;

	int c = 0;
	for(int j = 0; j < newHeight; j++)
	{
		for(int i = 0; i < newWidth; i++)
		{
			sf::Vector2f p(i*34.0f, j*34.0f);
			bool found = false;
			for(int k = 0; k < tiles.size(); k++)
			{
				if(tiles[k].sprite.getPosition() == p)
				{
					n.push_back(tiles[k]);
					found  = true;
					break;

				}

			}

			if(!found)
			{
				Tile t(tile, p);
				n.push_back(t);

			}

		}

	}

	return n;

}

void save(std::string path,
	int width,
	int height,
	std::vector<Tile>& tiles,
	std::vector<Entity>& entities)
{
	std::ofstream f;
	f.open(path.c_str());

	if(f)
	{
		f << width << "\n";
		f << height << "\n";

		for(int i = 0; i < tiles.size()-1; i++)
		{
			f << tiles[i].frame << " ";

		}

		f << tiles[tiles.size()-1].frame << "\n";

		f << entities.size() << "\n";
		for(int i = 0; i < entities.size(); i++)
		{
			Entity& e = entities[i];
			sf::Vector2f pos = e.sprite.getPosition()/34.0f;
			int x = pos.x;
			int y = pos.y;
			f << x << " " << y << " " << e.name;
			for(int j = 0; j < e.params.size(); j++)
			{
				f << " " << trim(e.params[j]);

			}

			f << "\n";

		}

	}

}

void load(std::string path,
	int& width,
	int& height,
	std::vector<Tile>& tiles,
	std::vector<Entity>& entities,
	EditorUI& ui)
{
	std::fstream f;
	f.open(path.c_str());

	if(f)
	{
		int w = 0;
		int h = 0;
		f >> w;
		f >> h;

		tiles.clear();
		entities.clear();

		width = w;
		height = h;

		for(unsigned int i = 0; i < height; i++)
		{
			for(unsigned int j = 0; j < width; j++)
			{
				int frame = 0;
				f >> frame;

				sf::Vector2f p(j*34.0f, i*34.0f);
				Tile t(frame, p);
				tiles.push_back(t);

			}

		}

		int count = 0;
		f >> count;
		for(int i = 0; i < count; i++)
		{
			int x;
			int y;
			std::string name;
			f >> x >> y >> name;
			sf::Vector2f pos(x*34, y*34);

			for(int j = 0; j < ui.entities.size(); j++)
			{
				EntityDef& def = ui.entities[j];
				if(def.name == name)
				{
					std::vector<std::string> params;

					Entity e(def.name, def.frame, pos);
          if(!def.hasFunc)
          {
            for(int i = 0; i < def.params.size(); i++)
            {
              std::string p;
              f >> p;
              params.push_back(p);

            }
          }
          else
          {
  					e.special = def.func(pos, f, params);

          }

          e.params = params;
					entities.push_back(e);

					break;

				}

			}

		}

	}

}

void setEntity(std::vector<Tile>& tiles,
	std::vector<Entity>& entities, int index,
	EntityDef& def, std::vector<char*> params)
{
	std::vector<std::string> v;
	for(int i = 0; i < params.size(); i++)
	{
		v.push_back(params[i]);

	}

	bool found = false;
  std::fstream f;
	for(int j = 0; j < entities.size(); j++)
	{
		if(entities[j].sprite.getPosition() == tiles[index].sprite.getPosition())
		{
			sf::Vector2f pos = tiles[index].sprite.getPosition();
			Entity e(def.name, def.frame, pos);
			e.params = v;
      if(def.hasFunc)
      {
			     e.special = def.func(pos, f, v);
      }
			entities[j] = e;
			found = true;
			break;

		}

	}

	if(!found)
	{
		sf::Vector2f pos = tiles[index].sprite.getPosition();
		Entity e(def.name, def.frame, pos);
		e.params = v;
    if(def.hasFunc)
    {
  		e.special = def.func(pos, f, v);

    }
		entities.push_back(e);

	}
}

void setEntity(std::vector<Tile>& tiles,
	std::vector<Entity>& entities, int index,
	EntityDef& def)
{
	std::vector<char*> v;
	setEntity(tiles, entities, index, def, v);

}

int main(int argc, char** argv)
{
	EditorUI ui(&worldTexture, &entityTexture, &toolTexture);
	sf::Vector2f winSize(ui.windowWidth, ui.windowHeight);
	sf::RenderWindow window(sf::VideoMode(winSize.x, winSize.y), "Witchy - Map Editor");
	//window.setFramerateLimit(60);
	ImGui::SFML::Init(window);

  //window.setMouseCursorVisible(false);

  std::string argpath;
  if(argc > 1)
  {
    argpath = argv[1];

  }

	sf::RectangleShape camera(sf::Vector2f(winSize.x, winSize.y));
	camera.setOrigin(winSize.x/2, winSize.y/2);

	sf::RectangleShape guiView(camera);
	camera.setPosition(winSize.x/2 - 60, winSize.y/2 - 60);

  sf::RectangleShape mouseView(camera);
  mouseView.setPosition(0, 0);

	worldTexture.loadFromFile("../res/world.png");
	entityTexture.loadFromFile("../res/entities.png");
  toolTexture.loadFromFile("tools.png");

	std::vector<Tile> tiles;
	std::vector<Entity> entities;

  if(argpath != "")
  {
    argpath = ui.mapsDir + argpath;
    strcpy(ui.path, argpath.c_str());
    load(argpath, ui.width, ui.height, tiles, entities, ui);

  }
	else if(ui.openOnStart)
  {
		load(ui.path, ui.width, ui.height, tiles, entities, ui);

  }
	else
  {
		tiles = newMap(ui.newWidth, ui.newHeight, ui.newTile);

  }

	bool hasFocus = true;
	sf::Clock deltaClock;

  sf::Vector2f wheelOffset;

  bool keepUnhoverable = false;

  bool leftClick = false;

  bool selecting = false;
  sf::RectangleShape selection;
  selection.setFillColor(sf::Color(255, 255, 153, 127));

  std::vector<Tile> clipBoardTiles;
	std::vector<Entity> clipBoardEntities;
  int clipWidth = 0;

	while(window.isOpen())
	{
    wheelOffset = sf::Vector2f(0, 0);

		sf::Event event;
		while(window.pollEvent(event))
		{
			ImGui::SFML::ProcessEvent(event);
			if(event.type == sf::Event::Closed)
			{
				window.close();

			}
			else if(event.type == sf::Event::Resized)
			{
				sf::Vector2f nSize(event.size.width, event.size.height);
				camera.setSize(nSize);
				camera.setOrigin(nSize/2.0f);

				guiView.setSize(nSize);
				guiView.setOrigin(nSize/2.0f);

        mouseView.setSize(nSize);

				ui.windowWidth = nSize.x;
				ui.windowHeight = nSize.y;

			}
			else if(event.type == sf::Event::GainedFocus)
			{
				hasFocus = true;

			}
			else if(event.type == sf::Event::LostFocus)
			{
				hasFocus = false;

			}
      else if(event.type == sf::Event::KeyPressed)
      {
        //no shortcuts when dropdown open
        if(!ui.modalOpen)
        {
          if(event.key.code == sf::Keyboard::P) //paint bursh
          {
            ui.selectedTab = 0;
            ui.setTool(0, true);

          }
          else if(event.key.code == sf::Keyboard::B) //fill bucket
          {
            ui.selectedTab = 0;
            ui.setTool(1, true);

          }
          else if(event.key.code == sf::Keyboard::R) //selection
          {
            if(ui.selectedTab == 2)
              ui.selectedTab = 0;
            ui.setTool(2, true);

          }
          else if(event.key.code == sf::Keyboard::E) //erase
          {
            if(ui.selectedTab == 2)
              ui.selectedTab = 0;
            ui.setTool(3, true);

          }
          else if(event.key.code == sf::Keyboard::N) //pencil
          {
            ui.selectedTab = 1;
            ui.setTool(4, false);
          }
          else if(event.key.code == sf::Keyboard::Num1)
          {
            ui.selectedTab = 0;
            ui.setTool(ui.currentTool);

          }
          else if(event.key.code == sf::Keyboard::Num2)
          {
            ui.selectedTab = 1;
            ui.setTool(ui.currentTool);

          }
          else if(event.key.code == sf::Keyboard::Num3)
          {
            ui.selectedTab = 2;
            ui.setTool(ui.currentTool);

          }

        }

      }

		}

    bool oldClick = leftClick;
		leftClick = hasFocus && sf::Mouse::isButtonPressed(sf::Mouse::Left);
    bool justLeftClick = !oldClick && leftClick;
    bool justLeftRelease = oldClick && !leftClick;
		bool rightClick = hasFocus && sf::Mouse::isButtonPressed(sf::Mouse::Right);

    bool copyKey = hasFocus && sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::C);
    bool pasteKey = hasFocus && sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::V);

    if(leftClick && ui.dropdownOpen)
    {
      keepUnhoverable = true;

    }

    if(!leftClick && keepUnhoverable)
    {
      keepUnhoverable = false;

    }

		if(hasFocus)
		{
			float speed = ui.speed;
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				camera.move(0, -speed);

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				camera.move(0, speed);

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				camera.move(-speed, 0);

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				camera.move(speed, 0);

      camera.move(wheelOffset.x*speed, wheelOffset.y*speed);

		}

		ImGui::SFML::Update(deltaClock.restart());
		ui.update();

		if(ui.event == 0) //make a new map
		{
			tiles.clear();
			entities.clear();
			tiles = newMap(ui.width, ui.height, ui.newTile);

		}
		else if(ui.event == 1) //open
		{
			std::string path(ui.path);
			load(path, ui.width, ui.height, tiles, entities, ui);

		}
		else if(ui.event == 2) //save/saveas
		{
			std::string path(ui.path);
			save(path, ui.width, ui.height, tiles, entities);

		}
		else if(ui.event == 3) //resize the map
		{
			tiles = resize(tiles, ui.height, ui.width, ui.modalWidth, ui.modalHeight, ui.newTile);
			ui.width = ui.modalWidth;
			ui.height = ui.modalHeight;

		}
		else if(ui.event == 5)
		{
			setEntity(tiles, entities, ui.potentialIndex,
				ui.entities[ui.selectedEntity], ui.modalParams);

		}

		sf::View gameView(camera.getGlobalBounds());
    sf::Vector2i windowMouse = sf::Mouse::getPosition(window);
		sf::Vector2f mPos = window.mapPixelToCoords(windowMouse, gameView);
		ui.mPos = sf::Vector2i(mPos/34.0f);
		window.setView(gameView);

		window.clear(sf::Color::White);

    bool canHover = ui.hoveringMap && !ui.dropdownOpen && !keepUnhoverable;

    if(ui.currentTool == 2) // select tiles
    {
      if(justLeftClick)
      {
        selecting = true;
        selection.setPosition(mPos);
        selection.setSize(sf::Vector2f(0, 0));

      }

      if(selecting && leftClick)
      {
        sf::Vector2f selSize = mPos - selection.getPosition();
        selection.setSize(selSize);

      }

      if(selecting && justLeftRelease)
      {
        selecting = false;

      }

      if(copyKey)
      {
        clipBoardTiles.clear();
        clipBoardEntities.clear();

        float startY = -1.0f;
        clipWidth = 0;
        bool doneClipWidth = false;

        for(int i = 0; i < tiles.size(); i++)
        {
          if(tiles[i].sprite.getGlobalBounds().intersects(selection.getGlobalBounds()))
          {
            if(startY == -1.0f)
            {
              startY = tiles[i].sprite.getPosition().y;
              clipWidth++;

            }
            else if(startY == tiles[i].sprite.getPosition().y)
            {
              clipWidth++;

            }

            clipBoardTiles.push_back(tiles[i]);

          }

        }

        for(int i = 0; i < entities.size(); i++)
        {
          if(entities[i].sprite.getGlobalBounds().intersects(selection.getGlobalBounds()))
          {
            clipBoardEntities.push_back(entities[i]);

          }

        }

      }

    }
    else
    {
      for(int i = 0; i < tiles.size(); i++)
      {

        tiles[i].update(mPos, canHover);
        if(tiles[i].hovered)
        {
          if(leftClick)
          {
            if(ui.selectedTab == 0)
            {
              if(ui.currentTool == 0) // paint tile
              {
                tiles[i].frame = ui.selectedTile;
                tiles[i].sprite.setTextureRect(tileFrame(ui.selectedTile));

              }
              else if(ui.currentTool == 1) //fill tiles
              {
                if(tiles[i].frame != ui.selectedTile)
                {
                  int tile = tiles[i].frame;
                  std::vector<int> toVisit;
                  toVisit.push_back(i);

                  while(!toVisit.empty())
                  {
                    int ind = toVisit[0];
                    toVisit.erase(toVisit.begin());

                    if(tiles[ind].frame == tile)
                    {
                      tiles[ind].frame = ui.selectedTile;
                      tiles[ind].sprite.setTextureRect(tileFrame(ui.selectedTile));

                      //up
                      if(ind - ui.width > 0)
                      {
                        toVisit.push_back(ind - ui.width);
                      }

                      //down
                      if(ind + ui.width < tiles.size())
                      {
                        toVisit.push_back(ind + ui.width);

                      }

                      if((ind + 1)%ui.width != 0)
                      {
                        toVisit.push_back(ind + 1);

                      }

                      if((ind)%ui.width != 0)
                      {
                        toVisit.push_back(ind - 1);

                      }

                    }

                  }

                }

              }
              if(ui.currentTool == 3) //erase
              {
                tiles[i].frame = ui.newTile;
                tiles[i].sprite.setTextureRect(tileFrame(ui.newTile));

              }


            }
            else if(ui.selectedTab == 1)
            {
              if(ui.currentTool == 4) //pencil
              {
                EntityDef& def = ui.entities[ui.selectedEntity];

                if(def.params.size() > 0)
                {
                  ui.openModal = "Parameters";
                  ui.potentialIndex = i;
                  ui.entityParams.clear();

                }
                else
                {
                  setEntity(tiles, entities, i, def);

                }

              }
              else if(ui.currentTool == 3) //erase
              {
                for(int j = 0; j < entities.size(); j++)
                {
                  if(entities[j].sprite.getPosition() == tiles[i].sprite.getPosition())
                  {
                    entities.erase(entities.begin()+j);
                    break;

                  }

                }

              }

            }

          }
          else if(rightClick)
          {
            if(ui.selectedTab == 1)
            {
              if(ui.currentTool == 4)
              {
                for(int j = 0; j < entities.size(); j++)
                {
                  if(entities[j].sprite.getPosition() == tiles[i].sprite.getPosition())
                  {
                    if(entities[j].params.size() > 0)
                    {
                      ui.openModal = "Parameters";
                      ui.potentialIndex = j;
                      ui.entityName = entities[j].name;
                      ui.entityParams = entities[j].params;

                    }
                    break;

                  }

                }

              }

            }

          }

        }

      }

    }

    if(pasteKey)
    {
      int startInd = -1;
      for(int i = 0; i < tiles.size(); i++)
      {
        tiles[i].update(mPos, canHover);
        if(tiles[i].hovered)
        {
          startInd = i;
          break;

        }
      }

      if(startInd != -1)
      {
        for(int j = 0; j < clipBoardTiles.size(); j++)
        {
          int down = j/clipWidth;
          int right = j%clipWidth;

          //std::cout << "down: " << down << " right " << right << "\n";
          int realrow = startInd + down*ui.width;
          int ind = startInd + right + down*ui.width;

          if(ind< tiles.size() && realrow/ui.width == ind/ui.width)
          {
            tiles[ind].frame = clipBoardTiles[j].frame;
            tiles[ind].sprite.setTextureRect(tileFrame(clipBoardTiles[j].frame));
          }

        }

      }

    }

    for(int i = 0; i < tiles.size(); i++)
    {
      if(tiles[i].sprite.getGlobalBounds().intersects(camera.getGlobalBounds()))
      {
           window.draw(tiles[i].sprite);

      }

    }

		for(int i = 0; i < entities.size(); i++)
		{
      if(entities[i].sprite.getGlobalBounds().intersects(camera.getGlobalBounds()))
      {
			     window.draw(entities[i].sprite);
      }
			if(ui.selectedTab == 1)
				window.draw(entities[i].special);

		}

    if(ui.currentTool == 2)
    {
      window.draw(selection);

    }

		window.setView(sf::View(guiView.getGlobalBounds()));


		ImGui::Render();

    window.setView(sf::View(mouseView.getGlobalBounds()));

    //ui.cursor.setSize(sf::Vector2f(100, 100));
    //ui.cursor.setFillColor(sf::Color::Red);

    ui.cursor.setPosition(sf::Vector2f(windowMouse) - mouseView.getSize()/2.0f);
    window.draw(ui.cursor);

		window.display();

	}

	ui.savePrefs();
	ImGui::SFML::Shutdown();

	return 0;
}
