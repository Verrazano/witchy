WITCHY
======
A Top-down puzzle/adventure-rpg game in the vein of gameboy/nes legend of zelda games.

Compiling
=========
Requires SFML 2.4.x and c++11
Use the make file.

optionally: -Wno-deprecated-declarations to suppress sfml deprecated warning.

Formating
========
find src/ -iname *.h -o -iname *.cpp | xargs clang-format -i

CONTROLS
========
z - use (throw potion)
x - interact (pickup, read, brew)
w,a,s,d - move
r - reload textures
HOME - open console (WIP)

Media
=====

![dungeon](media/dungeon.gif)
![hoppers](media/hoppers.gif)
