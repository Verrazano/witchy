#include "TextureManager.h"
#include <iostream>

TextureManager::TextureManager() {}

TextureManager::~TextureManager()
{
  for (int i = 0; i < textures.size(); i++)
  {
    delete textures[i];
  }

  textures.clear();
  paths.clear();
}

sf::Texture* TextureManager::getTexture(std::string path)
{
  for (int i = 0; i < paths.size(); i++)
  {
    if (paths[i] == path)
    {
      return textures[i];
    }
  }

  paths.push_back(path);

  sf::Texture* tex = new sf::Texture();
  if (tex->loadFromFile(path))
  {
    std::cout << "loaded texture: " << path << "\n";
  }

  textures.push_back(tex);
  return tex;
}

bool TextureManager::hasTexture(std::string path)
{
  for (int i = 0; i < paths.size(); i++)
  {
    if (paths[i] == path)
    {
      return true;
    }
  }

  return false;
}

void TextureManager::reload()
{
  for (int i = 0; i < textures.size(); i++)
  {
    textures[i]->loadFromFile(paths[i]);
  }
}