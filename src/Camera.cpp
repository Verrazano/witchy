#include "Camera.h"
#include "Util/Maths.h"

const float Camera::SMALL_TRAUMA = 0.25f;
const float Camera::BIG_TRAUMA = 0.5f;

Camera::Camera() {
  trauma = 0.0f;
}

void Camera::update()
{
  trauma -= 0.05f;

}

void Camera::addTrauma(float t) {
  trauma += t;
}

sf::View Camera::getView() {
  //trauma = fmin(1, fmax(trauma, 0));
  //float shake = trauma*trauma;
  //sf::Vector2f offset = unitVec(qrandIn(0, pi))*shake;
  sf::FloatRect bounds = sf::RectangleShape::getGlobalBounds();
  //bounds.left += offset.x;
  //bounds.top += offset.y;
  sf::View view(bounds);
  return view;
}
