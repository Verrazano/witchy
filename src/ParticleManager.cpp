#include "ParticleManager.h"

ParticleManager::Particle::Particle(sf::Vertex* quad) : quad(quad)
{
  dead = true;
  quad = NULL;
  frames = 0;
}

ParticleManager::ParticleManager()
{
  int count = 1000;

  verts = sf::VertexArray(sf::Quads, count * 4);

  for (int i = 0; i < count; i++)
  {
    particles.push_back(Particle(&verts[i * 4]));
  }
}

void ParticleManager::addParticle(sf::Vector2f pos, sf::Vector2f vel, sf::Color c, float life)
{
  addParticle([&](Particle& p) {
    p.velx = vel.x;
    p.vely = vel.y;
    p.life = life;

    p.frames = 0;

    p.quad[0].position = sf::Vector2f(pos.x - 1, pos.y - 1);
    p.quad[0].color = c;

    p.quad[1].position = sf::Vector2f(pos.x + 1, pos.y - 1);
    p.quad[1].color = c;

    p.quad[2].position = sf::Vector2f(pos.x + 1, pos.y + 1);
    p.quad[2].color = c;

    p.quad[3].position = sf::Vector2f(pos.x - 1, pos.y + 1);
    p.quad[3].color = c;

  });
}

void ParticleManager::addParticle(sf::Vector2f pos, sf::Vector2f vel, sf::IntRect frame, float life)
{
  addAnimatedParticle(pos, vel, frame, 0, life);
}

void ParticleManager::addAnimatedParticle(sf::Vector2f pos, sf::Vector2f vel, sf::IntRect frame, unsigned int frames,
                                          float life)
{
  addParticle([&](Particle& p) {
    p.velx = vel.x;
    p.vely = vel.y;
    p.life = life;

    p.frame = frame;
    p.frames = frames;

    pos.x = (int)pos.x;
    pos.y = (int)pos.y;

    sf::Color c = sf::Color::White;

    p.quad[0].position = sf::Vector2f(pos.x - 8, pos.y - 8);
    p.quad[0].texCoords = sf::Vector2f(frame.left, frame.top);
    p.quad[0].color = c;

    p.quad[1].position = sf::Vector2f(pos.x + 8, pos.y - 8);
    p.quad[1].texCoords = sf::Vector2f(frame.left + frame.width, frame.top);
    p.quad[1].color = c;

    p.quad[2].position = sf::Vector2f(pos.x + 8, pos.y + 8);
    p.quad[2].texCoords = sf::Vector2f(frame.left + frame.width, frame.top + frame.height);
    p.quad[2].color = c;

    p.quad[3].position = sf::Vector2f(pos.x - 8, pos.y + 8);
    p.quad[3].texCoords = sf::Vector2f(frame.left, frame.top + frame.height);
    p.quad[3].color = c;

  });
}

void ParticleManager::addParticle(std::function<void(Particle&)> f)
{
  for (int i = 0; i < particles.size(); i++)
  {
    if (particles[i].dead)
    {
      Particle& p = particles[i];
      p.dead = false;

      f(p);

      p.timer.restart();
      return;
    }
  }
}

void ParticleManager::update()
{
  for (int i = 0; i < particles.size(); i++)
  {
    if (!particles[i].dead)
    {
      Particle& p = particles[i];

      sf::Vector2f m(p.velx, p.vely);

      p.rem += m;

      float t = p.timer.getElapsedTime().asSeconds();
      sf::Color nc = p.quad[0].color;
      nc.a = (1.0f - t / p.life) * 255.0f;

      if (p.frames > 0)
      {
        float fr = p.life / (float)p.frames;
        int f = (int)(t / fr);

        sf::IntRect frame(p.frame);
        frame.left += f * 8;

        p.quad[0].texCoords = sf::Vector2f(frame.left, frame.top);
        p.quad[1].texCoords = sf::Vector2f(frame.left + frame.width, frame.top);
        p.quad[2].texCoords = sf::Vector2f(frame.left + frame.width, frame.top + frame.height);
        p.quad[3].texCoords = sf::Vector2f(frame.left, frame.top + frame.height);
      }
      else
      {
        p.quad[0].color = nc;
        p.quad[1].color = nc;
        p.quad[2].color = nc;
        p.quad[3].color = nc;
      }

      sf::Vector2f m2((int)p.rem.x, (int)p.rem.y);
      p.rem -= m2;

      p.quad[0].position += m2;
      p.quad[1].position += m2;
      p.quad[2].position += m2;
      p.quad[3].position += m2;

      p.velx *= 0.97f;
      p.vely *= 0.97f;

      if (p.timer.getElapsedTime().asSeconds() > p.life)
      {
        p.dead = true;
        p.quad[0].color = sf::Color::Transparent;
        p.quad[1].color = sf::Color::Transparent;
        p.quad[2].color = sf::Color::Transparent;
        p.quad[3].color = sf::Color::Transparent;
      }
    }
  }
}

void ParticleManager::draw(sf::RenderWindow& window, sf::RenderStates states)
{
  states.texture = tex;

  window.draw(verts, tex);
}

void ParticleManager::clear() {
  for (int i = 0; i < particles.size(); i++)
  {
    if (!particles[i].dead) {
      Particle& p = particles[i];
      p.dead = true;
      p.quad[0].color = sf::Color::Transparent;
      p.quad[1].color = sf::Color::Transparent;
      p.quad[2].color = sf::Color::Transparent;
      p.quad[3].color = sf::Color::Transparent;
    }
  }
}
