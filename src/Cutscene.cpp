#include "Cutscene.h"
#include "Game.h"

Cutscene::Cutscene(Game* game) : inited(false), game(game)
{
  blackout.setSize(game->config.wSize);
  blackout.setFillColor(sf::Color(0, 0, 0, 0));
}

void Cutscene::init()
{
}

bool Cutscene::update()
{
  return false;

}

void Cutscene::cleanup()
{

}

bool Cutscene::fadeOut()
{
  bool ret = false;

  sf::Color c = blackout.getFillColor();
  int a = c.a;
  a += 25;
  if(a >= 255)
  {
    a = 255;
    ret = true;
  }

  c.a = a;

  blackout.setFillColor(c);
  game->guiLayer.push_back(&blackout);

  return ret;

}

bool Cutscene::fadeIn()
{
  bool ret = false;

  sf::Color c = blackout.getFillColor();
  int a = c.a;
  a -= 25;
  if(a <= 0)
  {
    a = 0;
    ret = true;
  }

  c.a = a;

  blackout.setFillColor(c);
  game->guiLayer.push_back(&blackout);

  return ret;

}
