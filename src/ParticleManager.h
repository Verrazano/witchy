#ifndef PARTICLEMANAGER_H_
#define PARTICLEMANAGER_H_

#include <SFML/Graphics.hpp>
#include <functional>

class ParticleManager
{
public:
  ParticleManager();

  void addParticle(sf::Vector2f pos, sf::Vector2f vel, sf::Color c, float life = 5.0f);

  void addParticle(sf::Vector2f pos, sf::Vector2f vel, sf::IntRect frame, float life = 5.0f);

  void addAnimatedParticle(sf::Vector2f pos, sf::Vector2f vel, sf::IntRect frame, unsigned int frames,
                           float life = 5.0f);

  void update();

  void draw(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

  void clear();

  struct Particle
  {
    Particle(sf::Vertex* quad);

    sf::Vertex* quad;
    sf::Clock timer;
    float life;
    float velx;
    float vely;
    bool dead;
    sf::Vector2f rem;
    sf::IntRect frame;
    unsigned int frames;
  };

  void addParticle(std::function<void(Particle&)> f);

  sf::Texture* tex;
  std::vector<Particle> particles;
  sf::VertexArray verts;
};

#endif /*PARTICLEMANAGER*/
