#ifndef ENTITY_H_
#define ENTITY_H_

#include "Util/Properties.h"
#include "Util/Sprite.h"
#include <SFML/Graphics.hpp>
#include <list>
#include <set>

class Game;
class Effect;

class Entity
{
public:
  Entity(Game* game, sf::Vector2f size, sf::Vector2f pos);

  virtual ~Entity();

  virtual std::string getName() = 0;

  virtual void onTick();
  virtual void onDie();

  virtual bool doesCollide(Entity* e);
  virtual void onCollide(Entity* e);
  virtual void onCollideMap();

  virtual void onEffect(std::string name);
  virtual void onEffectEnd(std::string name);

  virtual void onDeath(Entity* other);
  virtual void onCreate(Entity* other);

  virtual void onHit();

  virtual void onMapSwitch();

  void kill();

  bool move(sf::Vector2f m);
  bool move(float x, float y);
  void resolveIntersect();

  void setPosition(sf::Vector2f pos);
  sf::Vector2f getPosition();
  sf::FloatRect getBounds();

  void addTag(std::string tag);
  bool hasTag(std::string tag);
  bool hasTag(std::vector<std::string> tag);
  void remTag(std::string tag);

  void update();

  void setZ(int nz);

  void dispatchMessages();

  void sendMessage(Entity* sender, std::string name, Properties props = Properties());

  typedef struct Message
  {
    Entity* sender;
    std::string name;
    Properties props;

  } Message;

  Message makeMessage(Entity* sender, std::string name, Properties props);

  std::vector<Message> messages;

  virtual void onMessage(Message msg);

  virtual void draw(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

  void hit(int damage);
  void setMaxHealth(int max);
  void drawHearts();

  std::set<unsigned int> trackedResources;

  sf::Vector2f rem;

  sf::RectangleShape sprite;
  sf::RectangleShape bounds;
  Sprite rsprite;

  bool collideMap;
  bool solid;

  int maxHealth;
  int health;
  std::vector<sf::RectangleShape> hearts;
  bool showHearts;
  int heartsAlpha;
  int hitRed;

  bool useCorrections;

  bool noOnDie;
  bool noDelete;

  // sf::CircleShape tileMarker;

  float speedFactor;

  Game* game;

  std::vector<std::string> tags;

  sf::FloatRect intersection;
  Entity* collidingWith;
  Entity* player;

  int z;

  Properties props;

  unsigned int id;

  bool registered;
  std::list<Entity*>::iterator listIt;

  int solidFilter;

  sf::Vector2f lastPos;
  bool didMove;

  // variables with map switch
  bool destroyOnSwitch;
  bool keepOnSwitch;
};

#endif /*ENTITY*/
