#include "Game.h"
#include "SoundEffect.h"

int main(int argc, char** argv)
{
  Game game;

  while (game.isRunning())
  {
    game.tick();
  }

  SoundEffect::cleanup();
}
