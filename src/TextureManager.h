#ifndef TEXTUREMANAGER_H_
#define TEXTUREMANAGER_H_

#include <SFML/Graphics.hpp>

class TextureManager
{
public:
  TextureManager();

  ~TextureManager();

  sf::Texture* getTexture(std::string path);
  bool hasTexture(std::string path);

  void reload();

  std::vector<std::string> paths;
  std::vector<sf::Texture*> textures;
};

#endif /*TEXTUREMANAGER*/