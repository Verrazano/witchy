#ifndef CAMERA_H_
#define CAMERA_H_

#include <SFML/Graphics.hpp>

class Camera : public sf::RectangleShape
{
public:
  static const float SMALL_TRAUMA;
  static const float BIG_TRAUMA;

  Camera();

  void update();
  void addTrauma(float t);

  sf::View getView();

  float trauma;
};

#endif /*CAMERA*/
