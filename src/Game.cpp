#include "Game.h"
#include "Controller.h"
#include "Entities/Player/Player.h"
#include "Entity.h"
#include "Map/MapLoader.h"
#include "SoundEffect.h"
#include "Util/Strings.h"
#include <SFML/Config.hpp>
#include <algorithm>
#include <iostream>
#include "imgui/imgui.h"
#include "imgui/imgui-SFML.h"
#include "Cutscene.h"
#include "Cutscenes/ScriptedScene.h"
#include "Cutscenes/GoToLakeScene.h"

Game::Game()
{
  consolas.loadFromFile("res/fonts/consolas.ttf");
  pixeltype.loadFromFile("res/fonts/pixeltype.ttf");

  console = Console(sf::Vector2f(config.wSize.x - 100, 200));
  std::cout << "SFML VERSION " << SFML_VERSION_MAJOR << "." << SFML_VERSION_MINOR << "\n";

  window.create(sf::VideoMode(config.wSize.x, config.wSize.y), "witchy", sf::Style::Close);
  window.setFramerateLimit(30);

  ImGui::SFML::Init(window);
  ImGui::GetIO().IniFilename = NULL;

  ticks = 0;

  sf::ContextSettings contextSettings = window.getSettings();
  std::cout << "OpenGL Version " << contextSettings.majorVersion << "." << contextSettings.minorVersion << "\n";
  shadersAvailable = sf::Shader::isAvailable();
  if (shadersAvailable)
  {
    std::cout << "Shaders are supported\n";
  }
  else
  {
    std::cout << "!!!!! SHADERS ARE NOT SUPPORTED !!!!!\n";
  }

  std::cout << "Maximum Texture Size " << sf::Texture::getMaximumSize() << "\n";

  std::cout << "========================================\n";

  SoundEffect::globalVolume = config.volume;

  guiView = sf::View(sf::FloatRect(0, 0, config.wSize.x, config.wSize.y));
  camera.setSize(sf::Vector2f(160, 144));
  camera.setOrigin(camera.getSize()/2.0f);

  framerate = 0;
  framerateText = sf::Text("", consolas, 15);
  framerateText.setPosition(2, 2);
  framerateText.setFillColor(sf::Color::Black);

  particles.tex = textures.getTexture("res/particles.png");
  particleTexSize = particles.tex->getSize();

  srand(time(NULL));

  nextId = 1;

  /*sf::Image mapIMG;
  mapIMG.loadFromFile("res/map.png");*/

  baseMap = config.mapPath;
  mapsToLoad.push_back(config.baseMapPath + baseMap);

  while (!mapsToLoad.empty())
  {
    std::string mPath = mapsToLoad[0];
    mapsToLoad.erase(mapsToLoad.begin());

    if (maps.count(mPath) == 0)
    {
      Map m(this, mPath);
      registerTiles(m);
      registerEntities(m);

      m.load();

      maps[mPath] = m;
    }
  }

  switchMap(config.baseMapPath + baseMap);

  textbox = textures.getTexture("res/textbox.png");
  textboxname = textures.getTexture("res/textboxname.png");
  textboxquestion = textures.getTexture("res/textboxquestion.png");

  dialogBack = sf::RectangleShape(config.wSize);

  dialogText.setFont(pixeltype);
  dialogText.setCharacterSize(8 * config.scale);
  dialogText.setPosition(sf::Vector2f(9 * config.scale, 98 * config.scale));
  dialogText.setBounds(sf::FloatRect(9 * config.scale, 98 * config.scale, 141 * config.scale, 36 * config.scale));
  dialogText.allowResize(false);
  dialogText.setPadding(0);
  //dialogText.drawBounds(true); //TODO: debug and fix gui screen scaling issues

  dialogNameText = sf::Text("", pixeltype, 16 * config.scale);
  dialogNameText.setFillColor(sf::Color::Black);

  dialogOption1 = sf::Text("", pixeltype, 8 * config.scale);
  dialogOption1.setFillColor(sf::Color::Black);

  dialogOption2 = sf::Text("", pixeltype, 8 * config.scale);
  dialogOption2.setFillColor(sf::Color::Black);

  dialogDebug.setFillColor(sf::Color::Transparent);
  dialogDebug.setOutlineColor(sf::Color::Red);
  dialogDebug.setOutlineThickness(1);

  alagard.loadFromFile("res/fonts/alagard.ttf");
  areaTitleText = sf::Text("", alagard, 16 * config.scale);
  areaTitleText.setStyle(sf::Text::Underlined);
  areaTitleText.setFillColor(sf::Color::White);

  hasFocus = true;

  removeAtEnd = false;

  //TEMPORARY
  cutscenes["go_to_lake"] = new GoToLakeScene(this);
  cutscene = NULL;
  cutscenePlaying = false;

}

Game::~Game()
{
  for (int i = 0; i < entities.size(); i++)
  {
    delete entities[i];
  }

  entities.clear();

  for(const auto& elem : cutscenes)
  {
    delete elem.second;
  }

  cutscenes.clear();
}

bool Game::isRunning() { return window.isOpen(); }

void Game::tick()
{
  framerate = (int)(1.0f / frameTimer.restart().asSeconds());
  framerateText.setString(toString(framerate) + " fps");

  sf::Event event;
  while (window.pollEvent(event))
  {
    ImGui::SFML::ProcessEvent(event);
    if (event.type == sf::Event::Closed)
    {
      window.close();
    }
    else if (event.type == sf::Event::KeyPressed)
    {
      if (event.key.code == sf::Keyboard::Key::Home)
      {
        console.consoleOpen = !console.consoleOpen;
        Controller::captureInput = hasFocus && !console.consoleOpen;
      }
      else if (event.key.code == sf::Keyboard::R)
      {
        textures.reload();
      }
    }
    else if (event.type == sf::Event::GainedFocus)
    {
      hasFocus = true;
      Controller::captureInput = hasFocus && !console.consoleOpen;
    }
    else if (event.type == sf::Event::LostFocus)
    {
      hasFocus = false;
      Controller::captureInput = hasFocus && !console.consoleOpen;
    }
  }

  ImGui::SFML::Update(deltaClock.restart());

  if (entitiesToAdd.size() > 0)
  {
    invalidateZ = true;
  }

  for (int i = 0; i < entitiesToAdd.size(); i++)
  {
    entities.push_back(entitiesToAdd[i]);
  }

  entitiesToAdd.clear();

  for (int i = 0; i < listenersToUnregister.size(); i++)
  {
    Entity* le = listenersToUnregister[i];
    listeners.erase(le->listIt);
  }

  listenersToUnregister.clear();


  if(cutscenePlaying)
  {
    if(cutscene)
    {
      if(!cutscene->inited)
      {
        cutscene->init();
        cutscene->inited = true;

      }
      else
      {
        if(cutscene->update())
        {
          cutscene->cleanup();
          //delete cutscene;
          cutscene = NULL;
          cutscenePlaying = false;

        }

      }

    }

  }

  window.clear(sf::Color(20, 12, 28));

  camera.update();
  sf::View gameView = camera.getView();
  gameView.setViewport(config.viewPort);
  window.setView(gameView);

  map.draw(window);

  std::vector<Entity*>::iterator it;
  for (it = entities.begin(); it != entities.end();)
  {
    Entity* entity = *it;

    if (entity == NULL)
    {
      it++;
      continue;
      // it = entities.erase(it);
      // continue;
    }

    // std::cout << "entity: " << entity << "\n";
    if (entity->health <= 0)
    {
      if (!entity->noOnDie)
        entity->onDie();

      unregisterListener(entity);

      std::list<Entity*>::iterator lit;
      for (lit = listeners.begin(); lit != listeners.end(); lit++)
      {
        Entity* listener = *lit;
        listener->onDeath(entity);
      }

      delete entity;
      it = entities.erase(it);
      continue;
    }

    if (!entity->noDelete)
    {
      entity->update();

      entity->draw(window);
    }
    it++;
  }

  if (removeAtEnd)
  {
    // TODO: optimize this toDeleteList
    for (it = entities.begin(); it != entities.end();)
    {
      Entity* entity = *it;
      if (entity->noDelete)
      {
        entity->noDelete = false;
        it = entities.erase(it);
        continue;
      }
      it++;
    }
    removeAtEnd = false;
  }

  for (int i = 0; i < topLayer.size(); i++)
  {
    window.draw(*topLayer[i]);
  }

  particles.update();
  particles.draw(window);

  topLayer.clear();

  window.setView(guiView);

  for (int i = 0; i < guiLayer.size(); i++)
  {
    window.draw(*guiLayer[i]);
  }

  guiLayer.clear();

  if (areaTitleTick > 0)
  {
    areaTitleTick -= 2;

    if (areaTitleTick < 255)
    {
      areaTitleText.setFillColor(sf::Color(255, 255, 255, areaTitleTick));
    }

    window.draw(areaTitleText);
  }

  if(console.consoleOpen)
  {
    console.update();

  }

  ImGui::Render();

  if (config.debug)
  {
    window.draw(framerateText);
  }

  window.display();

  //if (invalidateZ)
  {
    invalidateZ = false;
    std::stable_sort(entities.begin(), entities.end(), [](Entity* lhs, Entity* rhs) {
      if(lhs->z == rhs->z)
      {
              return lhs->getPosition().y <
      rhs->getPosition().y;

      }

      return lhs->z < rhs->z;

    });
  }

  if (nextMap != "")
  {
    switchMap(nextMap);
    nextMap = "";
  }

  ticks++;
}

bool Game::isCollidingAt(Entity* entity, sf::Vector2f pos)
{
  sf::RectangleShape bounds = entity->bounds;
  bounds.setPosition(pos);
  sf::FloatRect rect = bounds.getGlobalBounds();
  sf::FloatRect intersection;

  if (entity->solid)
  {
    // TODO: put entities in buckets to make this more efficient
    for (int i = 0; i < entities.size(); i++)
    {
      Entity* e = entities[i];
      if (e != entity && e->solid)
      {
        if (e->bounds.getGlobalBounds().intersects(rect, intersection) && entity->doesCollide(e) &&
            e->doesCollide(entity))
        {
          entity->collidingWith = e;
          entity->intersection = intersection;
          return true;
        }
      }
    }
  }

  if (entity->collideMap)
  {
    return map.isCollidingAt(rect, pos, (Tile::Flag)entity->solidFilter);
  }

  return false;
}

void Game::resolveIntersect(Entity* entity)
{
  if (entity->solid)
  {
    sf::FloatRect bounds = entity->bounds.getGlobalBounds();

    for (int i = 0; i < entities.size(); i++)
    {
      Entity* e = entities[i];
      if (e != entity && e->health > 0 && e->solid && entity->doesCollide(e))
      {
        sf::FloatRect intersect;
        if (e->bounds.getGlobalBounds().intersects(bounds, intersect))
        {
          sf::Vector2f pos = entity->bounds.getPosition();
          if (intersect.width > intersect.height)
            if (pos.y > intersect.top)
              entity->bounds.move(0, intersect.height);
            else
              entity->bounds.move(0, -intersect.height);
          else if (pos.x > intersect.left)
            entity->bounds.move(intersect.width, 0);
          else
            entity->bounds.move(-intersect.width, 0);

          entity->sprite.setPosition(entity->bounds.getPosition());
        }
      }
    }
  }

  if (entity->collideMap)
  {
    map.resolveCollision(entity, (Tile::Flag)entity->solidFilter);
  }
}

std::vector<Entity*> Game::getEntitiesInBox(sf::FloatRect rect, std::function<bool(Entity*)> selector)
{
  std::vector<Entity*> found;
  for(Entity* e : entities)
  {
    if(e->bounds.getGlobalBounds().intersects(rect))
    {
      if(selector(e))
      {
        found.push_back(e);
      }

    }

  }

  return found;


}

std::vector<Entity*> Game::getEntitiesInRadius(sf::Vector2f pos, float range, std::function<bool(Entity*)> selector)
{
  std::vector<Entity*> found;
  for(Entity* e : entities)
  {
    float d = dist(pos, e->getPosition());
    if(d <= range)
    {
      if(selector(e))
      {
        found.push_back(e);
      }

    }

  }

  return found;

}

std::function<bool(Entity*)> Game::withName(std::vector<std::string> names)
{
  return [names](Entity* e)
  {
    for(std::string name : names)
    {
      if(e->getName() == name)
      {
        return true;
      }
    }
    return false;
  };

}

std::function<bool(Entity*)> Game::withTag(std::vector<std::string> tags)
{
  return [tags](Entity* e)
  {
    for(std::string tag : tags)
    {
      if(e->hasTag(tag))
      {
        return true;
      }
    }
    return false;
  };

}

Entity* Game::getPlayer()
{
  for (Entity* e : entities)
  {
    if (e->getName() == "player")
    {
      return e;
    }
  }

  return NULL;
}

void Game::displayDialog(std::string text)
{
  dialogBack.setTexture(textbox);
  dialogText.setString(text);
  // sf::FloatRect bounds = dialogText.getLocalBounds();
  // dialogText.setOrigin(0, bounds.height + 1*config.scale);
  // dialogText.setPosition(9*config.scale, 98*config.scale);

  // dialogDebug.setPosition(dialogText.getPosition());
  // dialogDebug.setSize(sf::Vector2f(bounds.width, bounds.height));

  guiLayer.push_back(&dialogBack);
  guiLayer.push_back(&dialogText);
  // guiLayer.push_back(&dialogDebug);
}

void Game::displayNamedDialog(std::string text, std::string name)
{
  dialogBack.setTexture(textboxname);

  dialogText.setString(text);
  // sf::FloatRect bounds = dialogText.getLocalBounds();
  // dialogText.setOrigin(0, bounds.height + 1*config.scale);
  // dialogText.setPosition(9*config.scale, 98*config.scale);

  dialogNameText.setString(name);
  sf::FloatRect bounds = dialogNameText.getLocalBounds();
  dialogNameText.setOrigin(0, bounds.height);
  dialogNameText.setPosition(101 * config.scale, 85 * config.scale);

  // dialogDebug.setPosition(dialogNameText.getPosition());
  // dialogDebug.setSize(sf::Vector2f(bounds.width, bounds.height));

  guiLayer.push_back(&dialogBack);
  guiLayer.push_back(&dialogText);
  // guiLayer.push_back(&dialogDebug);
  guiLayer.push_back(&dialogNameText);
}

void Game::displayQuestionDialog(std::string text, std::string name,
  std::string option1, std::string option2)
{
  option1 = "(z) " + option1;
  option2 = "(x) " + option2;
  dialogBack.setTexture(textboxquestion);
  dialogText.setString(text);

  dialogNameText.setString(name);
  sf::FloatRect bounds = dialogNameText.getLocalBounds();
  dialogNameText.setOrigin(0, bounds.height);
  dialogNameText.setPosition(101 * config.scale, 85 * config.scale);

  dialogOption1.setString(option1);
  bounds = dialogOption1.getLocalBounds();
  dialogOption1.setOrigin(0, bounds.height);
  dialogOption1.setPosition(12 * config.scale, 130 * config.scale);

  dialogOption2.setString(option2);
  bounds = dialogOption2.getLocalBounds();
  dialogOption2.setOrigin(0, bounds.height);
  dialogOption2.setPosition(64 * config.scale, 130 * config.scale);

  guiLayer.push_back(&dialogBack);
  guiLayer.push_back(&dialogText);
  // guiLayer.push_back(&dialogDebug);
  guiLayer.push_back(&dialogNameText);
  guiLayer.push_back(&dialogOption1);
  guiLayer.push_back(&dialogOption2);

}

void Game::displayAreaTitle(std::string title)
{
  if (lastAreaTitle == title)
  {
    return;
  }

  lastAreaTitle = title;

  areaTitleTick = 270;
  areaTitleText.setString(title);
  areaTitleText.setOrigin(areaTitleText.getLocalBounds().width / 2.0f, 0.0f);

  sf::Vector2f position((int)(config.wSize.x / 2.0f), (int)(config.wSize.y / 2.0f));

  areaTitleText.setPosition(position);
  areaTitleText.setFillColor(sf::Color::White);
}

Entity* Game::getEntityById(unsigned int id)
{
  for (int i = 0; i < entities.size(); i++)
  {
    if (entities[i]->id == id)
      return entities[i];
  }

  return NULL;
}

void Game::removeEntityWithId(unsigned int id)
{
  for (int i = 0; i < entities.size(); i++)
  {
    if (entities[i]->id == id)
    {
      removeAtEnd = true;
      entities[i]->noDelete = true;
      return;
    }
  }

  for (int i = 0; i < entitiesToAdd.size(); i++)
  {
    if(entitiesToAdd[i]->id == id)
    {
      entitiesToAdd.erase(entitiesToAdd.begin() + i);
      return;

    }

  }

}

unsigned int Game::addEntity(Entity* entity)
{
  if (entity != NULL && entity->id == 0)
  {
    entity->id = nextId;
    nextId++;

    entitiesToAdd.push_back(entity);

    std::list<Entity*>::iterator it;
    for (it = listeners.begin(); it != listeners.end(); it++)
    {
      if (*it != entity)
        (*it)->onCreate(entity);
    }

    return entity->id;
  }
  else if (entity != NULL && entity->id != 0)
  {
    if (entity->noDelete)
    {
      entity->noDelete = false;
    }
    else
    {
      entitiesToAdd.push_back(entity);
    }

    return entity->id;
  }

  return 0;
}

void Game::registerListener(Entity* e)
{
  if (!e->registered)
  {
    e->registered = true;
    e->listIt = listeners.insert(listeners.end(), e);
  }
}

void Game::unregisterListener(Entity* e)
{
  if (e->registered)
  {
    listenersToUnregister.push_back(e);
    e->registered = false;
  }
}

void Game::addParticle(sf::Vector2f pos, sf::Vector2f vel, unsigned int frame, float life)
{
  sf::IntRect f = getFrame(frame, sf::Vector2u(8, 8), particleTexSize);
  particles.addParticle(pos, vel, f, life);
}

void Game::addParticles(sf::Vector2f pos, unsigned int frame, unsigned int frames, unsigned count, float life,
                        float powerMin, float powerMax, float angleMin, float angleMax)
{
  for (unsigned int i = 0; i < count; i++)
  {
    float r = qrandIn(angleMin, angleMax);
    float p = qrandIn(powerMin, powerMax);

    sf::Vector2f vel(p * cos(r), p * sin(r));

    unsigned frameNum = frame;
    if (frames != 0)
    {
      frameNum += qrand() % frames;
    }

    sf::IntRect f = getFrame(frameNum, sf::Vector2u(8, 8), particleTexSize);

    particles.addParticle(pos, vel, f, life);
  }
}

void Game::addParticlesInCloud(sf::Vector2f pos, float radius, unsigned int frame, unsigned int frames, unsigned count,
                               float life, float powerMin, float powerMax)
{
  for (unsigned int i = 0; i < count; i++)
  {
    float r = qrandIn(0.0f, 2 * pi);
    float p = qrandIn(powerMin, powerMax);

    sf::Vector2f vel(p * cos(r), p * sin(r));

    unsigned frameNum = frame;
    if (frames != 0)
    {
      frameNum += qrand() % frames;
    }

    sf::IntRect f = getFrame(frameNum, sf::Vector2u(8, 8), particleTexSize);

    sf::Vector2f cpos = pos + unitVec(qrand() % 1000 / 1000.0f * 2 * pi) * qrandIn(0, radius);

    particles.addParticle(cpos, vel, f, life);
  }
}

void Game::addAnimatedParticles(sf::Vector2f pos, unsigned int frame, unsigned int frames, unsigned count, float life,
                                float powerMin, float powerMax, float angleMin, float angleMax)
{
  for (unsigned int i = 0; i < count; i++)
  {
    float r = qrandIn(angleMin, angleMax);
    float p = qrandIn(powerMin, powerMax);

    sf::Vector2f vel(p * cos(r), p * sin(r));

    sf::IntRect f = getFrame(frame, sf::Vector2u(8, 8), particleTexSize);

    particles.addAnimatedParticle(pos, vel, f, frames, life);
  }
}

std::string Game::currentMapPath() { return map.path; }

void Game::switchMap(std::string nextMap)
{
  std::cout << "switching map: " << nextMap << "\n";
  this->nextMap = nextMap;

  areaTitleTick = 0;

  std::vector<Entity*> keepEntities;
  std::vector<Entity*>::iterator it;
  for (it = entities.begin(); it != entities.end();)
  {
    Entity* entity = *it;

    if (entity->keepOnSwitch)
    {
      keepEntities.push_back(entity);
      it = entities.erase(it);
      continue;
    }
    else if (entity->destroyOnSwitch)
    {
      if (!entity->noOnDie)
        entity->onDie();

      unregisterListener(entity);

      std::list<Entity*>::iterator lit;
      for (lit = listeners.begin(); lit != listeners.end(); lit++)
      {
        Entity* listener = *lit;
        listener->onDeath(entity);
      }

      delete entity;
      it = entities.erase(it);
      continue;
    }

    it++;
  }

  if (map.path != "")
  {
    // store map entities back in the map
    for (int i = 0; i < entitiesToAdd.size(); i++)
    {
      entities.push_back(entitiesToAdd[i]);
    }

    map.entities = entities;
    maps[map.path] = map;
  }

  // now load next map
  map = maps[nextMap];

  entities.clear();

  if (!map.entitiesAdded)
  {
    map.entitiesAdded = true;
    for (int i = 0; i < map.entities.size(); i++)
    {
      addEntity(map.entities[i]);
    }
  }
  else
  {
    entities = map.entities;
  }

  if (entitiesToAdd.size() > 0)
  {
    invalidateZ = true;
  }

  for (int i = 0; i < entitiesToAdd.size(); i++)
  {
    entities.push_back(entitiesToAdd[i]);
  }

  entitiesToAdd.clear();

  for (Entity* entity : keepEntities)
  {
    addEntity(entity);
    entity->onMapSwitch();
  }

  particles.clear();

}

void Game::playCutscene(std::string name)
{
  Cutscene* c = cutscenes[name];
  cutscene = c;
  cutscenePlaying = true;

}
