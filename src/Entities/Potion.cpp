#include "Potion.h"
#include "../Game.h"
#include "../Util/Maths.h"
#include "../Util/Sprites.h"
#include "Entities.h"

Potion::Potion(Game* game, sf::Vector2f pos, std::string potion)
    : Pickupable(game, sf::Vector2f(16, 16), pos), potion(potion)
{
  rsprite.setTexture(game->textures.getTexture("res/potions.png"));
  rsprite.addAnim("main", {0}, 0.1f, false);
  rsprite.addAnim("throw", {0, 1, 2, 3, 4, 5, 6, 7}, 0.06f);

  if (potion == "shrink")
  {
    rsprite.setOffset(sf::Vector2u(0, 0));
  }

  carriedAnim = "main";

  useCorrections = false;
  solid = false;

}

std::string Potion::getName() { return "potion_" + potion; }

void Potion::onTick() { updateThrown(); }

void Potion::onDie()
{
  sf::Vector2f pos = getPosition();
  if(potion == "shrink")
  {
    float range = 32.0f;
    std::vector<Entity*> shrinkables = game->getEntitiesInRadius(pos, range, Game::withTag({"shrinkable"}));
    for(Entity* e : shrinkables)
    {
      e->sendMessage(this, "shrink");

    }

    game->addParticlesInCloud(pos, range, 0, 3, 40, 1.0f);

  }

}

void Potion::onPickup() { addTag("picked_up"); }

void Potion::onThrown()
{
  rsprite.setAnim("throw").play();

}

void Potion::onLand()
{
  if (hasTag("picked_up"))
  {
    kill();
  }
}
