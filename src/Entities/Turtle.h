#ifndef TURTLE_H_
#define TURTLE_H_

#include "../Entity.h"

class Turtle : public Entity
{
public:
  Turtle(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  int state; // 0=waiting,1=idle,2=move,3=attack,4=cooldown
  sf::Clock actionTime;
  sf::Vector2f target;
  bool attackFrame;
};

#endif /*TURTLE*/