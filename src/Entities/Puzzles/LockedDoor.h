#ifndef LOCKEDDOOR_H_
#define LOCKEDDOOR_H_

#include "../../Entity.h"

class LockedDoor : public Entity
{
public:
  LockedDoor(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onCollide(Entity* entity);
  bool opening;
};

#endif /*LOCKEDDOOR*/
