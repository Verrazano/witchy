#include "Key.h"
#include "../../Game.h"

Key::Key(Game* game, sf::Vector2f pos) : Pickupable(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/entities.png"));

  rsprite.addAnim("main", {33}, 0.2f, false);
  rsprite.setAnim("main");

  // solid = false;

  carriedAnim = "main";

  props.setInt("potionColor", 0xdbd75d);
  addTag("ingredient");
  addTag("key");
}

std::string Key::getName() { return "key"; }

void Key::onTick() { updateThrown(); }

bool Key::doesCollide(Entity* other) { return other->getName() == "locked_door" || Pickupable::doesCollide(other); }
