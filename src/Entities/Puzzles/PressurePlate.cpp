#include "PressurePlate.h"
#include "../../Game.h"
#include "../Triggers/Zone.h"

PressurePlate::PressurePlate(Game* game, sf::Vector2f pos) :
  Entity(game, sf::Vector2f(16, 16), pos)
{
  solid = false;

  rsprite.setTexture(game->textures.getTexture("res/puzzles/pressureplate.png"));
  rsprite.addAnim("main", {0, 1}, 0.1f, false);

  pressed = false;

}

std::string PressurePlate::getName()
{
  return "pressure_plate";

}

void PressurePlate::onTick()
{
  if(game->ticks%5 == 0)
  {
    sf::FloatRect b = bounds.getGlobalBounds();
    std::vector<Entity*> entities = game->getEntitiesInBox(b, Game::withTag({"heavy"}));

    if(!entities.empty())
    {
      if(!pressed)
      {
        rsprite.getAnim().setIndex(1);
        rsprite.resetTextureRect();

        pressed = true;

        auto puzzle_elements = getPuzzleElements();

        for(auto p : puzzle_elements)
        {
          p->sendMessage(this, "pressure_plate_pressed");
        }
      }

    }
    else if(pressed)
    {
      rsprite.getAnim().setIndex(0);
      rsprite.resetTextureRect();

      auto puzzle_elements = getPuzzleElements();

      pressed = false;

      for(auto p : puzzle_elements)
      {
        p->sendMessage(this, "pressure_plate_unpressed");
      }

    }

  }


}

std::vector<Entity*> PressurePlate::getPuzzleElements()
{
  sf::FloatRect b = bounds.getGlobalBounds();
  std::vector<Entity*> zones = game->getEntitiesInBox(b, Game::withName({"zone"}));
  Zone* zone = (Zone*)zones[0];
  sf::FloatRect zoneBounds = zone->bounds.getGlobalBounds();

  std::vector<Entity*> puzzle_elements = game->getEntitiesInBox(zoneBounds, Game::withTag({"puzzle_element"}));

  return puzzle_elements;
}
