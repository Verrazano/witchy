#ifndef PUZZLEBLOCK_H_
#define PUZZLEBLOCK_H_

#include "../../Entity.h"

class PuzzleBlock : public Entity
{
public:
  PuzzleBlock(Game* game, sf::Vector2f pos);

  std::string getName();

  void onMessage(Message msg);

  sf::Vector2f originalPos;

};

#endif /*PUZZLEBLOCK*/
