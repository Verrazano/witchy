#ifndef PRESSUREPLATE_H_
#define PRESSUREPLATE_H_

#include "../../Entity.h"

class PressurePlate : public Entity
{
public:
  PressurePlate(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  std::vector<Entity*> getPuzzleElements();

  bool pressed;

};

#endif /*PRESSUREPLATE*/
