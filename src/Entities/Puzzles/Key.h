#ifndef KEY_H_
#define KEY_H_

#include "../Pickupable.h"

class Key : public Pickupable
{
public:
  Key(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  bool doesCollide(Entity* other);
};

#endif /*Key*/
