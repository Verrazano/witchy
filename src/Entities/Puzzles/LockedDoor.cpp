#include "LockedDoor.h"
#include "../../Game.h"
#include "../Player/Player.h"

LockedDoor::LockedDoor(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(24, 32), pos - sf::Vector2f(0, 8))
{
  rsprite.setSize(sf::Vector2f(32, 32));
  rsprite.setFrameSize(sf::Vector2u(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/lockeddoor.png"));
  opening = false;

  rsprite.setOffset(sf::Vector2u(0, 16));

  rsprite.addAnim("open", {0, 1, 2, 3, 4, 5, 6, 7}, 0.15f, false);
  rsprite.setAnim("open");
}

std::string LockedDoor::getName() { return "locked_door"; }

void LockedDoor::onTick()
{
  if (opening && !rsprite.getAnim().isPlaying())
    opening = false;

  if (solid && opening)
  {
    if (rsprite.getAnim().getFrame() == 5)
      solid = false;
  }

  if (opening)
  {
    if (game->ticks % 5 == 0)
    {
      game->addParticles(getPosition(), 53, 3, 3, 0.8f, 0.75f, 1.5f);
    }
  }
}

void LockedDoor::onCollide(Entity* entity)
{
  if (solid && !opening)
  {
    if (entity->hasTag("key"))
    {
      opening = true;
      rsprite.getAnim().play();
      entity->health = -1;
    }
    else if (entity->getName() == "player")
    {
      Player* player = (Player*)entity;
      if (player->holding != NULL && player->holding->hasTag("key"))
      {
        opening = true;
        rsprite.getAnim().play();

        // probably needs to be wrapped in a function
        delete player->holding;
        player->holding = NULL;
        player->walkProp = "";
        player->rsprite.setAnim(player->facingToString());
        player->state = Player::State::Walking;
      }
    }
  }
}
