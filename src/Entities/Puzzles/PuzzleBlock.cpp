#include "PuzzleBlock.h"
#include "../../Game.h"

PuzzleBlock::PuzzleBlock(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(12, 16), pos)
{
  useCorrections = true;

  originalPos = pos;

  rsprite.setSize(sf::Vector2f(16, 16));
  //rsprite.setOrigin(8, 9);
  rsprite.setTexture(game->textures.getTexture("res/entities.png"));
  rsprite.setOffset(sf::Vector2u(56, 8));

  addTag("pushable");
  //addTag("shrinkable");
  addTag("puzzle_element");

  solidFilter |= Tile::Puzzle;

  resolveIntersect();

}

std::string PuzzleBlock::getName() { return "puzzle_block"; }

void PuzzleBlock::onMessage(Message msg)
{
  if(msg.name == "pressure_plate_pressed")
  {
    sf::Vector2f pos = getPosition();
    if(pos != originalPos)
    {
      setPosition(originalPos);

      game->addParticles(pos, 53, 3, 3, 0.3, 0.5, 1.5);
      game->addParticlesInCloud(pos, 16.0f, 53, 3, 2);

      game->addParticles(originalPos, 53, 3, 3, 0.3, 0.5, 1.5);
      game->addParticlesInCloud(originalPos, 16.0f, 53, 3, 2);

    }

  }

}
