#include "Crystal.h"
#include "../Game.h"
#include "../Util/Maths.h"
#include "CrystalBullet.h"
#include "Shard.h"

Crystal::Crystal(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(12, 16), pos)
{
  rsprite.setSize(sf::Vector2f(16, 32));
  rsprite.setFrameSize(sf::Vector2u(8, 16));
  rsprite.setTexture(game->textures.getTexture("res/crystal.png"));

  rsprite.addAnim("hover", {0, 1, 2, 1});
  rsprite.addAnim("idle", {1, 3, 3, 4, 3, 4, 3, 3, 1}, 0.15, false);
  rsprite.addAnim("attack", {1, 3, 3, 4, 5, 6, 7, 8, 4, 3, 1}, 0.1, false);
  rsprite.addAnim("teleport", {1, 9, 10, 11, 11, 10, 9, 1}, 0.1, false);
  rsprite.setAnim("hover").play();

  setZ(1);

  addTag("pushable");
  //addTag("stunnable");
  addTag("creature");
  addTag("glass");
  addTag("shrinkable");

  state = 0;

  setMaxHealth(1);
}

std::string Crystal::getName() { return "crystal"; }

void Crystal::onDie()
{
  game->addAnimatedParticles(getPosition(), 56, 5, 3, 1.0f, 0.5, 1.5);

  Pickupable* e = new Shard(game, getPosition(), "blue");
  e->throwMe(unitVec(qrandIn(0, pi * 2)) * qrandIn(1.0f, 3.0f));
  game->addEntity(e);
}

void Crystal::onCollideMap()
{
  /*if (stunned)
  {
    health = -1;
  }*/
}

void Crystal::onTick()
{
  sf::Vector2f pos = getPosition();

  if (state == 0 && game->ticks % 5 == 0)
  {
    std::vector<Entity*> entities = game->getEntitiesInRadius(getPosition(),
      64.0f, Game::withName({"player"}));

    if (entities.size() > 0)
    {
      Entity* player = entities[0];
      state = 2;
      rsprite.setAnim("attack").play();
      target = player->getPosition();
      shot = false;
    }
  }

  if (state == 0)
  {
    if (actionTime.getElapsedTime().asSeconds() > 1.5)
    {
      int r = qrand() % 2;
      if (r == 0)
      {
        rsprite.setAnim("idle").play();
        state = 1;
      }
      actionTime.restart();
    }
  }
  else if (state == 1 && !rsprite.getAnim().isPlaying())
  {
    rsprite.setAnim("hover").play();
    state = 0;
    actionTime.restart();
  }
  else if (state == 2)
  {
    if (!rsprite.getAnim().isPlaying())
    {
      state = 0;
      actionTime.restart();
    }

    if (!shot && rsprite.getAnim().getFrame() == 8)
    {
      shot = true;
      game->addEntity(new CrystalBullet(this, pos, unitVec(target - pos)));
    }
  }
}
