#ifndef SHARD_H_
#define SHARD_H_

#include "Pickupable.h"
#include <functional>

class Player;

typedef std::function<void(Player*)> Power;

class Shard : public Pickupable
{
public:
  Shard(Game* game, sf::Vector2f pos, std::string type); // red, yellow, green, blue, purple

  std::string getName();

  void onTick();

  std::string type;

  Power power;

  int uses;
  bool hasPower;
};

void noPower(Player*);
void redPower(Player*);

#endif /*SHARD*/