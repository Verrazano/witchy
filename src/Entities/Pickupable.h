#ifndef PICKUPABLE_H_
#define PICKUPABLE_H_

#include "../Entity.h"

class Pickupable : public Entity
{
public:
  Pickupable(Game* game, sf::Vector2f size, sf::Vector2f pos);

  // returns true if hitting wall
  bool updateThrown();

  void pickup();
  // TODO: fix shitty shit with upvel
  void throwMe(sf::Vector2f vel, float upVel = 0);

  virtual void onPickup();
  virtual void onThrown();
  virtual void onLand();

  bool doesCollide(Entity* other);

  void reset();

  std::string carriedAnim;

  bool bigShadow;
  sf::Vector2f origin;
  float height;
  sf::Vector2f vel;
  bool thrown;
  float up;
  int origZ;
  Entity* thrower;
};

#endif /*PICKUPABLE*/
