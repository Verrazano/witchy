#ifndef READABLE_H_
#define READABLE_H_

#include "../Entity.h"

class Readable
{
public:
  Readable(Entity* e);

  struct Dialog
  {
    std::vector<std::string> texts;

    int endState; //0 question, 1 skipto, 2 end state, 3 end, 4 cutscene
    std::string target;

    std::string option1;
    std::string target1;

    std::string option2;
    std::string target2;

  };

  void set(std::map<std::string, Dialog> tree, std::string name = "");

  void setBubbleOrigin(sf::Vector2f origin);

  void onTick();
  void onMessage(Entity::Message msg);

  void setReadRadius(float r);

  std::string current_state;

  std::map<std::string, Dialog> tree;
  int dIndex;
  int waitQuestion;

  Entity* e;

  std::string text;
  std::string name;

  sf::RectangleShape bubble;

  bool reading;
  unsigned int playerId;
  sf::Clock timer;
  int index;
  float radius;
};

#endif /*READABLE*/
