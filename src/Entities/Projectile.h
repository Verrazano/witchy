#ifndef PROJECTILE_H_
#define PROJECTILE_H_

#include "../Entity.h"

class Projectile : public Entity
{
public:
  Projectile(Entity* shooter, sf::Vector2f dir, sf::Vector2f pos, sf::Vector2f size);

  void onTick();

  bool doesCollide(Entity* other);
  virtual bool doesProjectileCollide(Entity* other);
  void onCollide(Entity* other);

  void reflect(Entity* newShooter);

  int damage;  // defaults 1
  float speed; // defaults to 4
  sf::Vector2f dir;
  Entity* shooter;
};

#endif /*PROJECTILE*/