#include "Chest.h"
#include "../Game.h"
#include "Entities.h"
#include <iostream>

Chest::Chest(Game* game, sf::Vector2f pos, bool big, std::string item)
    : Entity(game, sf::Vector2f(16, 16), pos), item(item), big(big)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/chest.png"));
  rsprite.addAnim("open", {0, 1, 2, 3}, 0.13f, false);
  rsprite.setAnim("open");
  if (big)
  {
    rsprite.setOffset(sf::Vector2u(0, 8));
  }

  addTag("container");

  opened = false;
}

std::string Chest::getName() { return big ? "big_chest" : "small_chest"; }

void Chest::onTick()
{
  if (item != "nothing" && rsprite.getAnim().getFrame() == 2)
  {
    sf::Vector2f pos = getPosition();
    Pickupable* p = NULL;
    if (item == "frog")
      p = new Frog(game, pos);
    /*else if (Potion::getParticleIndex(item) != -1)
      p = new Potion(game, pos, item);*/
    else if (item == "rock")
      p = new Rock(game, pos + sf::Vector2f(0, 12));
    else if (item == "key")
      p = new Key(game, pos);
    else if (item.find("_shard") != -1)
      p = new Shard(game, pos, item.substr(0, item.find("_shard")));
    else if (item.find("flower") != -1)
      p = new Flower(game, pos, item.substr(item.find("_") + 1), true);
    else if(item == "gnome")
      p = new Gnome(game, pos);
    else if(item == "mushroom")
      p = new Mushroom(game, pos);

    item = "nothing";

    if (p != NULL)
    {
      // p->pickup();
      p->thrower = this;
      p->pickup();
      p->throwMe(sf::Vector2f(0, 2));
      game->addEntity(p);
    }
  }
}

void Chest::onCollide(Entity* entity)
{
  std::string name = entity->getName();
  if (!opened && (name == "player" || name == "gnome"))
  {
    player = entity;
    rsprite.getAnim().play();
    opened = true;
  }
}
