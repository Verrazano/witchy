#ifndef ZONE_H_
#define ZONE_H_

#include "../../Entity.h"
#include "ScriptedEvents.h"
#include <SFML/Graphics.hpp>

class Marker;

class Zone : public Entity
{
public:
  Zone(Game* game, sf::Vector2f pos, sf::Vector2f size, std::string enterScript = "nothing",
       std::string leaveScript = "nothing", std::string scriptData = "nothing");

  std::string getName();

  void onTick();

  void onDeath(Entity* entity);

  void onCreate(Entity* entity);

  std::vector<Marker*> getMarkers();
  std::vector<Marker*> getMarkersWithTag(std::string tag);

  sf::RectangleShape debug;

  static int nextColor;
  static std::vector<sf::Color> zoneColors;

  // sf::FloatRect zoneBounds;
  bool current;

  bool markersChecked;
  std::vector<Marker*> markers;

  std::string scriptData;

  ScriptedEvents events;
};

#endif /*ZONE*/
