#include "MapTeleport.h"
#include "../../Game.h"
#include "../../Util/Strings.h"

MapTeleport::MapTeleport(Game* game, sf::Vector2f pos, std::string map, sf::Vector2f target, std::string tagRequired)
    : Entity(game, sf::Vector2f(12, 12), pos), map(map), target(target), tagRequired(tagRequired)
{
}

std::string MapTeleport::getName() { return "map_teleport"; }

void MapTeleport::onCollide(Entity* e)
{
  bool tagged = tagRequired == "none" || e->hasTag(tagRequired);
  if ((e->getName() == "player" || e->getName() == "character") && tagged)
  {
    Properties p;
    p.setString("map", map);
    p.setFloat("x", target.x);
    p.setFloat("y", target.y);

    e->sendMessage(this, "map_teleport", p);
  }
}
