#ifndef MARKER_H_
#define MARKER_H_

#include "../../Entity.h"

// TODO: maybe not make this stuff entities, because it probably really doesn't
// need to be
class Marker : public Entity
{
public:
  Marker(Game* game, sf::Vector2f pos, std::string tags);

  std::string getName();

  void onTick();
};

#endif /*MARKER*/