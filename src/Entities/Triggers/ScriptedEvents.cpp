#include "ScriptedEvents.h"
#include "../../Game.h"
#include "../Player/Player.h"
#include "../Creatures/Slime.h"
#include "Marker.h"
#include "Zone.h"
#include <iostream>

ScriptedEvents::ScriptedEvents(std::string enterScript, std::string leaveScript)
{
  onDeath = nothingOnDeath;
  onCreate = nothingOnCreate;

  if (enterScript == "createEnemies")
    onEnter = createEnemies;
  else if (enterScript == "closeAndSpawn")
  {
    onEnter = closeAndSpawn;
    onDeath = openOnKill;
  }
  else if (enterScript == "areaTitle")
    onEnter = areaTitle;
  else
    onEnter = nothingOnEnter;

  if (leaveScript == "destroyEnemies")
    onLeave = destroyEnemies;
  else
    onLeave = nothingOnLeave;
}

void nothingOnEnter(Zone* zone, Player* player)
{
  // std::cout << "nothingOnEnter\n";
}

void nothingOnLeave(Zone* zone, Player* player)
{
  // std::cout << "nothingOnLeave\n";
}

void nothingOnDeath(Zone* zone, Entity* other) {}

void nothingOnCreate(Zone* zone, Entity* other) {}

void createEnemies(Zone* zone, Player* player)
{
  std::vector<Marker*> markers = zone->getMarkers();
  Game* game = zone->game;

  int& count = zone->props.getInt("count");
  count = 0;

  for (int i = 0; i < markers.size(); i++)
  {
    sf::Vector2f pos = markers[i]->getPosition();
    Marker* m = markers[i];

    int& id = m->props.getInt("myEntity");

    if (id == 0)
    {
      /*if (m->hasTag("purple_slime"))
      {
        Slime* slime = new Slime(game, pos, "purple", true);
        id = game->addEntity(slime);
        slime->respawn();
      }
      else if (m->hasTag("red_slime"))
      {
        Slime* slime = new Slime(game, pos, "red", true);
        id = game->addEntity(slime);
        slime->respawn();
      }*/
    }

    if (id != 0)
    {
      count++;
    }
  }
}

void destroyEnemies(Zone* zone, Player* player)
{
  // std::cout << "destroyEnemies\n";

  std::vector<Marker*> markers = zone->getMarkers();
  Game* game = zone->game;

  for (int i = 0; i < markers.size(); i++)
  {
    Marker* m = markers[i];
    int& myEntity = m->props.getInt("myEntity");
    if (myEntity != 0)
    {
      game->removeEntityWithId(myEntity);
      myEntity = 0;
    }
  }
}

void closeAndSpawn(Zone* zone, Player* player)
{
  // std::cout << "closeAndSpawn\n";
  Properties& props = zone->props;

  bool& challenged = props.getBool("challenged");
  if (!challenged)
  {
    challenged = true;

    Game* game = zone->game;
    game->registerListener(zone);
    sf::FloatRect bounds = zone->getBounds();
    bounds.left -= 16;
    bounds.top -= 16;
    bounds.width += 16;
    bounds.height += 16;
    std::vector<Entity*> entities = game->getEntitiesInBox(bounds, Game::withName({"gate"}));
    for (int i = 0; i < entities.size(); i++)
    {
      entities[i]->sendMessage(zone, "goUp");
    }

    createEnemies(zone, player);
  }
}

void openOnKill(Zone* zone, Entity* other)
{
  std::string name = other->getName();
  if (name != "purple_slime" && name != "red_slime" && name != "grey_slime")
    return;

  int& count = zone->props.getInt("count");
  count--;

  if (count <= 0)
  {
    Game* game = zone->game;
    game->unregisterListener(zone);

    sf::FloatRect bounds = zone->getBounds();
    bounds.left -= 16;
    bounds.top -= 16;
    bounds.width += 16;
    bounds.height += 16;
    std::vector<Entity*> entities = game->getEntitiesInBox(bounds, Game::withName({"gate"}));
    for (int i = 0; i < entities.size(); i++)
    {
      entities[i]->sendMessage(zone, "goDown");
    }
  }
}

void areaTitle(Zone* zone, Player* player) { zone->game->displayAreaTitle(zone->scriptData); }
