#ifndef GATE_H_
#define GATE_H_

#include "../../Entity.h"
#include "../../Util/Anim.h"

class Gate : public Entity
{
public:
  Gate(Game* game, sf::Vector2f pos, bool up = false);

  std::string getName();

  void onTick();

  void onMessage(Message msg);
};

#endif /*GATE*/