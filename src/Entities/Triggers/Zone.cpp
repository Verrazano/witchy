#include "Zone.h"
#include "../../Game.h"
#include "../../Util/Strings.h"
#include "Marker.h"
#include <iostream>

int Zone::nextColor = 0;
std::vector<sf::Color> Zone::zoneColors;

Zone::Zone(Game* game, sf::Vector2f pos, sf::Vector2f size, std::string enterScript, std::string leaveScript,
           std::string scriptData)
    : Entity(game, sf::Vector2f(size.x * 16, size.y * 16), pos), scriptData(trim(scriptData))
{
  static bool loaded = false;
  if (!loaded)
  {
    loaded = true;
    zoneColors.push_back(sf::Color(255, 0, 0, 100));
    zoneColors.push_back(sf::Color(0, 255, 0, 100));
    zoneColors.push_back(sf::Color(0, 0, 255, 100));
    zoneColors.push_back(sf::Color(255, 255, 0, 100));
    zoneColors.push_back(sf::Color(255, 0, 255, 100));
    zoneColors.push_back(sf::Color(0, 255, 255, 100));
  }

  /*zoneBounds.top = pos.y;
  zoneBounds.left = pos.x;
  zoneBounds.width = size.x;
  zoneBounds.height = size.y;*/

  debug.setPosition(pos.x - 8, pos.y - 8);
  debug.setSize(size * 16.0f);
  setPosition(debug.getPosition() + debug.getSize() / 2.0f);
  debug.setFillColor(zoneColors[nextColor % zoneColors.size()]);
  nextColor++;

  solid = false;
  current = false;
  markersChecked = false;

  events = ScriptedEvents(enterScript, leaveScript);
}

std::string Zone::getName() { return "zone"; }

void Zone::onTick()
{
  if (game->config.debug)
  {
    // game->topLayer.push_back(&debug);
  }
}

void Zone::onDeath(Entity* other)
{
  if (other->getBounds().intersects(getBounds()))
  {
    events.onDeath(this, other);
  }
}

void Zone::onCreate(Entity* other)
{
  if (other->getBounds().intersects(getBounds()))
  {
    events.onCreate(this, other);
  }
}

std::vector<Marker*> Zone::getMarkers()
{
  if (!markersChecked)
  {
    markersChecked = true;
    std::vector<Entity*> entities = game->getEntitiesInBox(getBounds(), Game::withName({"marker"}));
    for (int i = 0; i < entities.size(); i++)
    {
      markers.push_back((Marker*)entities[i]);
    }
  }

  return markers;
}

std::vector<Marker*> Zone::getMarkersWithTag(std::string tag)
{
  getMarkers();
  std::vector<Marker*> found;
  for (int i = 0; i < markers.size(); i++)
  {
    if (markers[i]->hasTag(tag))
    {
      found.push_back(markers[i]);
    }
  }

  return found;
}
