#ifndef SCRIPTEDEVENTS_H_
#define SCRIPTEDEVENTS_H_

#include <functional>
#include <string>

class Zone;
class Player;
class Entity;

// TODO: change this to allow for both triggers and zones
// maybe make superclass for zone and teleport to inherit from
class ScriptedEvents
{
public:
  ScriptedEvents(std::string enterScript = "nothing", std::string leaveScript = "nothing");

  std::function<void(Zone*, Player*)> onEnter;
  std::function<void(Zone*, Player*)> onLeave;
  // maybe on message?

  std::function<void(Zone*, Entity*)> onDeath;
  std::function<void(Zone*, Entity*)> onCreate;
};

void nothingOnEnter(Zone* zone, Player* player);
void nothingOnLeave(Zone* zone, Player* player);
void nothingOnDeath(Zone* zone, Entity* other);
void nothingOnCreate(Zone* zone, Entity* other);

void createEnemies(Zone* zone, Player* player);
void destroyEnemies(Zone* zone, Player* player);

void closeAndSpawn(Zone* zone, Player* player);
void openOnKill(Zone* zone, Entity* other);

void areaTitle(Zone* zone, Player* player);

#endif /*SCRIPTEDEVENTS*/
