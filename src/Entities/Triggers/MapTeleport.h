#ifndef MAPTELEPORT_H_
#define MAPTELEPORT_H_

#include "../../Entity.h"

class MapTeleport : public Entity
{
public:
  MapTeleport(Game* game, sf::Vector2f pos, std::string map, sf::Vector2f target, std::string tagRequired = "none");

  std::string getName();

  void onCollide(Entity* e);

  std::string map;
  sf::Vector2f target;
  std::string tagRequired;
};

#endif /*MAPTELEPORT*/
