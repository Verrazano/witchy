#include "Marker.h"
#include "../../Game.h"

Marker::Marker(Game* game, sf::Vector2f pos, std::string tags) : Entity(game, sf::Vector2f(16, 16), pos)
{
  solid = false;
  sprite.setFillColor(sf::Color::Transparent);

  // std::cout << "tags: \"" << tags << "\"\n";
  while (tags.find(" ") != std::string::npos)
  {
    std::string tag = tags.substr(0, tags.find(" "));
    tags = tags.substr(tags.find(" ") + 1);

    addTag(tag);
  }

  addTag(tags);
}

std::string Marker::getName() { return "marker"; }

void Marker::onTick() {}