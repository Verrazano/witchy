#include "Gate.h"
#include "../../Game.h"
#include <iostream>

Gate::Gate(Game* game, sf::Vector2f pos, bool up) : Entity(game, sf::Vector2f(16, 16), pos)
{
  rsprite.setTexture(game->textures.getTexture("res/entities.png"));

  Anim goUp("goUp");
  goUp.setLooping(false);
  goUp.addFrame(46);
  goUp.addFrame(47);
  goUp.addFrame(48);

  rsprite.addAnim(goUp);

  Anim goDown("goDown");
  goDown.setLooping(false);
  goDown.addFrame(48);
  goDown.addFrame(47);
  goDown.addFrame(46);

  rsprite.addAnim(goDown);

  solid = up;
  std::string anim = solid ? "goDown" : "goUp";
  rsprite.setAnim(anim);
}

std::string Gate::getName() { return "gate"; }

void Gate::onTick() {}

void Gate::onMessage(Message msg)
{
  if (msg.name == "goUp")
  {
    rsprite.setAnim("goUp");
    rsprite.getAnim().play();
    solid = true;
  }
  else if (msg.name == "goDown")
  {
    rsprite.setAnim("goDown").play();
    solid = false;
  }
  else if (msg.name == "flip")
  {
    solid = !solid;

    std::string anim = solid ? "goUp" : "goDown";
    rsprite.setAnim(anim).play();
  }
}