#include "Recipe.h"
#include "../../Game.h"
#include "../Entities.h"

std::vector<Recipe> Recipe::recipies;

void Recipe::initRecipies(Game* game)
{
  sf::Vector2f pos(8, 8);

  recipies.push_back(Recipe([=]() { return new Potion(game, pos, "shrink"); }, "gnome_cap", "mushroom"));

}
