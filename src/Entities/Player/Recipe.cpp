#include "Recipe.h"
#include "../../Entity.h"
#include <iostream>

Recipe::Recipe(std::function<Entity*(void)> result) : result(result) { makesMultiple = false; }

Recipe::Recipe(std::function<Entity*(void)> result, std::string ingredient) : result(result)
{
  makesMultiple = false;
  addReq(ingredient, 2);
}

Recipe::Recipe(std::function<Entity*(void)> result, std::string ingredient1, std::string ingredient2)
    : result(result)
{
  makesMultiple = false;
  addReq(ingredient1, 1);
  addReq(ingredient2, 1);
}

Recipe::Recipe(std::function<std::vector<Entity*>(void)> results) : results(results) { makesMultiple = true; }

void Recipe::addReq(std::string name, int count) { ingredients[name] = count; }

bool Recipe::hasReqs(std::map<std::string, int>& ing)
{
  for (auto& kv : ingredients)
  {
    if (!(ing[kv.first] >= kv.second))
    {
      return false;
    }
  }

  return true;
}
