#include "Player.h"
#include "../../Game.h"
#include "../../Util/Maths.h"
#include "../Pickupable.h"
#include "../Projectile.h"
#include "../Shard.h"

void Player::onTick()
{
  c.update();

  if (updateTeleporting())
  {
    return;
  }

  bool left = !lockControls && c.pressing("left");
  bool right = !lockControls && c.pressing("right");
  bool up = !lockControls && c.pressing("up");
  bool down = !lockControls && c.pressing("down");

  // hit, summon
  bool action1 = !lockControls && c.justPressed("action1");
  bool action1Pressing = !lockControls && c.pressing("action1");
  bool action1Released = !lockControls && c.justReleased("action1");

  if(action1Pressing)
  {
    cauldronTicks++;

  }
  else if(action1Released)
  {
    breakCauldron();

  }

  // pickup, throw
  bool action2 = !lockControls && c.justPressed("action2");

  sf::Vector2f pos = getPosition();

  //TODO: add a cool animation/sfx to hoping down ledges
  if(onLedge > 0)
  {
    sf::FloatRect ledgeBox = getBounds();
    ledgeBox.height += 6;
    if(game->map.isOnLedge(ledgeBox, getPosition()))
    {
        move(0, 2);
    }
    else
    {
      move(0, 4);
      onLedge = 0;
      solidFilter = Tile::Flag::Solid | Tile::Flag::Death | Tile::Flag::Ledge;
      resolveIntersect();
      jumplandSound.play();
      sprite.setFillColor(sf::Color::Transparent);
    }

  }
  else if(state == State::Fanfare)
  {
    fanfareTicks--;
    if(fanfareTicks <= 0)
    {
      state = State::Walking;
      fanfareTicks = 0;
      walkProp = "";
      rsprite.setAnim(walkProp + facingToString()).play();
    }

  }
  else if (!lockControls && (state == State::Walking || state == State::Carrying || state == State::Brewing))
  {
    // create the unit vec representing the direction the player wants to go
    sf::Vector2f m;

    if (left)
    {
      m.x -= 1.0f;
    }

    if (right)
    {
      m.x += 1.0f;
    }

    if (up)
    {
      m.y -= 1.0f;
    }

    if (down)
    {
      m.y += 1.0f;
    }

    m = unitVec(m);
    float mlen = mag(m);

    updateFacing(m);

    if (mlen > 0.0f)
    {
      float speed = (state == State::Walking ? walkSpeed : carrySpeed) * speedFactor;

      canPush = true;
      move(m * speed);
      canPush = false;

      if(didMove && m.y > 0) {
        sf::FloatRect ledgeBox = getBounds();
        ledgeBox.height += 6;
        if(game->map.isOnLedge(ledgeBox, getPosition())) {
          solidFilter = Tile::Flag::Solid | Tile::Flag::Death;
          //useCorrections = false;
          sprite.setFillColor(sf::Color::White);
          onLedge = 7;
        }
      }

      if (didMove && game->ticks % 10 == 0)
      {
        sf::Vector2f pos = getPosition() + sf::Vector2f((qrand() % 8) - 4.0f, 8.0f);
        if (dir == Facing::Down || dir == Facing::DownLeft || dir == Facing::DownRight)
        {
          pos.y -= 22.0f;
        }

        if (isRightSide())
        {
          pos.x -= 4.0f;
        }
        else if (isLeftSide())
        {
          pos.x += 4.0f;
        }

        Map& map = game->map;
        Tile t = map.getTile(map.getTileIndex(game->map.toTileSpace(getPosition())));
        SoundEffect* stepSfx = &leavesStepSound;
        if (t.flags & Tile::Flag::Gravel)
        {
          stepSfx = &gravelStepSound;
        }
        else if (t.flags & Tile::Flag::Leaves)
        {
          stepSfx = &leavesStepSound;
        }
        else if (t.flags & Tile::Flag::Stone)
        {
          stepSfx = &stoneStepSound;
        }
        else if (t.flags & Tile::Flag::Wood)
        {
          stepSfx = &woodStepSound;
        }

        if (stepSfx->getStatus() == sf::Sound::Stopped)
        {
          stepSfx->setPitch(1.0f + (qrand() % 1000) / 1000.f - 0.25f);
          stepSfx->play();
        }

        game->addParticle(pos, sf::Vector2f(0.0f, -0.3f), 30, 0.8f);
      }

      updateZone();

      if (facing != lastFacing)
      {
        rsprite.setAnim(walkProp + facingToString()).play();
      }
      else if (!rsprite.getAnim().isPlaying())
      {
        Anim& a = rsprite.setAnim(walkProp + facingToString());
        a.play();
        a.setIndex(1);
        rsprite.resetTextureRect();
      }
    }
    else if (rsprite.getAnim().isPlaying())
    {
      rsprite.getAnim().setPlaying(false);
      rsprite.getAnim().reset();
      rsprite.resetTextureRect();
    }

    if (state == State::Walking)
    {
      if(hasCauldron && action1Pressing && cauldronTicks > 15)
      {
        walkProp = "carry";
        rsprite.setAnim(walkProp + facingToString());
        state = State::Brewing;

        keyitemSprite.setAnim("cauldron").play();
        keyitemSprite.setOffset(sf::Vector2u(0, 8*9));

        sf::Vector2f pos = getPosition() + sf::Vector2f(0, -14);
        game->addParticles(pos, 53, 3, 4, 0.3, 0.5, 1.5);
        game->addParticlesInCloud(pos, 16.0f, 53, 3, 3);

      }
      else if (action1) // hit
      {
        if(read())
        {
          return;
        }

        if(hasStaff) {
          swingSound.play();

          if (isRightSide())
          {
            rsprite.setAnim("hitright").play();
          }
          else
          {
            rsprite.setAnim("hitleft").play();
          }

          state = State::Hitting;
        }
      }
      else if (action2) // pickup
      {
        std::vector<Entity*> entities;
        entities = game->getEntitiesInRadius(pos, 16.0f);

        target = NULL;
        float minDist = 0.0f;

        for (Entity* entity : entities)
        {
          float d = dist(entity->getPosition(), pos);
          if (entity->hasTag("pickupable") && (target == NULL || d < minDist))
          {
            minDist = d;
            target = entity;
          }
        }

        if (target != NULL)
        {
          state = State::Picking;
          rsprite.setAnim("pickup").play();
        }
      }
    }
    else if (state == State::Carrying)
    {
      if (action2) // throw, this might not be a great idea we'll see later
      {
        throwHeld(lightThrowPower);
        rsprite.setAnim(facingToString());
        state = State::Walking;
      }
      else if (action1) // if shard use
      {
        if(read())
        {
          return;

        }

        if (holding->getName().find("_shard") != -1)
        {
          if (((Shard*)holding)->hasPower)
          {
            ((Shard*)holding)->power(this);
            ((Shard*)holding)->uses--;
            if (((Shard*)holding)->uses == 0)
            {
              delete holding;
              holding = NULL;
              walkProp = "";
              rsprite.setAnim(facingToString());
              state = Player::State::Walking;
            }
          }
        }
        else if (holding != NULL)
        {
          state = State::SettingDown;
          rsprite.setAnim("setdown").play();
        }
      }
    }
    else if(state == State::Brewing)
    {
      int maxIngredients = 3;
      bool cauldronFull = cauldronEntities.size() >= maxIngredients;

      sf::Vector2f ppos = getPosition();
      if (game->ticks % 15 == 0)
      {
        sf::Vector2f pos = ppos + sf::Vector2f(0, -16.0f);
        game->addParticles(pos, 40, 6, 1, 1.f, 0.5, 1.0f, -pi / 6.0f, -5 * pi / 6.0f);

      }

      float grabRadius = 64.0f;

      if(!cauldronFull && game->ticks%5 == 0)
      {
        sf::Vector2f pos = ppos + unitVec(qrandIn(0, 2*pi))*(grabRadius-8.0f);
        sf::Vector2f v = unitVec(ppos - pos)*2.0f;
        sf::IntRect f = getFrame(30, sf::Vector2u(8, 8), game->particleTexSize);
        game->particles.addParticle(pos, v, f, 0.4f);

      }

      if(!cauldronFull)
      {
        std::vector<Entity*> entities = game->getEntitiesInRadius(getPosition(), grabRadius, Game::withTag({"ingredient", "suckable"}));
        for(Entity* e : entities)
        {
          sf::Vector2f v = ppos - e->getPosition();
          float m = mag(v);
          if(!cauldronFull && m < 16.0f && !e->hasTag("suckable"))
          {
            if (e->hasTag("pickupable"))
            {
              Pickupable* p = (Pickupable*)e;
              p->pickup();
              p->reset();

            }

            ingredients[e->getName()]++;

            game->removeEntityWithId(e->id);

            cauldronEntities.push_back(e);
            cauldronFull = cauldronEntities.size() >= maxIngredients;

            int color = e->props.getInt("potionColor");
            if (color != 0 && game->shadersAvailable)
            {
              potionShader.setParameter("pColor", hexToColor(color));
            }

          }
          else
          {
            if(e->hasTag("pickup_first"))
            {
              Pickupable* p = (Pickupable*)e;
              p->pickup();
              p->reset();
              e->remTag("pickup_first");

            }
            v = unitVec(v)*(1-m/grabRadius)*3.0f;

            if(e->hasTag("suckable"))
            {
              e->props.setFloat("suckable:forceX", v.x);
              e->props.setFloat("suckable:forceY", v.y);
              e->props.setFloat("suckable:x", pos.x);
              e->props.setFloat("suckable:y", pos.y);

            }
            else
            {
              e->move(v);

            }

          }

        }

      }

    }

  }
  else if (state == State::Hitting || state == State::Hit)
  {
    sf::Vector2f fVec = getDirVec();
    sf::FloatRect box(pos.x + fVec.x * 16 - 12, pos.y + fVec.y * 16 - 12, 24.0f, 24.0f);

    if (game->config.debug)
    {
      hitRegion.setSize(sf::Vector2f(box.width, box.height));
      hitRegion.setPosition(box.left, box.top);
      hitRegion.setFillColor(sf::Color::Transparent);
      hitRegion.setOutlineColor(sf::Color::Red);
      hitRegion.setOutlineThickness(0.25f);
      game->topLayer.push_back(&hitRegion);
    }

    if (state == State::Hitting && rsprite.getAnim().getIndex() == 1)
    {
      std::vector<Entity*> entities;
      entities = game->getEntitiesInBox(box, Game::withTag({"stunnable", "projectile", "player_breakable"}));

      for (Entity* entity : entities)
      {
        if (entity->hasTag("player_breakable"))
        {
          entity->hit(1);
          // this particle doesn't look good when breaking pots
          // game->addAnimatedParticles(entity->getPosition(), 48, 5, 1, 0.4f,
          // 0, 0);
        }
        else if (entity->hasTag("projectile"))
        {
          Projectile* proj = (Projectile*)entity;
          proj->reflect(this);
        }
        else
        {
          sf::Vector2f ePos = entity->getPosition();
          sf::Vector2f dir = unitVec(ePos - pos);

          Properties stunProps;
          stunProps.setFloat("power", hitPower);
          stunProps.setFloat("dirX", dir.x);
          stunProps.setFloat("dirY", dir.y);

          entity->sendMessage(this, "stun", stunProps);

          // pos, frame, frames, count, life, powerMin, powerMax
          game->addAnimatedParticles(ePos, 48, 5, 1, 0.4f, 0, 0);
        }
      }

      state = State::Hit;
    }
    else if (state == State::Hit && !rsprite.getAnim().isPlaying())
    {
      // hitting can only come from walking
      rsprite.setAnim(facingToString());
      state = State::Walking;
    }
  }
  else if (state == State::Picking)
  {
    if (holding == NULL && rsprite.getAnim().getIndex() == 2)
    {
      holding = target;
      target = NULL;

      game->removeEntityWithId(holding->id);

      Pickupable* p = (Pickupable*)holding;
      p->pickup();
      holdingSprite = holding->rsprite;
    }
    else if (!rsprite.getAnim().isPlaying())
    {
      walkProp = "carry";
      rsprite.setAnim(walkProp + facingToString());
      state = State::Carrying;
    }
  }
  else if (state == State::SettingDown)
  {
    if (holding != NULL && rsprite.getAnim().getIndex() == 2)
    {
      setDownHeld();
    }
    else if (!rsprite.getAnim().isPlaying())
    {
      walkProp = "";
      rsprite.setAnim(facingToString());
      state = State::Walking;
    }
  }
  else if (state == State::Stunned)
  {
    state = State::Walking;
    rsprite.setAnim(facingToString());
  }

  updateCamera();
}
