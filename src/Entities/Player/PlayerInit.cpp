#include "Player.h"
#include "../../Game.h"
#include "Recipe.h"
#include <iostream>

Player::Player(Game* game, sf::Vector2f pos, bool hasAbilities) : Entity(game, sf::Vector2f(12, 12), pos)
{
  sf::Texture* witchTex = game->textures.getTexture("res/witch.png");

  // initialize sprite and anims
  rsprite.setSize(sf::Vector2f(16, 32));
  rsprite.setOrigin(8, 20);
  rsprite.setTexture(witchTex);
  rsprite.setFrameSize(sf::Vector2u(8, 16));

  rsprite.addAnim("down", {0, 1, 0, 2});
  rsprite.addAnim("right", {3, 4, 3, 5});
  rsprite.addAnim("up", {6, 7, 6, 8});
  rsprite.addAnim("left", {9, 10, 9, 11});

  rsprite.addAnim("carrydown", {12, 13, 12, 14});
  rsprite.addAnim("carryright", {15, 16, 15, 17});
  rsprite.addAnim("carryup", {18, 19, 18, 20});
  rsprite.addAnim("carryleft", {21, 22, 21, 23});

  rsprite.addAnim("pickup", {24, 25, 26, 12}, 0.15f, false);
  rsprite.addAnim("setdown", {12, 26, 25, 24, 0}, 0.15f, false);

  rsprite.addAnim("hitright", {27, 28, 29, 30}, 0.1f, false);
  rsprite.addAnim("hitleft", {31, 32, 33, 34}, 0.1f, false);

  rsprite.addAnim("stunned", {35, 36});

  // for shadow on ledges
  sprite.setSize(sf::Vector2f(16, 16));
  sprite.setOrigin(8, 0);
  sprite.setFillColor(sf::Color::Transparent);
  sprite.setTexture(game->textures.getTexture("res/entities.png"));
  sprite.setTextureRect(sf::IntRect(6 * 8, 0, 8, 8));

  keyitemSprite = Sprite(sf::Vector2f(16, 16), sf::Vector2u(8, 8), witchTex);
  keyitemSprite.setOffset(sf::Vector2u(0, 8*9));
  keyitemSprite.addAnim("cauldron", {0, 1, 2, 1}, 0.5f);

  cauldronPickup = NULL;

  if (game->shadersAvailable)
  {
    potionShader.loadFromFile("res/potion.frag", sf::Shader::Fragment);
    potionShader.setParameter("pColor", sf::Color(109, 170, 44));
    potionShader.setParameter("cColor", sf::Color(109, 170, 44));
  }

  Recipe::initRecipies(game);

  // initialize controls
  c.addButton("left", []() { return sf::Keyboard::isKeyPressed(sf::Keyboard::Left); });

  c.addButton("right", []() { return sf::Keyboard::isKeyPressed(sf::Keyboard::Right); });

  c.addButton("up", []() { return sf::Keyboard::isKeyPressed(sf::Keyboard::Up); });

  c.addButton("down", []() { return sf::Keyboard::isKeyPressed(sf::Keyboard::Down); });

  // hit, summon
  c.addButton("action1", []() { return sf::Keyboard::isKeyPressed(sf::Keyboard::Z); });

  // pick up, throw
  c.addButton("action2", []() { return sf::Keyboard::isKeyPressed(sf::Keyboard::X); });

  // entity configuration
  setZ(4);

  addTag("shrinkable");
  addTag("speedable");
  addTag("creature");
  addTag("scare_frog");
  addTag("heavy");

  // player variable initialization
  lockControls = false;

  if(hasAbilities)
  {
    hasStaff = true;
    hasCauldron = true;
  }
  else
  {
    hasStaff = false;
    hasCauldron = false;
    rsprite.setOffset(sf::Vector2u(0, 112));
  }

  dir = Facing::Down;
  lastFacing = Facing::None;
  facing = Facing::Down;
  state = State::Walking;

  lastDirVec = sf::Vector2f(0, 1);

  walkSpeed = 2.0f;
  carrySpeed = 2.0f;

  hitPower = 5.0f;

  onLedge = 0;

  target = NULL;

  holding = NULL;

  lightThrowPower = 16.0f * 4.0f / 12.0f;

  teleporting = false;
  blackout.setSize(game->config.wSize);
  blackout.setFillColor(sf::Color::Black);

  cameraTarget = pos;
  game->camera.setPosition(pos);
  currentZone = NULL;

  canPush = false;

  setMaxHealth(3);

  sf::Texture* texture = game->textures.getTexture("res/hearts.png");
  sf::Vector2u size = texture->getSize();

  float heartSize = 8 * game->config.scale;

  sf::RectangleShape heart(sf::Vector2f(heartSize, heartSize));
  // heart.setOrigin();
  heart.setTexture(texture);
  heart.setTextureRect(getFrame(0, sf::Vector2u(8, 8), size));

  for (int i = 0; i < maxHealth; i++)
  {
    heart.setPosition(heartSize * i + heartSize / 2, heartSize / 2);
    playerHearts.push_back(heart);
  }

  /*dirText = sf::Text("dir: " + dirToString(), game->consolas);
  dirText.setFillColor(sf::Color::Black);
  dirText.setPosition(5, 5);

  facingText = sf::Text("facing: " + facingToString(), game->consolas);
  facingText.setFillColor(sf::Color::Black);
  facingText.setPosition(5, 40);

  debugInfo = sf::Text("", game->consolas);
  debugInfo.setFillColor(sf::Color::Black);
  debugInfo.setPosition(5, 75);*/

  swingSound = SoundEffect("res/sounds/swing2.wav");
  swingSound.baseVolume = 0.2f;

  gravelStepSound = SoundEffect("res/sounds/steps/gravel.ogg");
  leavesStepSound = SoundEffect("res/sounds/steps/leaves01.ogg");
  stoneStepSound = SoundEffect("res/sounds/steps/stone01.ogg");
  woodStepSound = SoundEffect("res/sounds/steps/wood01.ogg");
  fanfareSound = SoundEffect("res/sounds/itemfanfare.wav");
  jumplandSound = SoundEffect("res/sounds/jumpland.wav");

  keepOnSwitch = true;

  cauldronTicks = 0;

}
