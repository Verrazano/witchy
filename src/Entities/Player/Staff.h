#ifndef STAFF_H_
#define STAFF_H_

#include "../../Entity.h"

class Staff : public Entity
{
public:
  Staff(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  bool doesCollide(Entity* e);
  void onCollide(Entity* e);

};

#endif /*STAFF*/
