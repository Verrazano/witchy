#include <functional>
#include <string>
#include <vector>
#include <map>

class Entity;
class Game;

class Recipe
{
public:
  Recipe(std::function<Entity*(void)> result);
  Recipe(std::function<Entity*(void)> result, std::string ingredient);
  Recipe(std::function<Entity*(void)> result, std::string ingredient1, std::string ingredient2);

  Recipe(std::function<std::vector<Entity*>(void)> results);

  void addReq(std::string name, int count);

  bool hasReqs(std::map<std::string, int>& ing);

  bool makesMultiple;

  std::function<Entity*(void)> result;
  std::function<std::vector<Entity*>(void)> results;
  std::map<std::string, int> ingredients;

  static std::vector<Recipe> recipies;
  static void initRecipies(Game* game);

};
