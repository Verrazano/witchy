#ifndef PLAYER_H_
#define PLAYER_H_

#include "../../Controller.h"
#include "../../Entity.h"
#include "../../SoundEffect.h"

class Zone;

class Player : public Entity
{
public:
  // listed in order of priority
  enum class Facing
  {
    None, // used for resetting lastFacing
    DownRight,
    DownLeft,
    UpRight,
    UpLeft,
    Down,
    Right,
    Up,
    Left

  };

  enum class State
  {
    Walking,
    Carrying,
    Hitting,
    Hit,
    Picking,
    SettingDown,
    Stunned,
    Brewing,
    Fanfare

  };

  Player(Game* game, sf::Vector2f pos, bool hasAbilities = true);

  // overridden function
  std::string getName();

  void onTick();

  void onCollide(Entity* e);

  void onMessage(Message msg);

  void onMapSwitch();

  void onHit();

  void draw(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

  // helper functions
  void throwHeld(float power);
  void setDownHeld();

  void updateCamera();

  void updateZone();

  bool updateTeleporting();

  void updateFacing(sf::Vector2f m);

  bool isRightSide();
  bool isLeftSide();

  sf::Vector2f getFacingVec();
  sf::Vector2f getDirVec();

  std::string dirToString();
  std::string facingToString();

  void breakCauldron();
  bool updateBrewing();

  void giveHat();

  bool read();

  // controls
  bool lockControls;
  Controller c;
  bool hasStaff;
  bool hasCauldron;

  // states
  Facing dir;
  Facing lastFacing;
  Facing facing;
  State state;

  sf::Vector2f lastDirVec;

  float walkSpeed;
  float carrySpeed;

  float hitPower;

  int onLedge;

  // for action 2
  Entity* target;

  Entity* holding;
  Sprite holdingSprite;
  Sprite keyitemSprite;

  std::string walkProp;
  float lightThrowPower;

  int fanfareTicks;

  // teleporting
  bool teleporting;
  sf::Vector2f teleportTarget;
  sf::RectangleShape blackout;
  std::string teleportMap;

  // camera tracking
  sf::Vector2f cameraTarget;
  Zone* currentZone;

  bool canPush;

  sf::Shader potionShader;
  int cauldronTicks;
  std::vector<Entity*> cauldronEntities;
  std::map<std::string, int> ingredients;
  Entity* cauldronPickup;

  // debug text
  sf::Text dirText;
  sf::Text facingText;
  sf::Text debugInfo;

  sf::RectangleShape hitRegion;

  std::vector<sf::RectangleShape> playerHearts;

  SoundEffect swingSound;

  SoundEffect gravelStepSound;
  SoundEffect leavesStepSound;
  SoundEffect stoneStepSound;
  SoundEffect woodStepSound;
  SoundEffect fanfareSound;
  SoundEffect jumplandSound;

};

#endif /*PLAYER*/
