#include "Staff.h"
#include "../../Game.h"

Staff::Staff(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(16, 16), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setOrigin(8, 8);
  rsprite.setTexture(game->textures.getTexture("res/witch.png"));
  rsprite.setOffset(sf::Vector2u(0, 8*11));
  rsprite.addAnim("main", {0}, false, 0.1f);

  setZ(4);

}

std::string Staff::getName()
{
  return "staff";

}

void Staff::onTick()
{
  if(game->ticks%17 == 0)
  {
    game->addParticles(getPosition(), 24, 6, 1, 0.4);
  }

}

bool Staff::doesCollide(Entity* e)
{
  bool isPlayer = e->getName() == "player";
  return isPlayer;

}

void Staff::onCollide(Entity* e)
{
  e->sendMessage(this, "pickup_staff");
  kill();

}
