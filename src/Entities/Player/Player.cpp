#include "Player.h"
#include "../../Game.h"
#include "../../SoundEffect.h"
#include "../../Util/Maths.h"
#include "../../Util/Strings.h"
#include "../Pickupable.h"
#include "../Triggers/Zone.h"
#include "Recipe.h"
#include <iostream>

std::string Player::getName() { return "player"; }

void Player::onCollide(Entity* e)
{
  if (canPush && e->hasTag("pushable") && !e->hasTag("pushed"))
  {
    sf::Vector2f ePos = e->getPosition();
    sf::Vector2f pos = getPosition();

    float xDist = fabs(ePos.x - pos.x);
    float yDist = fabs(ePos.y - pos.y);

    if (intersection.width == intersection.height)
    {
      return;
    }

    bool horOrVer = xDist > yDist;
    bool horOrVer2 = intersection.width < intersection.height;

    float speed = (state == State::Walking ? walkSpeed : carrySpeed) * speedFactor;
    if (speed <= 0.0f)
    {
      return;
    }

    sf::Vector2f m = lastDirVec * speed;

    // prevents moving the block at weird angles or dragging it along the sides
    // of walls wihout getting behind it
    if ((facing == Facing::Down || facing == Facing::Up) && !horOrVer && !horOrVer2)
    {
      m.x = 0.0f;
    }
    else if ((isRightSide() || isLeftSide()) && horOrVer && horOrVer2)
    {
      m.y = 0.0f;
    }
    else
    {
      return;
    }

    e->addTag("pushed");
    e->move(m);
    e->remTag("pushed");
  }
}

void Player::onMessage(Message msg)
{
  if (msg.name == "map_teleport")
  {
    teleportMap = msg.props.getString("map");
    if(teleportMap.find("current") != std::string::npos)
      teleportMap = "";
    teleportTarget = sf::Vector2f(msg.props.getFloat("x"), msg.props.getFloat("y"));
    teleporting = true;
    blackout.setFillColor(sf::Color(0, 0, 0, 0));
  }
  else if(msg.name == "shrink" && !hasTag("shrunk"))
  {
    bounds.setScale(0.5f, 0.5f);
    sprite.setScale(0.5f, 0.5f);
    rsprite.sprite.setScale(0.5f, 0.5f);

    useCorrections = false;

    addTag("shrunk");

  }
  else if(msg.name == "pickup_staff")
  {
    if(holding)
    {
      throwHeld(lightThrowPower);
    }

    state = State::Fanfare;
    walkProp = "carry";
    updateFacing(sf::Vector2f(0, 1));
    rsprite.setAnim(walkProp + facingToString());
    rsprite.resetTextureRect();
    holdingSprite = msg.sender->rsprite;
    fanfareTicks = 30;
    fanfareSound.play();
    hasStaff = true;

  }

}

/*void Player::onStun()
{
  rsprite.setAnim("stunned").play();

  throwHeld(2.0f);
  target = NULL;

  state = State::Stunned;
}*/

void Player::onMapSwitch()
{
  updateZone();
  if(currentZone) {
    game->camera.setPosition(currentZone->getPosition());
  }
}

void Player::onHit()
{
  for (int i = playerHearts.size() - 1; i >= 0; i--)
  {
    if (health <= i)
    {
      sf::IntRect rect = getFrame(1, sf::Vector2u(8, 8), sf::Vector2u(16, 8));
      playerHearts[i].setTextureRect(rect);
    }
  }
}

void Player::draw(sf::RenderWindow& window, sf::RenderStates states)
{
  SoundEffect::listener = getPosition();

  // draw hearts
  for (int i = 0; i < playerHearts.size(); i++)
  {
    game->guiLayer.push_back(&playerHearts[i]);
  }

  window.draw(sprite, states); // for shadow
  rsprite.draw(window, states);

  if (((state == State::Picking || state == State::Carrying ||
       state == State::SettingDown) &&
      holding != NULL) || state == State::Brewing || state == State::Fanfare)
  {
    int f = rsprite.getAnim().getFrame();
    sf::Vector2f offset(0, -12);

    if (f == 25)
      offset = sf::Vector2f(0, 2);
    else if (f == 26)
      offset = sf::Vector2f(0, 0);
    else if (f == 12 || f == 15 || f == 18 || f == 21)
      offset = sf::Vector2f(0, -10);

    if(state == State::Brewing)
    {
      sf::RenderStates cauldronStates = states;
      if (game->shadersAvailable)
      {
        cauldronStates.shader = &potionShader;
      }

      offset.y -= 2;
      keyitemSprite.update();
      keyitemSprite.setPosition(getPosition() + offset);
      keyitemSprite.draw(window, cauldronStates);
      keyitemSprite.draw(window, cauldronStates);

    }
    else
    {
      holdingSprite.update();
      holdingSprite.setPosition(getPosition() + offset);
      holdingSprite.draw(window, states);

    }

  }

  if (game->config.debug)
  {
    // window.draw(tileMarker);
    window.draw(bounds);
  }

  /*dirText.setString("dir: " + dirToString());
  facingText.setString("facing: " + facingToString());

  debugInfo.setString("lastFacingVec: " + toString(lastDirVec) + "\nangleOf: " +
  toString(angleOf(lastDirVec)));

  game->guiLayer.push_back(&dirText);
  game->guiLayer.push_back(&facingText);
  game->guiLayer.push_back(&debugInfo);*/
}

void Player::throwHeld(float power)
{
  if (holding != NULL)
  {
    bool moving = rsprite.getAnim().isPlaying();

    sf::Vector2f throwVec = moving ? lastDirVec : getFacingVec();

    int id = game->addEntity(holding);

    holding->setPosition(getPosition());

    ((Pickupable*)holding)->throwMe(throwVec * power);
    ((Pickupable*)holding)->thrower = this;
    holding->resolveIntersect();
    holding = NULL;

    walkProp = "";
  }
}

void Player::setDownHeld()
{
  if (holding != NULL)
  {
    int id = game->addEntity(holding);

    //Prevents objects from being put inside of solid tiles
    sf::Vector2f pos = getPosition() + sf::Vector2f(0, 10);
    if(game->map.getTile(pos).flags & holding->solidFilter)
    {
      pos = getPosition() + sf::Vector2f(-10, 0);
      if(game->map.getTile(pos).flags & holding->solidFilter)
      {
        pos = getPosition() + sf::Vector2f(10, 0);
        if(game->map.getTile(pos).flags & holding->solidFilter)
        {
          pos = getPosition() + sf::Vector2f(0, -10);

        }

      }

    }

    holding->setPosition(pos);
    ((Pickupable*)holding)->reset();

    holding->resolveIntersect();
    holding = NULL;
  }
}

void Player::updateCamera()
{
  if (currentZone == NULL)
  {
    updateZone();
  }

  // lock camera to zone bounds
  if (currentZone != NULL)
  {
    float roomSize = 9 * 16;
    sf::FloatRect zoneBounds = currentZone->getBounds();
    sf::Vector2f pPos = sprite.getPosition();

    if (zoneBounds.width <= roomSize)
      cameraTarget.x = currentZone->getPosition().x;
    else if (pPos.x + roomSize / 2.0f + 8 > zoneBounds.left + zoneBounds.width)
      cameraTarget.x = sprite.getPosition().x - ((pPos.x + roomSize / 2.0f) - (zoneBounds.left + zoneBounds.width)) - 8;
    else if (pPos.x - roomSize / 2.0f - 8 < zoneBounds.left)
      cameraTarget.x = sprite.getPosition().x + (zoneBounds.left - (pPos.x - roomSize / 2.0f)) + 8;
    else
      cameraTarget.x = sprite.getPosition().x;

    if (zoneBounds.height <= roomSize)
      cameraTarget.y = currentZone->getPosition().y;
    else if (pPos.y + roomSize / 2.0f > zoneBounds.top + zoneBounds.height)
      cameraTarget.y = sprite.getPosition().y - ((pPos.y + roomSize / 2.0f) - (zoneBounds.top + zoneBounds.height));
    else if (pPos.y - roomSize / 2.0f < zoneBounds.top)
      cameraTarget.y = sprite.getPosition().y + (zoneBounds.top - (pPos.y - roomSize / 2.0f));
    else
      cameraTarget.y = sprite.getPosition().y;
  }

  // update the camera position
  sf::Vector2f l = (cameraTarget - game->camera.getPosition()) * 0.25f;
  // chop some precision off
  /*l.x = chop(l.x, 2);
  l.y = chop(l.y, 2);*/

  float scale = game->config.scale;

  // doing scale rounding and division fixes graphical glitch with gaps between
  // tiles
  sf::Vector2f cPos = game->camera.getPosition() + l;
  game->camera.setPosition(round(cPos.x * scale) / scale, round(cPos.y * scale) / scale);
}

void Player::updateZone()
{
  sf::Vector2f pos = getPosition();
  sf::FloatRect rect(pos.x - 8, pos.y - 8, 16, 16);
  std::vector<Entity*> found = game->getEntitiesInBox(rect, Game::withName({"zone"}));

  bool inOld = false;
  int next = -1;
  for (int i = 0; i < found.size(); i++)
  {
    if (found[i] == currentZone)
    {
      inOld = true;
      break;
    }

    next = i;

  }

  if (!inOld && next != -1)
  {
    if (currentZone != NULL)
    {
      currentZone->current = false;
      currentZone->events.onLeave(currentZone, this);
    }

    currentZone = (Zone*)found[next];
    currentZone->current = true;
    currentZone->events.onEnter(currentZone, this);

  }
}

bool Player::updateTeleporting()
{
  if (teleporting)
  {
    sf::Color c = blackout.getFillColor();
    if (getPosition() == teleportTarget)
    {
      c.a -= 51;
      if (c.a == 0)
      {
        teleporting = false;
      }
    }
    else
    {
      c.a += 51;
      if (c.a == 255)
      {
        setPosition(teleportTarget);

        if (teleportMap != "")
        {
          game->nextMap = teleportMap;
          teleportMap = "";
        }

        updateZone();
        if(currentZone) {
          game->camera.setPosition(currentZone->getPosition());
          cameraTarget = currentZone->getPosition();
        }
      }
    }

    blackout.setFillColor(c);
    game->guiLayer.push_back(&blackout);

    return true;
  }

  return false;
}

void Player::updateFacing(sf::Vector2f m)
{
  float mLen = mag(m);

  if (mLen > 0.0f)
  {
    lastFacing = facing;
    lastDirVec = m;

    float d = angleOf(m);

    if (aprox(d, 315.0f))
    {
      dir = Facing::DownRight;
      facing = Facing::Down;
    }
    else if (aprox(d, 225.0f))
    {
      dir = Facing::DownLeft;
      facing = Facing::Down;
    }
    else if (aprox(d, 45.0f))
    {
      dir = Facing::UpRight;
      facing = Facing::Up;
    }
    else if (aprox(d, 135.0f))
    {
      dir = Facing::UpLeft;
      facing = Facing::Up;
    }
    else if (aprox(d, 270.0f))
    {
      dir = facing = Facing::Down;
    }
    else if (aprox(d, 180.0f))
    {
      dir = facing = Facing::Left;
    }
    else if (aprox(d, 90.0f))
    {
      dir = facing = Facing::Up;
    }
    else if (aprox(d, 0.0f))
    {
      dir = facing = Facing::Right;
    }
    else
    {
      dir = facing = Facing::Down;
    }
  }
}

bool Player::isRightSide() { return dir == Facing::DownRight || dir == Facing::UpRight || dir == Facing::Right; }

bool Player::isLeftSide() { return dir == Facing::DownLeft || dir == Facing::UpLeft || dir == Facing::Left; }

sf::Vector2f Player::getFacingVec()
{
  if (facing == Facing::Down)
  {
    return sf::Vector2f(0, 1);
  }
  else if (facing == Facing::Right)
  {
    return sf::Vector2f(1, 0);
  }
  else if (facing == Facing::Up)
  {
    return sf::Vector2f(0, -1);
  }
  else
  {
    return sf::Vector2f(-1, 0);
  }
}

sf::Vector2f Player::getDirVec()
{
  if (dir == Facing::DownRight)
  {
    return sf::Vector2f(0.707, 0.707);
  }
  else if (dir == Facing::DownLeft)
  {
    return sf::Vector2f(-0.707, 0.707);
  }
  else if (dir == Facing::UpRight)
  {
    return sf::Vector2f(0.707, -0.707);
  }
  else if (dir == Facing::UpLeft)
  {
    return sf::Vector2f(-0.707, -0.707);
  }
  if (facing == Facing::Down)
  {
    return sf::Vector2f(0, 1);
  }
  else if (facing == Facing::Right)
  {
    return sf::Vector2f(1, 0);
  }
  else if (facing == Facing::Up)
  {
    return sf::Vector2f(0, -1);
  }
  else
  {
    return sf::Vector2f(-1, 0);
  }
}

std::string Player::dirToString()
{
  if (dir == Facing::DownRight)
  {
    return "down right";
  }
  else if (dir == Facing::DownLeft)
  {
    return "down left";
  }
  else if (dir == Facing::UpRight)
  {
    return "up right";
  }
  else if (dir == Facing::UpLeft)
  {
    return "up left";
  }
  else if (dir == Facing::Down)
  {
    return "down";
  }
  else if (dir == Facing::Right)
  {
    return "right";
  }
  else if (dir == Facing::Up)
  {
    return "up";
  }
  else
  {
    return "left";
  }
}

std::string Player::facingToString()
{
  if (facing == Facing::Down)
  {
    return "down";
  }
  else if (facing == Facing::Right)
  {
    return "right";
  }
  else if (facing == Facing::Up)
  {
    return "up";
  }
  else
  {
    return "left";
  }
}

void Player::breakCauldron()
{
  cauldronTicks = 0;
  if(state == State::Brewing)
  {
    updateBrewing();

    ingredients.clear();

    sf::Vector2f releasePos = getPosition() + sf::Vector2f(0, -10);
    if(game->map.getTile(releasePos).flags & (int32_t)Tile::Flag::Solid)
    {
      releasePos = getPosition();

    }

    for(Entity* e : cauldronEntities)
    {
      if (e->health > 0)
      {
        Pickupable* p = (Pickupable*)e;
        p->setPosition(releasePos);
        p->throwMe(unitVec(qrandIn(0, pi * 2)) * qrandIn(2.0f, 2.5f));
        p->thrower = this;
        game->addEntity(e);
        e->resolveIntersect();
      }
      else
      {
        if(e->hasTag("cauldron_nodie"));
        e->noOnDie = true;
        game->addEntity(e);
      }

    }

    potionShader.setParameter("pColor", sf::Color(109, 170, 44));

    sf::Vector2f pos = getPosition() + sf::Vector2f(0, -14);
    game->addParticles(pos, 53, 3, 4, 0.3, 0.5, 1.5);
    game->addParticlesInCloud(pos, 16.0f, 53, 3, 3);

    if(cauldronPickup != NULL)
    {
      game->addEntity(cauldronPickup);
      for(Entity* me : cauldronEntities)
      {
        me->trackedResources.insert(cauldronPickup->id);
      }

      holding = cauldronPickup;

      game->removeEntityWithId(holding->id);

      ((Pickupable*)holding)->pickup();
      holdingSprite = holding->rsprite;

      walkProp = "carry";
      state = State::Carrying;

    }
    else
    {
      walkProp = "";
      state = State::Walking;
    }

    cauldronEntities.clear();

    rsprite.setAnim(walkProp + facingToString());

    cauldronPickup = NULL;
  }

}

bool Player::updateBrewing()
{
  /*for (auto& kv : ingredients)
  {
    std::cout << kv.first << ": " << kv.second << "\n";

  }*/

  for (Recipe& r : Recipe::recipies)
  {
    if (r.hasReqs(ingredients))
    {
      if (r.makesMultiple) // useful for breaking stuff apart by putting it in
                           // the cauldron
      {
        std::vector<Entity*> e = r.results();
        for (int i = 0; i < e.size(); i++)
        {
          e[i]->setPosition(getPosition() + sf::Vector2f(0, -10));
          game->addEntity(e[i]);

          if(e[i]->hasTag("pickupable"))
          {
            Pickupable* p = (Pickupable*)e[i];
            p->setPosition(getPosition() + sf::Vector2f(0, -10));
            p->throwMe(unitVec(qrandIn(0, pi * 2)) * qrandIn(2.0f, 2.5f));
            p->thrower = this;
            e[i]->resolveIntersect();

          }

          for(Entity* me : cauldronEntities)
          {
            me->trackedResources.insert(e[i]->id);
          }

        }
      }
      else
      {
        Entity* e = r.result();
        e->setPosition(getPosition() + sf::Vector2f(0, -10));

        if(!e->hasTag("pickupable"))
        {
          e->setPosition(getPosition() + sf::Vector2f(0, -16));
          game->addEntity(e);

          for(Entity* me : cauldronEntities)
          {
            me->trackedResources.insert(e->id);
          }

        }
        else
        {
          cauldronPickup = e;

        }

      }

      std::map<std::string, int> ing = r.ingredients;
      for (auto& kv : ing)
      {
        for (Entity* e : cauldronEntities)
        {
          if (e->health > 0 && kv.first == e->getName() && kv.second > 0)
          {
            e->health = -1;
            kv.second--;
          }
        }
      }


      return true;
    }
  }

  return false;

}

void Player::giveHat()
{
  rsprite.setOffset(sf::Vector2u(0, 0));

}

bool Player::read()
{
  sf::Vector2f pos = getPosition();
  std::vector<Entity*> entities;
  entities = game->getEntitiesInRadius(pos, 32.0f, Game::withTag({"readable"}));
  if (entities.size() > 0)
  {
    Entity* e = entities[0];
    if (dist(pos, e->getPosition()) <= e->props.getFloat("read_distance"))
    {
      entities[0]->sendMessage(this, "read");
      rsprite.getAnim().setPlaying(false);
      rsprite.getAnim().reset();
      rsprite.resetTextureRect();
      return true;
    }
  }
  return false;
}
