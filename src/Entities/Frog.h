#ifndef FROG_H_
#define FROG_H_

#include "Pickupable.h"

class Frog : public Pickupable
{
public:
  Frog(Game* game, sf::Vector2f pos, int color = -1); //-1 pick

  std::string getName();

  void onTick();

  void onLand();
  void onPickup();

  bool doesCollide(Entity* other);

  int scared;
  sf::Vector2f dest;
  bool jumping;
  sf::Vector2f scarePos;

  int color;
  float speed;
  std::string prefix;
};

#endif /*FROG*/
