#include "Projectile.h"
#include "../Game.h"
#include "../Util/Maths.h"

Projectile::Projectile(Entity* shooter, sf::Vector2f dir, sf::Vector2f pos, sf::Vector2f size)
    : Entity(shooter->game, size, pos), dir(dir), shooter(shooter)
{
  damage = 1;
  speed = 4;
  addTag("projectile");
  useCorrections = false;
}

void Projectile::onTick()
{
  if (!move(dir * speed))
  {
    health = -1;
  }
}

bool Projectile::doesCollide(Entity* other) { return other != shooter && doesProjectileCollide(other); }

bool Projectile::doesProjectileCollide(Entity* other) { return other->hasTag("creature"); }

void Projectile::onCollide(Entity* other)
{
  health = -1;
  other->hit(1);
}

void Projectile::reflect(Entity* newShooter)
{
  dir.x = -dir.x;
  dir.y = -dir.y;
  shooter = newShooter;
}