#ifndef CHARACTER_H_
#define CHARACTER_H_

#include "../../Entity.h"
#include "../Player/Player.h"

class Character : public Entity
{
public:
  enum class Facing
  {
    None, // used for resetting lastFacing
    Down,
    Right,
    Up,
    Left

  };


  Character(Game* game, sf::Vector2f pos, std::string tex);

  std::string getName();

  void onTick();

  void walk(sf::Vector2f m);
  void setFacing(Facing f);

  void onMessage(Message msg);

  static std::string facingToString(Facing facing);

private:
  void updateFacing(sf::Vector2f m);
  void updateAnim();

  Facing facing;
  Facing lastFacing;

  bool updated;

};

#endif /*CHARACTER*/
