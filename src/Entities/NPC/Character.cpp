#include "Character.h"
#include "../../Game.h"

Character::Character(Game* game, sf::Vector2f pos, std::string tex) :
  Entity(game, sf::Vector2f(12, 12), pos)
{
  rsprite.setSize(sf::Vector2f(16, 32));
  rsprite.setFrameSize(sf::Vector2u(8, 16));
  rsprite.setOrigin(8, 20);
  rsprite.setTexture(game->textures.getTexture(tex));

  rsprite.addAnim("down", {0, 1, 0, 2});
  rsprite.addAnim("right", {3, 4, 3, 5});
  rsprite.addAnim("up", {6, 7, 6, 8});
  rsprite.addAnim("left", {9, 10, 9, 11});

  /*rsprite.addAnim("carrydown", {12, 13, 12, 14});
  rsprite.addAnim("carryright", {15, 16, 15, 17});
  rsprite.addAnim("carryup", {18, 19, 18, 20});
  rsprite.addAnim("carryleft", {21, 22, 21, 23});*/

  setZ(4);

  facing = Facing::Down;
  lastFacing = Facing::None;

  updateAnim();

}

std::string Character::getName()
{
  return "character";

}

void Character::onTick()
{
  if(updated)
  {
    updated = false;
    updateAnim();
  }
  else if(rsprite.getAnim().isPlaying())
  {
    rsprite.getAnim().setPlaying(false);
    rsprite.getAnim().reset();
    rsprite.resetTextureRect();

  }

}

void Character::walk(sf::Vector2f m)
{
  float len = mag(m);
  if(len > 0.0f)
  {
    updated = true;
    updateFacing(m);

    m = unitVec(m);
    float speed = 2.0f;
    move(m * speed);

    updateAnim();

  }

}

void Character::setFacing(Facing f)
{
  facing = f;
  updateAnim();

}

std::string Character::facingToString(Facing facing)
{
  if(facing == Facing::Right)
  {
    return "right";

  }
  else if(facing == Facing::Up)
  {
    return "up";

  }
  else if(facing == Facing::Left)
  {
    return "left";
  }
  else
  {
    return "down";

  }

}

void Character::onMessage(Message msg)
{
  if(msg.name == "map_teleport")
  {
    std::string teleportMap = msg.props.getString("map");
    if(teleportMap.find("current") != std::string::npos)
      teleportMap = "";
    sf::Vector2f teleportTarget = sf::Vector2f(msg.props.getFloat("x"), msg.props.getFloat("y"));

    setPosition(teleportTarget);

    if(teleportMap != "")
    {
      game->removeEntityWithId(id);
      Map& map = game->maps[teleportMap];
      map.entities.push_back(this);
    }

  }

}

void Character::updateFacing(sf::Vector2f m)
{
  float a = angleOf(m);
  if(aprox(a, 270.0f))
  {
    facing = Facing::Down;
  }
  else if(aprox(a, 180.0f))
  {
    facing = Facing::Left;
  }
  else if(aprox(a, 90.0f))
  {
    facing = Facing::Up;
  }
  else if(aprox(a, 0.0f))
  {
    facing = Facing::Right;
  }


}

void Character::updateAnim()
{
  if(facing != lastFacing)
  {
    lastFacing = facing;
    rsprite.setAnim(facingToString(facing)).play();
  }
  else if(!rsprite.getAnim().isPlaying())
  {
    Anim& a = rsprite.setAnim(facingToString(facing));
    a.play();
    a.setIndex(1);
    rsprite.resetTextureRect();
  }

}
