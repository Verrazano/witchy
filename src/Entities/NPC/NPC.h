#ifndef NPC_H_
#define NPC_H_

#include "../../Entity.h"
#include "../Readable.h"
#include <map>
#include "NPCTrigger.h"

class NPC : public Entity
{
public:
  NPC(Game* game, sf::Vector2f pos, std::string file);

  std::string getName();

  void onTick();

  void readConfig(std::string path);

  void onMessage(Message msg);

  std::string name;

  Readable readable;

  static std::map<std::string, NPCTrigger*> triggerMap;
  std::vector<NPCTrigger*> triggers;

};

/*
npc file
frame index
name
dialog
*/

#endif /*NPC*/
