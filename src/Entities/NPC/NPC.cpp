#include "NPC.h"
#include "../../Game.h"
#include "../Player/Player.h"
#include "../../Util/Strings.h"
#include <fstream>
#include <iostream>
#include "VivianTriggers.h"
#include "../Player/Staff.h"

std::map<std::string, NPCTrigger*> NPC::triggerMap;

NPC::NPC(Game* game, sf::Vector2f pos, std::string file) : Entity(game, sf::Vector2f(12, 12), pos), readable(this)
{
  rsprite.setSize(sf::Vector2f(16, 32));
  rsprite.setOrigin(8, 20);
  rsprite.setTexture(game->textures.getTexture("res/npcs/characters.png"));
  rsprite.setFrameSize(sf::Vector2u(8, 16));

  readConfig("res/npcs/" + file + ".cfg");

  addTag("shrinkable");

  setZ(4);

  static bool loaded = false;
  if(!loaded)
  {
    loaded = true;
    //load triggers
    triggerMap["has_frog"] = new HasFrogTrigger();
    triggerMap["give_staff"] = new GiveEntityTrigger("give_staff", new Staff(game, sf::Vector2f(0, 0)));

  }

}

std::string NPC::getName() { return "npc_"+name; }

void NPC::onTick()
{
  Player* player = (Player*)game->getPlayer();

  for(auto it = triggers.begin(); it != triggers.end();)
  {
    NPCTrigger* t = (*it);
    if(t->doesTickTrigger(this, player))
    {
      if(!t->onTrigger(this, player))
      {
        delete t;
        it = triggers.erase(it);
        continue;
      }

    }
    it++;

  }

  readable.onTick();
}

void NPC::readConfig(std::string path)
{
  int frame = 3;
  name = "link";

  std::fstream f;
  f.open(path.c_str());

  std::map<std::string, Readable::Dialog> tree;

  if (f)
  {
    std::string last_var;
    f >> frame;
    std::cout << "frame: " << frame << "\n";
    f >> name;
    std::cout << "name: " << name << "\n";
    std::string start;
    f >> start;
    std::string text;
    std::string part;
    f >> part;
    while(std::getline(f, text))
    {
      text = part + text;
      part = "";
      if(text.find(">") != std::string::npos)
      {
        int s = text.find(">");
        std::string v = trim(text.substr(0, s));
        last_var = v;
        text = trim(text.substr(s+1));
        tree[v].texts.push_back(text);

      }
      else if(text[0] == '^')
      {
        text = text.substr(1);
        text = trim(text);
        tree[last_var].texts.push_back(text);

      }
      else if(text[0] == '?')
      {
        std::cout << "question\n";
        text = trim(text.substr(1));
        int s = text.find("|");
        std::string left = trim(text.substr(0, s));
        int a = left.find("@");
        std::string option1 = trim(left.substr(0, a));
        std::string target1 = trim(left.substr(a+1));

        std::string right = trim(text.substr(s+1));
        a = right.find("@");
        std::string option2 = trim(right.substr(0, a));
        std::string target2 = trim(right.substr(a+1));

        tree[last_var].option1 = option1;
        tree[last_var].option2 = option2;
        tree[last_var].target1 = target1;
        tree[last_var].target2 = target2;

        tree[last_var].endState = 0;

      }
      else if(text[0] == '=')
      {
        text = trim(text.substr(1));

        tree[last_var].endState = 1;
        tree[last_var].target = text;

      }
      else if(text.find("#=") == 0)
      {
        text = trim(text.substr(2));

        tree[last_var].endState = 2;
        tree[last_var].target = text;

      }
      else if(text[0] == '#')
      {
        text = trim(text.substr(1));
        tree[last_var].endState = 3;

      }
      else if(text[0] == '*')
      {
        text = trim(text.substr(1));
        tree[last_var].endState = 4;
        tree[last_var].target = text;

      }
      else if(text[0] == '[')
      {
        text = trim(text.substr(1));
        triggers.push_back(triggerMap[text]);

      }

    }

    rsprite.setOffset(sf::Vector2u(8 * frame, 0));

    addTag("npc_" + name);

    readable.set(tree, name);
    readable.current_state = start;
    readable.setBubbleOrigin(sf::Vector2f(8, 26));
  }
}

void NPC::onMessage(Message msg)
{
  if(msg.name == "read")
  {
    Player* player = (Player*)msg.sender;

    for(auto it = triggers.begin(); it != triggers.end();)
    {
      NPCTrigger* t = (*it);
      if(t->doesReadTrigger(this, player))
      {
        if(!t->onTrigger(this, player))
        {
          delete t;
          it = triggers.erase(it);
          continue;
        }

      }
      it++;

    }
  }

  readable.onMessage(msg);
}
