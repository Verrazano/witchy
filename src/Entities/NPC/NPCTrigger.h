#ifndef NPCTRIGGER_H_
#define NPCTRIGGER_H_

class NPC;
class Player;

class NPCTrigger
{
public:
  NPCTrigger();

  ~NPCTrigger();

  virtual bool doesTickTrigger(NPC* npc, Player* player) = 0;

  virtual bool doesReadTrigger(NPC* npc, Player* player) = 0;

  // true = keep
  virtual bool onTrigger(NPC* npc, Player* player) = 0;

};

#endif /*NPCTRIGGER*/
