#ifndef VIVIANTRIGGERS_H_
#define VIVIANTRIGGERS_H_

#include "NPCTrigger.h"
#include <string>

class Entity;

class HasFrogTrigger : public NPCTrigger
{
public:
  HasFrogTrigger();

  bool doesTickTrigger(NPC* npc, Player* player);

  bool doesReadTrigger(NPC* npc, Player* player);

  bool onTrigger(NPC* npc, Player* player);

};

class GiveEntityTrigger : public NPCTrigger
{
public:
  GiveEntityTrigger(std::string state, Entity* entity);

  bool doesTickTrigger(NPC* npc, Player* player);

  bool doesReadTrigger(NPC* npc, Player* player);

  bool onTrigger(NPC* npc, Player* player);

  std::string state;
  Entity* entity;

};

#endif /*VIVIANTRIGGERS*/
