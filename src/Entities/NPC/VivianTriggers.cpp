#include "VivianTriggers.h"
#include "NPC.h"
#include "../Player/Player.h"
#include "../../Game.h"

HasFrogTrigger::HasFrogTrigger()
{
}

bool HasFrogTrigger::doesTickTrigger(NPC* npc, Player* player)
{
  return false;
}

bool HasFrogTrigger::doesReadTrigger(NPC* npc, Player* player)
{
  if(npc->readable.current_state == "talk_to_me")
  {
    if(player->holding && player->holding->getName() == "frog")
    {
      npc->readable.current_state = "get_frog";
      return true;

    }

  }

  return false;
}

bool HasFrogTrigger::onTrigger(NPC* npc, Player* player)
{
  return false;
}

GiveEntityTrigger::GiveEntityTrigger(std::string state, Entity* entity) : state(state), entity(entity)
{
}

bool GiveEntityTrigger::doesTickTrigger(NPC* npc, Player* player)
{
  if(npc->readable.current_state == state)
  {
    return true;
  }

  return false;

}

bool GiveEntityTrigger::doesReadTrigger(NPC* npc, Player* player)
{
  return false;

}

bool GiveEntityTrigger::onTrigger(NPC* npc, Player* player)
{
  entity->setPosition(player->getPosition());
  player->game->addEntity(entity);
  return false;

}
