#include "Pickupable.h"
#include "../Game.h"
#include "../Util/Maths.h"

Pickupable::Pickupable(Game* game, sf::Vector2f size, sf::Vector2f pos) : Entity(game, size, pos)
{
  addTag("pickupable");
  bigShadow = false;

  sprite.setSize(sf::Vector2f(16, 16));
  sprite.setOrigin(8, 4);
  sprite.setFillColor(sf::Color::Transparent);
  sprite.setTexture(game->textures.getTexture("res/entities.png"));

  thrown = false;
  thrower = NULL;
  solid = false;
}

bool Pickupable::updateThrown()
{
  if (thrown)
  {
    if (height <= 0.0f)
    {
      rsprite.sprite.setOrigin(origin);
      sprite.setFillColor(sf::Color::Transparent);
      thrown = false;
      useCorrections = true;
      solid = false;
      onLand();
      thrower = NULL;
      setZ(origZ);
      return false;
    }

    if (up > 0)
    {
      height += 1.0f;
      up -= 1.0f;
      rsprite.setOrigin(origin.x, (int)(origin.y + height));
    }
    else
    {
      height -= 1.0f;
      rsprite.setOrigin(origin.x, (int)(origin.y + height));
      if (!move(vel))
      {
        height -= 1.0f;
        return true;
      }
    }
  }

  return false;
}

void Pickupable::pickup()
{
  if (thrown)
  {
    reset();
  }

  rsprite.setAnim(carriedAnim).play();
  origin = rsprite.getOrigin();
  origZ = z;
  onPickup();
}

void Pickupable::throwMe(sf::Vector2f vel, float upVel)
{
  this->vel = vel;
  up = upVel;

  height = 12.0f;

  origin = rsprite.getOrigin();
  rsprite.setOrigin(origin.x, (int)(origin.y + height));

  sprite.setFillColor(sf::Color::White);
  sprite.setTextureRect(sf::IntRect(6 * 8, bigShadow ? 0 : 8, 8, 8));

  thrown = true;
  useCorrections = false;

  origZ = z;
  setZ(10);

  solid = true;
  onThrown();
}

void Pickupable::onPickup() {}

void Pickupable::onThrown() {}

void Pickupable::onLand() {}

bool Pickupable::doesCollide(Entity* other)
{
  bool ignorePlayer = thrower != NULL && thrower->hasTag("container") && other->getName() == "player";
  return thrown && !ignorePlayer && other != thrower && !other->hasTag("pickupable");
}

void Pickupable::reset()
{
  rsprite.sprite.setOrigin(origin);
  sprite.setFillColor(sf::Color::Transparent);
  thrown = false;
  useCorrections = true;
  setZ(origZ);
}
