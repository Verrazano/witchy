#ifndef CHEST_H_
#define CHEST_H_

#include "../Entity.h"

class Chest : public Entity
{
public:
  Chest(Game* game, sf::Vector2f pos, bool big, std::string item);

  std::string getName();

  void onTick();

  void onCollide(Entity* entity);

  std::string item;
  bool big;
  bool opened;

  Entity* player;
};

#endif /*CHEST*/
