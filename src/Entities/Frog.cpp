#include "Frog.h"
#include "../Game.h"

Frog::Frog(Game* game, sf::Vector2f pos, int ncolor) : Pickupable(game, sf::Vector2f(16, 12), pos), color(ncolor)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/frogs/frogs.png"));

  Anim croak("croak", 1.2f);
  croak.setFrames({0, 0, 1});
  rsprite.addAnim(croak);
  rsprite.setAnim("croak").play();

  solid = false;

  if (color == -1)
  {
    color = qrand() % 3;
    props.setInt("potionColor", 0x346524);
    speed = 2.0f;

    Anim jump("jump", 0.1f, false);
    jump.setFrames({0, 2, 3, 4, 0});
    rsprite.addAnim(jump);
  }
  else
  {
    props.setInt("potionColor", 0xdbd75d);
    speed = 3.0f;

    Anim jump("jump", 0.05f, false);
    jump.setFrames({0, 2, 3, 4, 0});
    rsprite.addAnim(jump);
    // color == 3 is a key frog

    addTag("key");
    prefix = "key_";
  }

  rsprite.setOffset(sf::Vector2u(0, 8 * color));

  carriedAnim = "croak";
  addTag("ingredient");

  //addTag("stunnable");

  jumping = false;

  scared = 0;
}

std::string Frog::getName() { return prefix + "frog"; }

void Frog::onTick()
{
  updateThrown();

  if (color == 3 && game->ticks % 17 == 0) // sparkily effect
  {
    sf::Vector2f pos = getPosition();
    game->addParticles(pos, 3 * 8, 6, 1, 1, 0.0f, 1);
  }

  if (!thrown)
  {
    if (game->ticks % 9 == 0)
    {
      std::vector<Entity*> entities = game->getEntitiesInRadius(getPosition(), 32.0f, Game::withTag({"scare_frog"}));
      int count = entities.size();

      if (count > 0)
      {

        scarePos = entities[0]->getPosition();
        scared = 60;
      }
    }

    if (scared > 0)
    {
      if (rsprite.getAnim().getName() != "jump")
      {
        rsprite.setAnim("jump");
      }

      if (!rsprite.getAnim().isPlaying())
      {
        rsprite.setAnim("jump").play();
        solidFilter = Tile::Flag::Solid;

        int dir = rand() % 2;

        dest = getPosition();

        rsprite.setFacingLeft(true);

        sf::Vector2f updown(0, 16);
        if (scarePos.y > dest.y)
        {
          updown = -updown;
        }

        sf::Vector2f leftright(16, 0);
        if (scarePos.x > dest.x)
        {
          rsprite.setFacingLeft(false);
          leftright = -leftright;
        }

        if (dir == 0)
          dest += updown;
        else if (dir == 1)
          dest += leftright;

        solid = true;
        jumping = true;
        // resolveIntersect();
      }

      scared--;
    }
    else
    {
      if (!rsprite.getAnim().isPlaying())
      {
        rsprite.setAnim("croak").play();
        solid = false;
        jumping = false;
      }
    }

    if (jumping)
    {
      sf::Vector2f m = dest - getPosition();
      m = unitVec(m) * speed;
      move(m.x, m.y);
    }
  }
}

void Frog::onLand()
{
  jumping = false;
  rsprite.setAnim("croak").play();
}

void Frog::onPickup() { jumping = false; }

bool Frog::doesCollide(Entity* other)
{
  bool collideWithEntity = jumping && other->getName() != "player";
  return Pickupable::doesCollide(other) || collideWithEntity;
}
