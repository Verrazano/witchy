#include "CrystalBullet.h"
#include "../Game.h"

CrystalBullet::CrystalBullet(Entity* shooter, sf::Vector2f pos, sf::Vector2f dir)
    : Projectile(shooter, dir, pos, sf::Vector2f(8, 8))
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/entities.png"));
  rsprite.addAnim("main", {91});
  rsprite.setAnim("main");
}

std::string CrystalBullet::getName() { return "crystal_bullet"; }