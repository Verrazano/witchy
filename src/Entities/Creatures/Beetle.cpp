#include "Beetle.h"
#include "../../Game.h"

Beetle::Beetle(Game* game, sf::Vector2f pos) :
  Pickupable(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/creatures/beetle.png"));

  rsprite.addAnim("idle", {0});
  rsprite.addAnim("carried", {3});
  carriedAnim = "carried";

  addTag("ingredient");
  addTag("bug");

}

std::string Beetle::getName()
{
  return "beetle";

}

void Beetle::onTick()
{
  updateThrown();

}

void Beetle::onLand()
{
  rsprite.setAnim("idle").play();

}
