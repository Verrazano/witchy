#include "SlimeCube.h"
#include "../../Game.h"
#include "../../Util/Anim.h"
#include "../../Util/Maths.h"
#include "../../Util/Strings.h"
#include <iostream>

SlimeCube::SlimeCube(Game* game, sf::Vector2f pos) : Pickupable(game, sf::Vector2f(6, 8), pos)
{
  solid = false;

  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/creatures/slime_cube.png"));
  rsprite.addAnim("main", {0}, 0.1f, false);
  rsprite.addAnim("hop", {0, 1, 2, 3, 0}, 0.1f, false);

  useCorrections = true;

  carriedAnim = "main";

  addTag("ingredient");

  nextHop = qrandIn(2.5f, 4.5f);
}

std::string SlimeCube::getName() { return "slime_cube"; }

void SlimeCube::onLand() { hopTimer.restart(); }

void SlimeCube::onThrown() { hopTimer.restart(); }

void SlimeCube::onTick()
{
  updateThrown();

  if (hopTimer.getElapsedTime().asSeconds() >= nextHop)
  {
    game->addParticles(getPosition(), 3, 1, 2);
    rsprite.setAnim("hop").play();
    nextHop = qrandIn(2.5f, 4.5f);
    hopTimer.restart();
  }
}
