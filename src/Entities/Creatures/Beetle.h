#ifndef BEETLE_H_
#define BEETLE_H_

#include "../Pickupable.h"

class Beetle : public Pickupable
{
public:
  Beetle(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onLand();

};

#endif /*BEETLE*/
