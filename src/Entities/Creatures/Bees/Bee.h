#ifndef BEE_H_
#define BEE_H_

#include "../../../Entity.h"
#include "Hive.h"

class Bee : public Entity
{
public:
  Bee(Game* game, Hive* hive);

  std::string getName();

  void onTick();

  enum class State
  {
    Idle,
    Travel
  };

  State state;

  uint32_t thinkTick;
  bool atFlower;
  sf::Vector2f dest;
  int hiveId;
  sf::Vector2f hivePos;

};

#endif /*BEE*/
