#include "Bee.h"
#include "../../../Game.h"
#include "../../../Util/Maths.h"

Bee::Bee(Game* game, Hive* hive) :
  Entity(game, sf::Vector2f(8, 8), hive->getPosition())
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/creatures/bees.png"));

  rsprite.addAnim("main", {0, 1}, 0.1f, true);
  rsprite.setAnim("main").play();

  state = State::Idle;
  thinkTick = game->ticks + 20 + qrand()%30;

  hiveId = hive->id;
  hivePos = hive->getPosition();

  atFlower = false;

  solid = false;

  setZ(5);

  addTag("bug");

}

std::string Bee::getName()
{
  return "bee";

}

void Bee::onTick()
{
  float offset = ((float)(id + game->ticks))/20.0f;
  rsprite.setOrigin(8, 8 + (int)(sin(offset*2*pi)*4));

  uint32_t ticks = game->ticks;
  if(state == State::Idle)
  {
    if(ticks >= thinkTick)
    {
      state = State::Travel;
      if(!atFlower)
      {
        std::vector<Entity*> flowers = game->getEntitiesInRadius(hivePos, 96, Game::withTag({"flower"}));
        if(flowers.size() > 0)
        {
          Entity* flower = flowers[qrand()%flowers.size()];
          dest = flower->getPosition() + unitVec(qrandIn(0, pi*2))*8.0f;
          atFlower = true;

        }
        else
        {
          dest = hivePos + unitVec(qrandIn(0, pi*2))*48.0f;

        }

      }
      else
      {
        atFlower = false;
        dest = hivePos + unitVec(qrandIn(0, pi*2))*8.0f;

      }

      thinkTick = game->ticks + 20 + qrand()%30;

    }

  }
  else if(state == State::Travel)
  {
    if(ticks >= thinkTick)
    {
        state = State::Idle;
        thinkTick = game->ticks + 20 + qrand()%30;

    }
    else
    {
      //std::cout << "moving\n";
      sf::Vector2f d = dest - getPosition();
      sf::Vector2f offset = unitVec(d);

      if(offset.x > 0)
      {
        rsprite.setFacingLeft(false);

      }
      else
      {
        rsprite.setFacingLeft(true);
      }

      float speed = 48.0f/30.0f;
      offset *= speed;

      move(offset.x, offset.y);

      float m = mag(d);
      if(m < 2.0f)
      {
        state = State::Idle;
        thinkTick = game->ticks + 20 + qrand()%30;
      }

    }

  }

}
