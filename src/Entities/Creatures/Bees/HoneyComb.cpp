#include "HoneyComb.h"
#include "../../../Game.h"

HoneyComb::HoneyComb(Game* game, sf::Vector2f pos) :
  Pickupable(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/creatures/bees.png"));

  rsprite.addAnim("main", {8});
  carriedAnim = "main";

  addTag("ingredient");

}

std::string HoneyComb::getName()
{
  return "honeycomb";

}

void HoneyComb::onTick()
{
  updateThrown();

}
