#ifndef HONEYCOMB_H_
#define HONEYCOMB_H_

#include "../../Pickupable.h"

class HoneyComb : public Pickupable
{
public:
  HoneyComb(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

};

#endif /*HONEYCOMB*/
