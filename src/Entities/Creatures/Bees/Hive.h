#ifndef HIVE_H_
#define HIVE_H_

#include "../../../Entity.h"

class Hive : public Entity
{
public:
  Hive(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onDeath(Entity* other);

  void onDie();

  void onHit();

  int bees;
  int spawnTicks;

};

#endif /*HIVE*/
