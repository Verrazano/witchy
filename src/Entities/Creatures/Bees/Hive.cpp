#include "Hive.h"
#include "../../../Game.h"
#include "Bee.h"
#include "HoneyComb.h"

Hive::Hive(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(12, 12), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/creatures/bees.png"));

  rsprite.addAnim("main", {4});
  rsprite.addAnim("hit", {4, 5, 6, 7}, 0.1f, false);

  game->registerListener(this);

  bees = 0;
  spawnTicks = game->ticks + qrand()%60;

  addTag("breakable");
  addTag("player_breakable");

  health = 4;

}

std::string Hive::getName()
{
  return "hive";
}

void Hive::onTick()
{
  if(bees < 2 && game->ticks >= spawnTicks)
  {
    game->addEntity(new Bee(game, this));
    bees++;
    spawnTicks = game->ticks + qrand()%60;
  }

  Anim& anim = rsprite.getAnim();
  if(anim.getName() == "hit" && !anim.isPlaying())
  {
    rsprite.setAnim("main");
    addTag("breakable");
    addTag("player_breakable");

  }

}

void Hive::onDeath(Entity* other)
{
  if(other->getName() == "bee")
  {
    Bee* b = (Bee*)other;
    if(b->hiveId == id)
    {
      bees--;
      spawnTicks = game->ticks + qrand()%60;

    }

  }

}

void Hive::onDie()
{
  int drop_count = 2;
  for(int i = 0; i < drop_count; i++)
  {
    Pickupable* p = new HoneyComb(game, getPosition());

    float r = qrandIn(0.0f, pi*2.0f);
    sf::Vector2f vel = unitVec(r)*qrandIn(0.5f, 1.0f);
    p->throwMe(vel);

    game->addEntity(p);
  }

}

void Hive::onHit()
{
  game->addParticles(getPosition(), 24, 0, 3);
  remTag("breakable");
  remTag("player_breakable");
  rsprite.setAnim("hit").play();

}
