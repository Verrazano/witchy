#include "GnomeCap.h"
#include "../../Game.h"

GnomeCap::GnomeCap(Game* game, sf::Vector2f pos) :
  Pickupable(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/creatures/gnome.png"));

  rsprite.addAnim("main", {20});
  carriedAnim = "main";

  addTag("ingredient");

}

std::string GnomeCap::getName()
{
  return "gnome_cap";

}

void GnomeCap::onTick()
{
  updateThrown();

}
