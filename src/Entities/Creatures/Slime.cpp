#include "Slime.h"
#include "../../Game.h"
#include "SlimeCube.h"


Slime::Slime(Game* game, sf::Vector2f pos) :
  Entity(game, sf::Vector2f(16, 16), pos)
{
  sf::Texture* texture = game->textures.getTexture("res/creatures/slime.png");
  rsprite.setSize(sf::Vector2f(24, 16));
  rsprite.setFrameSize(sf::Vector2u(12, 8));
  rsprite.setTexture(texture);

  rsprite.addAnim("idle", {0});
  rsprite.addAnim("bounce", {0, 1, 2, 1, 0, 3, 0}, 0.16f, false);
  rsprite.addAnim("spawn", {6, 7, 8, 9, 10, 11}, 0.16f, false);
  rsprite.addAnim("stunned", {12, 13});
  rsprite.addAnim("stretch", {18, 19, 20, 19}, 0.12f);
  rsprite.addAnim("rip", {24, 25}, 0.12f, false);

  state = State::Spawn;
  solid = false;
  rsprite.setAnim("spawn").play();

  //Tags are added when the spawn animation finishes playing

}

std::string Slime::getName()
{
  return "slime";

}

void Slime::onTick()
{
  if(state == State::Idle)
  {
    if(game->ticks >= nextBounce)
    {
      setBounce();

    }

  }
  else if(state == State::Spawn)
  {
    if(!rsprite.getAnim().isPlaying())
    {
      setIdle();
      solid = true;
      resolveIntersect();
      addTag("pushable");
      addTag("stunnable");
      addTag("shrinkable");
      addTag("suckable");
      game->addParticles(getPosition(), 3, 0, 6, 0.3, 1, 2);

    }

  }
  else if(state == State::Bounce)
  {
    if(!rsprite.getAnim().isPlaying())
    {
      game->addParticlesInCloud(getPosition() + sf::Vector2f(0, 12), 8, 3, 0, 3, 0.3f, 0.2f, 0.4f);
      setIdle();

    }
    else
    {
      sf::Vector2f offset = unitVec(dest - getPosition());

      //slime moves 16 px a second 30 ticks in a second
      float speed = 16.0f/30.0f;
      offset *= speed;

      move(offset.x, offset.y);

    }

  }
  else if(state == State::Stunned)
  {
    move(stunVel.x, stunVel.y);
    stunVel *= 0.75f;

    float forceX = props.getFloat("suckable:forceX");
    float forceY = props.getFloat("suckable:forceY");

    sf::Vector2f force(forceX, forceY);
    float m = mag(force);

    float suckableX = props.getFloat("suckable:x");
    float suckableY = props.getFloat("suckable:y");

    sf::Vector2f cauldronPos(suckableX, suckableY);

    uint32_t ticks = game->ticks;

    if(m > 0.0f)
    {
      if(rsprite.getAnim().getName() == "stunned")
      {
        rsprite.setAnim("stretch").play();

      }

      stretchForce += m;

      float breakForce = 48.0f;

      if(stretchForce >= breakForce)
      {
        state = State::Rip;

      }
      else if(ticks%5 == 0)
      {
        //send slime particles to cauldron
        sf::Vector2f pos = getPosition() + unitVec(qrandIn(0.0f, pi*2.0f))*qrandIn(0.0f, 8.0f);
        sf::Vector2f v = unitVec(cauldronPos - pos)*3.0f;
        sf::IntRect f = getFrame(3, sf::Vector2u(8, 8), game->particleTexSize);
        game->particles.addParticle(pos, v, f, 0.6f);

      }

    }
    else if(stretchForce > 0.0f)
    {
      stretchForce -= 2.0f;
      if(stretchForce <= 0.0f)
      {
        stretchForce = 0.0f;

      }

    }
    else if(ticks >= stunTicks)
    {
      setIdle();

    }

    props.setFloat("suckable:forceX", 0.0f);
    props.setFloat("suckable:forceY", 0.0f);

  }
  else if(state == State::Rip)
  {
    Anim& anim = rsprite.getAnim();
    if(anim.getName() == "stretch" && anim.getIndex() == 3)
    {
      rsprite.setAnim("rip").play();

    }
    else if(anim.getName() == "rip" && !rsprite.getAnim().isPlaying())
    {
      kill();

    }

  }

}

void Slime::onDie()
{
  //spawn a cube on the left and right of the slime
  //check to make sure we don't spawn it inside of any tiles
  sf::Vector2f left = getPosition() + sf::Vector2f(-10, 0);
  if(game->map.getTile(left).flags & (int32_t)Tile::Flag::Solid)
  {
    left = getPosition();

  }

  sf::Vector2f right = getPosition() + sf::Vector2f(10, 0);
  if(game->map.getTile(right).flags & (int32_t)Tile::Flag::Solid)
  {
    right = getPosition();

  }

  Pickupable* p = new SlimeCube(game, left);

  float r = qrandIn(0.0f, pi*2.0f);
  sf::Vector2f vel = unitVec(r)*qrandIn(0.5f, 1.0f);
  p->throwMe(vel);

  game->addEntity(p);

  p = new SlimeCube(game, right);

  r = qrandIn(0.0f, pi*2.0f);
  vel = unitVec(r)*qrandIn(0.5f, 1.0f);
  p->throwMe(vel);

  game->addEntity(p);

}

void Slime::onMessage(Message msg)
{
  if(msg.name == "stun")
  {
    float power = msg.props.getFloat("power");
    float dirX = msg.props.getFloat("dirX");
    float dirY = msg.props.getFloat("dirY");

    stunTicks = game->ticks + 90;
    stunVel = sf::Vector2f(dirX, dirY)*power;

    state = State::Stunned;
    rsprite.setAnim("stunned").play();

    stretchForce = 0.0f;

  }
  else if(msg.name == "shrink" && !hasTag("shrunk"))
  {
    bounds.setScale(0.5f, 0.5f);
    sprite.setScale(0.5f, 0.5f);
    rsprite.sprite.setScale(0.5f, 0.5f);

    useCorrections = false;

    addTag("shrunk");

  }


}

void Slime::setIdle()
{
  state = State::Idle;

  rsprite.setAnim("idle").play();

  uint32_t ticks = game->ticks;
  uint32_t tickOffset = 60 + qrand()%45;
  nextBounce = ticks + tickOffset;

}

void Slime::setBounce()
{
  state = State::Bounce;

  rsprite.setAnim("bounce").play();

  sf::Vector2f offset = chooseFrom<sf::Vector2f>({sf::Vector2f(0, 16), sf::Vector2f(0, -16),
    sf::Vector2f(16, 0), sf::Vector2f(-16, 0)});

  dest = getPosition() + offset;

}
