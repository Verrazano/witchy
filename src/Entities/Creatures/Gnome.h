#ifndef GNOME_H_
#define GNOME_H_

#include "../Pickupable.h"

/*
stunnable
pickupable - when stunned
*/

class Gnome : public Pickupable
{
public:
  Gnome(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();
  bool doesCollide(Entity* other);
  void onMessage(Message msg);

  void onPickup();
  void onLand();

  void setIdle();
  void setStun(float power, sf::Vector2f vel);
  bool setGrab(std::string name);
  void setWander();

  bool walk();
  void throwHeld();

  void draw(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

  enum class State
  {
    Idle,
    Stunned,
    Pickedup,
    Wander,
    Grab

  };

  State state;

  std::string caplessProp;

  uint32_t stunTicks;
  sf::Vector2f stunVel;

  uint32_t thinkTick;

  uint32_t target;
  sf::Vector2f dest;

  uint32_t timeoutTick;

  Entity* holding;
  Sprite holdingSprite;

};

#endif /*GNOME*/
