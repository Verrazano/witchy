#include "Gnome.h"
#include "../../Game.h"
#include "GnomeCap.h"

Gnome::Gnome(Game* game, sf::Vector2f pos)
  : Pickupable(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setOrigin(8, 12);
  rsprite.setTexture(game->textures.getTexture("res/creatures/gnome.png"));

  rsprite.addAnim("idle", {0});
  rsprite.addAnim("walk", {0, 1, 0, 2});
  rsprite.addAnim("carry_idle", {4});
  rsprite.addAnim("carry", {4, 5, 4, 6});
  rsprite.addAnim("stunned", {8, 9});
  rsprite.addAnim("capless_idle", {12});
  rsprite.addAnim("capless_walk", {12, 13, 12, 14});
  rsprite.addAnim("capless_stunned", {16, 17});

  carriedAnim = "stunned";

  addTag("shrinkable");
  addTag("stunnable");
  remTag("pickupable");

  holding = NULL;

  solid = true;

  setIdle();

  // dont' use tile corrections, because this character is so small it doesn't matter
  useCorrections = false;

}

std::string Gnome::getName()
{
  return "gnome";

}

void Gnome::onTick()
{
  if(state == State::Idle)
  {
    /*
    gnomes like to eat mushrooms
    gnomes like to steal shiny things
    gnomes are scared of everything when they don't have their cap
    gnomes wander aimlessly
    */

    uint32_t ticks = game->ticks;
    if(ticks >= thinkTick)
    {
      /*
      check for nearby mushrooms and go to the closest one
      otherwise 50/50 wander or stay put
      */

      bool grabCap = false;
      if(caplessProp != "")
      {
        if(setGrab("gnome_cap"))
        {
          grabCap = true;

        }
      }

      if(!grabCap && !setGrab("mushroom"))
      {
        if(qrand()%2 == 0)
        {
          setWander();

        }

      }

    }

  }
  else if(state == State::Stunned)
  {
    move(stunVel.x, stunVel.y);
    stunVel *= 0.75f;

    uint32_t ticks = game->ticks;
    if(ticks >= stunTicks)
    {
      remTag("pickupable");
      setIdle();

    }


  }
  else if(state == State::Pickedup)
  {
    updateThrown();

  }
  else if(state == State::Grab)
  {
    if(walk())
    {
      Entity* toHold = game->getEntityById(target);
      if(toHold != NULL)
      {
        if(dist(toHold->getPosition(), getPosition()) <= 8.0f)
        {
          if(toHold->getName() == "gnome_cap")
          {
            toHold->kill();
            caplessProp = "";
            carriedAnim = "stunned";

          }
          else
          {
            holding = toHold;
            game->removeEntityWithId(holding->id);
            Pickupable* p = (Pickupable*)holding;
            p->pickup();
            holdingSprite = holding->rsprite;

          }

          setIdle();

        }

      }

    }

  }
  else if(state == State::Wander)
  {
    if(walk())
    {
      setIdle();

    }

  }

}

bool Gnome::doesCollide(Entity* other)
{
  bool collideWithEntity = state != State::Pickedup;
  return Pickupable::doesCollide(other) || (collideWithEntity && !other->hasTag("creature"));

}

void Gnome::onMessage(Message msg)
{
  if(msg.name == "stun")
  {
    float power = msg.props.getFloat("power");
    float dirX = msg.props.getFloat("dirX");
    float dirY = msg.props.getFloat("dirY");

    setStun(power, sf::Vector2f(dirX, dirY));

  }
  else if(msg.name == "shrink" && !hasTag("shrunk"))
  {
    bounds.setScale(0.5f, 0.5f);
    sprite.setScale(0.5f, 0.5f);
    rsprite.sprite.setScale(0.5f, 0.5f);

    addTag("shrunk");

  }

}

void Gnome::onPickup()
{
  state = State::Pickedup;

}

void Gnome::onLand()
{
  if(caplessProp == "")
  {
    caplessProp = "capless_";
    carriedAnim = caplessProp + "stunned";

    Pickupable* p = new GnomeCap(game, getPosition());
    sf::Vector2f vel = unitVec(qrandIn(0, 2*pi))*2.0f;
    p->throwMe(vel);

    game->addEntity(p);

  }

  //TODO: fix pickupable resetting corrections
  useCorrections = false;
  solid = true;
  setStun(0, sf::Vector2f(0, 0));

}

void Gnome::setIdle()
{
  state = State::Idle;
  if(holding != NULL)
  {
    rsprite.setAnim("carry_idle").play();

  }
  else
  {
    rsprite.setAnim(caplessProp + "idle").play();

  }


  thinkTick = game->ticks + 45;

}

void Gnome::setStun(float power, sf::Vector2f vel)
{
  stunTicks = game->ticks + 90;
  //knock the gnome back, because gnomes are light
  stunVel = vel*power*2.0f;

  state = State::Stunned;
  rsprite.setAnim(caplessProp + "stunned").play();

  throwHeld();

  addTag("pickupable");

}

bool Gnome::setGrab(std::string name)
{
  if(holding != NULL)
  {
    return false;

  }

  sf::Vector2f pos = getPosition();

  std::vector<Entity*> targets = game->getEntitiesInRadius(pos, 64.0f, Game::withName({name}));
  float minDist = 10000.0f;
  Entity* closestTarget = NULL;
  for(auto target : targets)
  {
    float d = dist(target->getPosition(), pos);
    if(d < minDist)
    {
      minDist = d;
      closestTarget = target;

    }

  }

  if(closestTarget != NULL)
  {
    state = State::Grab;
    target = closestTarget->id;
    dest = closestTarget->getPosition();
    rsprite.setAnim(caplessProp + "walk").play();
    timeoutTick = game->ticks + 90;
    return true;

  }

  return false;

}

void Gnome::setWander()
{
  int32_t x = qrand()%5 - 2;
  int32_t y = qrand()%5 - 2;
  dest = getPosition() + sf::Vector2f(x, y)*16.0f;
  state = State::Wander;
  if(holding != NULL)
  {
    rsprite.setAnim("carry").play();

  }
  else
  {
    rsprite.setAnim(caplessProp + "walk").play();

  }
  timeoutTick = game->ticks + 90;

}

bool Gnome::walk()
{
  sf::Vector2f d = dest - getPosition();
  sf::Vector2f offset = unitVec(d);

  //gnome moves 24 px a second 30 ticks in a second
  float speed = 24.0f/30.0f;
  offset *= speed;

  move(offset.x, offset.y);

  if(game->ticks >= timeoutTick)
  {
    setIdle();

  }

  float m = mag(d);
  if(m < 2.0f)
  {
    return true;

  }

  return false;

}

void Gnome::throwHeld()
{
  if(holding != NULL)
  {
    Pickupable* p = (Pickupable*)holding;
    p->setPosition(getPosition() + sf::Vector2f(0, -10));
    p->throwMe(unitVec(qrandIn(0, 2*pi))*2.0f);
    p->thrower = this;
    game->addEntity(p);
    p->resolveIntersect();
    holding = NULL;

  }

}

void Gnome::draw(sf::RenderWindow& window, sf::RenderStates states)
{
  rsprite.draw(window, states);

  if(holding != NULL)
  {
    sf::Vector2f offset(0, -10);

    int32_t frame = rsprite.getAnim().getFrame();
    if(frame == 5 || frame == 6)
    {
      offset = sf::Vector2f(0, -12);
    }

    holdingSprite.setPosition(getPosition() + offset);
    holdingSprite.update();
    holdingSprite.draw(window, states);

  }

  if (game->config.debug)
  {
    window.draw(bounds);
  }

}
