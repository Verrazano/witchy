#ifndef SLIMECUBE_H_
#define SLIMECUBE_H_

#include "../Pickupable.h"

class SlimeCube : public Pickupable
{
public:
  SlimeCube(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onLand();
  void onThrown();

  float nextHop;
  sf::Clock hopTimer;
  int particle;
};

#endif /*SLIMECUBE*/
