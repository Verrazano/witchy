#ifndef SLIME_H_
#define SLIME_H_

#include "../../Entity.h"

/*
pushable
stunnable
suckable - when stunned
*/

class Slime : public Entity
{
public:
  Slime(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();
  void onDie();
  void onMessage(Message msg);

  void setIdle();
  void setBounce();

  enum class State
  {
    Idle,
    Spawn,
    Bounce,
    Stunned,
    Rip

  };

  State state;

  uint32_t nextBounce;
  sf::Vector2f dest;
  uint32_t stunTicks;
  sf::Vector2f stunVel;
  float stretchForce;

};

#endif /*SLIME*/
