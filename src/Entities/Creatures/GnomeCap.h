#ifndef GNOMECAP_H_
#define GNOMECAP_H_

#include "../Pickupable.h"

class GnomeCap : public Pickupable
{
public:
  GnomeCap(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

};

#endif /*GNOMECAP*/
