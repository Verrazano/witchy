#ifndef ENTITIES_H_
#define ENTITIES_H_

#include "Chest.h"
#include "Crystal.h"
#include "Frog.h"
#include "Puzzles/Puzzles.h"
#include "NPC/NPC.h"
#include "Player/Player.h"
#include "Player/Staff.h"
#include "Potion.h"
#include "Scenary/Scenary.h"
#include "Shard.h"
#include "Creatures/Creatures.h"
#include "Triggers/Triggers.h"
#include "Turtle.h"

#endif /*ENTITIES*/
