#include "Turtle.h"
#include "../Game.h"
#include "../Util/Maths.h"

Turtle::Turtle(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(16, 10), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/turtles.png"));

  rsprite.addAnim("idle", {0, 1, 0, 1, 0}, 0.08, false);
  rsprite.addAnim("crawl", {2, 3, 2, 4});
  rsprite.addAnim("attack", {5, 6, 7, 8, 9, 6, 5}, 0.2, false);

  setZ(1);

  setMaxHealth(1);

  state = 0;
  addTag("pushable");
  addTag("creature");
  //addTag("stunnable");
}

std::string Turtle::getName() { return "turtle"; }

void Turtle::onTick()
{
  sf::Vector2f pos = getPosition();

  if ((state == 0 || state == 2) && game->ticks % 5 == 0)
  {
    Entity* player = game->getPlayer();
    if (player != NULL)
    {
      if (dist(player->getPosition(), pos) <= 18.0f)
      {
        state = 3;
        rsprite.setAnim("attack").play();
        attackFrame = false;
      }
      else
      {
        if (state != 2)
        {
          rsprite.setAnim("crawl").play();
        }

        state = 2;
        target = player->getPosition();
        speedFactor = 1.5f;
      }
    }
  }

  if (state == 0 || state > 3)
  {
    if (actionTime.getElapsedTime().asSeconds() > 1.5)
    {
      if (state > 3)
      {
        state--;
        if (state == 3)
          state = 0;
        actionTime.restart();
      }
      else
      {
        int r = qrand() % 6;
        if (r == 0)
        {
          state = 1;
          rsprite.setAnim("idle").play();
        }
        else if (r < 3)
        {
          state = 2;
          speedFactor = 1.0f;
          float a = ((float)(qrand() % 4)) * pi / 2.0f;
          target = pos + 16.0f * unitVec(a);
          rsprite.setAnim("crawl").play();
          actionTime.restart();
        }
      }
    }
  }
  else if (state == 1 && !rsprite.getAnim().isPlaying())
  {
    state = 0;
    actionTime.restart();
  }
  else if (state == 2)
  {
    float d = dist(target, pos);
    if (d <= 0.5f)
    {
      setPosition(target);
      state = 0;
      rsprite.setAnim("idle");
      actionTime.restart();
    }
    else
    {
      sf::Vector2f m = target - pos;
      m = unitVec(m) * 0.5f * speedFactor;
      move(m.x, m.y);
    }

    if (actionTime.getElapsedTime().asSeconds() > 2)
    {
      state = 0;
      rsprite.setAnim("idle");
      actionTime.restart();
    }
  }
  else if (state == 3)
  {
    if (!attackFrame && rsprite.getAnim().getIndex() == 3)
    {
      attackFrame = true;
      sf::FloatRect box(pos.x - 24, pos.y - 24, 48, 48);
      std::vector<Entity*> entities;
      entities = game->getEntitiesInBox(box, Game::withTag({"creature"}));
      for (Entity* entity : entities)
      {
        // sf::Vector2f ePos = entity->getPosition();
        // sf::Vector2f vel = unitVec(ePos - pos)*hitPower;
        // entity->stun(vel, hitStunDuration);
        if (entity != this)
          entity->hit(1);
        // pos, frame, frames, count, life, powerMin, powerMax
        // game->addAnimatedParticles(ePos, 48, 5, 1, 0.4f, 0, 0);
      }
    }
    else if (!rsprite.getAnim().isPlaying())
    {
      state = 4;
      rsprite.setAnim("idle");
      actionTime.restart();
    }
  }
}
