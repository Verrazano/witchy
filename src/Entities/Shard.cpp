#include "Shard.h"
#include "../Game.h"

Shard::Shard(Game* game, sf::Vector2f pos, std::string type) : Pickupable(game, sf::Vector2f(10, 10), pos), type(type)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setOrigin(sf::Vector2f(8, 8));
  rsprite.setTexture(game->textures.getTexture("res/shard.png"));

  rsprite.addAnim("main", {0}, 0.2f, false);

  power = noPower;
  hasPower = false;

  if (type == "red")
  {
    rsprite.setOffset(sf::Vector2u(0, 0));
    power = redPower;
    uses = 5;
    hasPower = true;
  }
  if (type == "yellow")
    rsprite.setOffset(sf::Vector2u(8, 0));
  else if (type == "blue")
  {
    rsprite.setOffset(sf::Vector2u(16, 0));
    props.setInt("potionColor", 0x6dc3cb);
    addTag("ingredient");
  }
  else if (type == "green")
    rsprite.setOffset(sf::Vector2u(24, 0));
  else if (type == "purple")
    rsprite.setOffset(sf::Vector2u(32, 0));

  carriedAnim = "main";

  setZ(2);
  solid = false;
}

std::string Shard::getName() { return type + "_shard"; }

void Shard::onTick() { updateThrown(); }