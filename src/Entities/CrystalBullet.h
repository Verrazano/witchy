#ifndef CRYSTALBULLET_H_
#define CRYSTALBULLET_H_

#include "Projectile.h"

class CrystalBullet : public Projectile
{
public:
  CrystalBullet(Entity* shooter, sf::Vector2f pos, sf::Vector2f dir);

  std::string getName();
};

#endif /*CRYSTALBULLET*/