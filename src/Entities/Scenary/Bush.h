#ifndef BUSH_H_
#define BUSH_H_

#include "../../Entity.h"

class Bush : public Entity
{
public:
  Bush(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onHit();

};

#endif /*BUSH*/
