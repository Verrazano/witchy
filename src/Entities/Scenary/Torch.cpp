#include "Torch.h"
#include "../../Game.h"

Torch::Torch(Game* game, sf::Vector2f pos)
  : Entity(game, sf::Vector2f(16, 16), pos + sf::Vector2f(0, -2))
{
  solid = false;

  rsprite.setTexture(game->textures.getTexture("res/scenary/torch.png"));
  rsprite.addAnim("main", {1, 2, 3, 4}, 0.25f + qrandIn(-0.05, 0.05));
  rsprite.setAnim("main").play();
  rsprite.getAnim().pickRandomFrame();

}

std::string Torch::getName()
{
  return "torch";

}

void Torch::onTick()
{
  if((id + game->ticks)%17 == 0)
  {
    sf::Vector2f pos = getPosition() + sf::Vector2f(0, -8);
    int count = 1;// + qrand()%3;
    game->addParticles(pos, 64, 2, count, 2, 0.3, 0.8, -pi/3, -2*pi/3);

  }

}
