#ifndef TORCH_H_
#define TORCH_H_

#include "../../Entity.h"

class Torch : public Entity
{
public:
  Torch(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

};

#endif /*TORCH*/
