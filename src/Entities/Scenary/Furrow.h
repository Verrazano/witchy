#ifndef FURROW_H_
#define FURROW_H_

#include "../../Entity.h"

class Furrow : public Entity
{
public:
  Furrow(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

};

#endif /*FURROW*/
