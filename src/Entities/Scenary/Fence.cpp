#include "Fence.h"
#include "../../Game.h"

Fence::Fence(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/scenary/fence.png"));
  rsprite.addAnim("main", {0, 1, 2}, 0.01, false);

  addTag("breakable");
  addTag("wood");

  health = 3;

  damageSfx = SoundEffect("res/sounds/wood_damage.flac");
  damageSfx.baseVolume = 0.5f;

  breakSfx = SoundEffect("res/sounds/wood_break.flac");
  breakSfx.baseVolume = 0.5f;
}

std::string Fence::getName() { return "fence"; }

void Fence::onTick() {}

void Fence::onHit()
{
  rsprite.getAnim().setIndex(3 - health);
  rsprite.resetTextureRect();

  if (health == 2)
  {
    game->addParticles(getPosition(), 38, 0, 2, 1);
    damageSfx.play(112, getPosition());
  }
  else if (health == 1)
  {
    breakSfx.play(112, getPosition());
    game->addParticles(getPosition(), 38, 0, 4, 1);
    remTag("breakable");
    solid = false;
    collideMap = false;
  }
}
