#include "Seed.h"
#include "../../Game.h"
#include "../../Util/Maths.h"
#include "Flower.h"

Seed::Seed(Entity* entity, std::string flower)
    : Entity(entity->game, sf::Vector2f(16, 16), entity->getPosition()), flower(flower)
{
  solid = false;

  rsprite.setTexture(game->textures.getTexture("res/scenary/seed.png"));

  rsprite.addAnim("grow", {0, 1, 2}, 10.0f, false);

  setZ(-1);

  growing = false;

  game->registerListener(this);
  trackedResources.insert(entity->id);
}

std::string Seed::getName() { return "seed_" + flower; }

void Seed::onTick()
{
  if (growing && !rsprite.getAnim().isPlaying())
  {
    health = -1;
  }
}

void Seed::onDie() { game->addEntity(new Flower(game, getPosition(), flower)); }

void Seed::onDeath(Entity* entity)
{
  if (trackedResources.count(entity->id))
  {
    trackedResources.erase(entity->id);
    if (!entity->trackedResources.empty())
    {
      trackedResources.insert(entity->trackedResources.begin(), entity->trackedResources.end());
    }
    else if (trackedResources.empty())
    {
      growing = true;
      rsprite.getAnim().play();
      game->unregisterListener(this);
    }
  }
}
