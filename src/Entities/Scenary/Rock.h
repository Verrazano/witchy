#ifndef ROCK_H_
#define ROCK_H_

#include "../Pickupable.h"

// all pickupables should probably act like the rock
class Rock : public Pickupable
{
public:
  Rock(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onCollide(Entity* entity);
};

#endif /*ROCK*/
