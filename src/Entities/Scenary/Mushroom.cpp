#include "Mushroom.h"
#include "../../Game.h"

Mushroom::Mushroom(Game* game, sf::Vector2f pos)
    : Pickupable(game, sf::Vector2f(10, 10), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/scenary/mushrooms.png"));

  rsprite.addAnim("main", {0});
  rsprite.addAnim("carried", {1});

  carriedAnim = "carried";

  addTag("ingredient");

}

std::string Mushroom::getName() { return "mushroom"; }

void Mushroom::onTick()
{
  updateThrown();
}
