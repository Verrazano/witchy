#include "Bush.h"
#include "../../Game.h"

Bush::Bush(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(16, 16), pos)
{
  rsprite.setTexture(game->textures.getTexture("res/scenary/bush.png"));
  rsprite.addAnim("main", {0}, 0.1f, false);
  rsprite.addAnim("break", {1, 2, 3}, 0.2f, false);

  solid = true;
  addTag("breakable");
  addTag("player_breakable");
}

std::string Bush::getName()
{
  return "bush";
}

void Bush::onTick()
{
  Anim& anim = rsprite.getAnim();
  if(anim.getName() == "break")
  {
    if(!anim.isPlaying())
    {
      kill();

    }

  }
}

void Bush::onHit()
{
  rsprite.setAnim("break").play();
  solid = false;
  game->addParticlesInCloud(getPosition(), 8.0f, 30, 0, 4, 1, 0.1, 0.4);

}
