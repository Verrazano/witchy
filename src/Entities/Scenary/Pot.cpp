#include "Pot.h"
#include "../../Game.h"
#include "../../Util/Maths.h"

Pot::Pot(Game* game, sf::Vector2f pos) : Pickupable(game, sf::Vector2f(12, 12), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setOrigin(sf::Vector2f(8, 10));
  rsprite.setTexture(game->textures.getTexture("res/scenary/pots.png"));

  rsprite.addAnim("main", {0}, 0.06f, false);
  rsprite.setAnim("main");

  rsprite.addAnim("break", {1, 2, 3}, 0.1f, false);

  solid = true;

  addTag("pushable");
  addTag("breakable");
  addTag("player_breakable");

  health = 2;

  carriedAnim = "main";
}

std::string Pot::getName() { return "pot"; }

void Pot::onTick()
{
  if (updateThrown() && health == 2)
  {
    this->hit(1);
  }

  if(health == 1)
  {
    if (health == 1 && !rsprite.getAnim().isPlaying())
    {
      health = -1;
    }
  }

}

void Pot::onLand()
{
  if (health == 2)
  {
    solid = true;
    this->hit(1);
  }
}

bool Pot::doesCollide(Entity* other) { return Pickupable::doesCollide(other) || !thrown; }

void Pot::onHit()
{
  rsprite.setAnim("break").play();
  remTag("pickupable");
  solid = false;
  vel = sf::Vector2f(0, 0);
  game->addParticlesInCloud(getPosition(), 16.0f, 38, 0, 8, 1, 0.1, 0.4);
}

void Pot::onCollide(Entity* entity)
{
  if (thrown && health == 2)
  {
    this->hit(1);
  }
  /*vel *= -0.7f;
  if ((entity->hasTag("creature") || entity->hasTag("glass")) &&
  entity->getName() != "grey_slime")
  {
    entity->hit(1);
  }*/
}
