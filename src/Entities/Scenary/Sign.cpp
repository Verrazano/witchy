#include "Sign.h"
#include "../../Game.h"

Sign::Sign(Game* game, sf::Vector2f pos, std::string text) : Entity(game, sf::Vector2f(16, 8), pos), readable(this)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setOrigin(8, 10);
  rsprite.setTexture(game->textures.getTexture("res/scenary/sign.png"));
  rsprite.addAnim("main", {0, 1, 2}, 0.01, false);

  addTag("breakable");
  addTag("wood");

  std::map<std::string, Readable::Dialog> tree;
  tree["start"].texts.push_back(text);
  tree["start"].endState = 3;

  readable.set(tree);
  readable.current_state = "start";
  readable.setBubbleOrigin(sf::Vector2f(8, 26));

  health = 3;

  damageSfx = SoundEffect("res/sounds/wood_damage.flac");
  damageSfx.baseVolume = 0.5f;

  breakSfx = SoundEffect("res/sounds/wood_break.flac");
  breakSfx.baseVolume = 0.5f;

  setZ(4);
}

std::string Sign::getName() { return "sign"; }

void Sign::onTick() { readable.onTick(); }

void Sign::onMessage(Message msg) { readable.onMessage(msg); }

void Sign::onHit()
{
  rsprite.getAnim().setIndex(3 - health);
  rsprite.resetTextureRect();

  if (health == 2)
  {
    game->addParticles(getPosition(), 38, 0, 2, 1);
    damageSfx.play(112, getPosition());
  }
  else if (health == 1)
  {
    breakSfx.play(112, getPosition());
    game->addParticles(getPosition(), 38, 0, 4, 1);
    remTag("breakable");
    solid = false;
    collideMap = false;
  }
}
