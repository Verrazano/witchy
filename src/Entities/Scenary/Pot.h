#ifndef POT_H_
#define POT_H_

#include "../Pickupable.h"

class Pot : public Pickupable
{
public:
  Pot(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onLand();

  void onHit();

  bool doesCollide(Entity* other);

  void onCollide(Entity* entity);
};

#endif /*ROCK*/
