#include "Flower.h"
#include "../../Game.h"
#include "Seed.h"

Flower::Flower(Game* game, sf::Vector2f pos, std::string flower, bool fromChest)
    : Pickupable(game, sf::Vector2f(10, 10), pos), flower(flower)
{
  solid = false;

  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/scenary/flowers.png"));

  rsprite.addAnim("wave", {0, 1, 2, 3, 4, 5}, 0.2f, false);
  rsprite.addAnim("carried", {6}, 0.2f, false);

  addTag("flower");
  addTag("ingredient");
  addTag("pickup_first");
  if (flower == "dahlia")
  {
    rsprite.setOffset(sf::Vector2u(0, 0));
    props.setInt("potionColor", 0xd34549);
  }
  else if (flower == "nightshade")
  {
    rsprite.setOffset(sf::Vector2u(0, 8));
    props.setInt("potionColor", 0xa665a6);
  }
  else if (flower == "daisy")
  {
    rsprite.setOffset(sf::Vector2u(0, 16));
    props.setInt("potionColor", 0xdbd75d);
  }
  else if(flower == "bluebell")
  {
    rsprite.setOffset(sf::Vector2u(0, 24));
    props.setInt("potionColor", 0x6dc3cb);
  }

  setZ(0);
  carriedAnim = "carried";

  if (fromChest)
  {
    rsprite.setAnim("carried");
  }

  seedDropped = fromChest;
}

std::string Flower::getName() { return "flower_" + flower; }

void Flower::onTick()
{
  updateThrown();

  if (!seedDropped)
  {
    if (game->ticks % (25 + id) == 0)
    {
      rsprite.pickRandomFrame();
    }
  }
}

void Flower::onPickup()
{
  if (!seedDropped)
  {
    seedDropped = true;
    game->addEntity(new Seed(this, flower));
  }
}

void Flower::onMessage(Message msg)
{
  if (msg.name == "cauldron_take")
  {
    if (!seedDropped)
    {
      seedDropped = true;
      game->addEntity(new Seed(this, flower));
    }
  }
}
