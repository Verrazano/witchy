#ifndef MUSHROOM_H_
#define MUSHROOM_H_

#include "../Pickupable.h"

class Mushroom : public Pickupable
{
public:
  Mushroom(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

};

#endif /*FLOWER*/
