#ifndef FENCE_H_
#define FENCE_H_

#include "../../Entity.h"
#include "../../SoundEffect.h"

class Fence : public Entity
{
public:
  Fence(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onHit();

  SoundEffect damageSfx;
  SoundEffect breakSfx;
};

#endif /*SIGN*/
