#include "Rock.h"
#include "../../Game.h"
#include "../../Util/Maths.h"

Rock::Rock(Game* game, sf::Vector2f pos) : Pickupable(game, sf::Vector2f(8, 8), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/entities.png"));

  int f = 26 + qrand() % 2;
  rsprite.addAnim("main", {f}, 0.1f, false);
  rsprite.setAnim("main");

  solid = false;

  carriedAnim = "main";

  props.setInt("potionColor", 0x8696a2);
  addTag("ingredient");
}

std::string Rock::getName() { return "rock"; }

void Rock::onTick() { updateThrown(); }

void Rock::onCollide(Entity* entity)
{
  vel *= -0.7f;
  if ((entity->hasTag("creature") || entity->hasTag("glass")) && entity->getName() != "grey_slime")
  {
    entity->hit(1);
  }
}
