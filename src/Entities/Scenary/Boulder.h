#ifndef BOULDER_H_
#define BOULDER_H_

#include "../Pickupable.h"

class Boulder : public Pickupable
{
public:
  Boulder(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();

  void onDie();

  void onHit();

  void onEffect(std::string name);
  void onEffectEnd(std::string name);
};

#endif /*Boulder*/
