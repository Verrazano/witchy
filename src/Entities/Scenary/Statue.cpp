#include "Statue.h"
#include "../../Game.h"

Statue::Statue(Game* game, sf::Vector2f pos, std::string text, int index)
    : Entity(game, sf::Vector2f(16, 16), pos + sf::Vector2f(0, -13)), readable(this)
{
  rsprite.setSize(sf::Vector2f(16, 32));
  rsprite.setOrigin(8, 14);
  rsprite.setFrameSize(sf::Vector2u(8, 16));
  rsprite.setTexture(game->textures.getTexture("res/scenary/statues.png"));
  rsprite.addAnim("main", {index}, 0.01, false);

  std::map<std::string, Readable::Dialog> tree;
  tree["start"].texts.push_back(text);
  tree["start"].endState = 3;

  readable.set(tree);
  readable.current_state = "start";

  readable.setBubbleOrigin(sf::Vector2f(8, 11));
  readable.setReadRadius(24.0f);

  setZ(4);
}

std::string Statue::getName() { return "statue"; }

void Statue::onTick() { readable.onTick(); }

void Statue::onMessage(Message msg) { readable.onMessage(msg); }
