#ifndef FLOWER_H_
#define FLOWER_H_

#include "../Pickupable.h"

class Flower : public Pickupable
{
public:
  Flower(Game* game, sf::Vector2f pos, std::string flower, bool fromChest = false);

  std::string getName();

  void onPickup();

  void onTick();

  void onMessage(Message msg);

  std::string flower;
  bool seedDropped;
};

#endif /*FLOWER*/
