#ifndef SIGN_H_
#define SIGN_H_

#include "../../Entity.h"
#include "../../SoundEffect.h"
#include "../Readable.h"

class Sign : public Entity
{
public:
  Sign(Game* game, sf::Vector2f pos, std::string text);

  std::string getName();

  void onTick();

  void onMessage(Message msg);

  void onHit();

  Readable readable;

  SoundEffect damageSfx;
  SoundEffect breakSfx;
};

#endif /*SIGN*/
