#ifndef STATUE_H_
#define STATUE_H_

#include "../../Entity.h"
#include "../../SoundEffect.h"
#include "../Readable.h"

class Statue : public Entity
{
public:
  Statue(Game* game, sf::Vector2f pos, std::string text, int index = 0);

  std::string getName();

  void onTick();

  void onMessage(Message msg);

  Readable readable;
};

#endif /*SIGN*/
