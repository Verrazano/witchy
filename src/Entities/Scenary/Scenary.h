#ifndef SCENARY_H_
#define SCENARY_H_

#include "Bones.h"
#include "Boulder.h"
#include "Fence.h"
#include "Flower.h"
#include "Mushroom.h"
#include "Pot.h"
#include "Rock.h"
#include "Sign.h"
#include "Statue.h"
#include "Torch.h"
#include "Bush.h"
#include "Furrow.h"

#endif /*SCENARY*/
