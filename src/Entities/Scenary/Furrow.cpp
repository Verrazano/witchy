#include "Furrow.h"
#include "../../Game.h"
#include "Seed.h"
#include "Flower.h"

Furrow::Furrow(Game* game, sf::Vector2f pos) : Entity(game, sf::Vector2f(16, 16), pos)
{
  rsprite.setTexture(game->textures.getTexture("res/scenary/seed.png"));
  rsprite.addAnim("main", {3}, 0.1f, false);

  solid = false;

}

std::string Furrow::getName()
{
  return "furrow";

}

void Furrow::onTick()
{
  if((id + game->ticks)%17 == 0)
  {
    std::vector<Entity*> flowers = game->getEntitiesInRadius(getPosition(), 16, Game::withTag({"flower"}));
    if(flowers.size() > 0)
    {
      Flower* f = (Flower*)flowers[0];
      f->setPosition(getPosition());
      game->addEntity(new Seed(f, f->flower));
      f->kill();
      this->kill();

    }

  }

}
