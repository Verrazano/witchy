#ifndef SEED_H_
#define SEED_H_

#include "../../Entity.h"

// TODO: make the seed track it's flower all the way to the use of a potion not
// just the cauldron
class Seed : public Entity
{
public:
  Seed(Entity* entity, std::string flower);

  std::string getName();

  void onTick();

  void onDie();

  void onDeath(Entity* entity);

  std::string flower;
  bool growing;
};

#endif /*SEED*/
