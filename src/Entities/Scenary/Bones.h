#ifndef BONES_H_
#define BONES_H_

#include "../Pickupable.h"

class Bones : public Pickupable
{
public:
  Bones(Game* game, sf::Vector2f pos);

  std::string getName();

  void onTick();
};

#endif /*ROCK*/
