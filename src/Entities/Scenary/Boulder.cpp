#include "Boulder.h"
#include "../../Game.h"
#include "../../Util/Maths.h"
#include "Rock.h"

Boulder::Boulder(Game* game, sf::Vector2f pos) : Pickupable(game, sf::Vector2f(16, 16), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setTexture(game->textures.getTexture("res/entities.png"));

  rsprite.addAnim("main", {36}, 0.1f, false);
  rsprite.setAnim("main");

  rsprite.addAnim("damaged", {37}, 0.1f, false);

  solid = true;

  carriedAnim = "main";

  remTag("pickupable");
  addTag("shrinkable");
  addTag("breakable");

  health = 2;
}

std::string Boulder::getName() { return "boulder"; }

void Boulder::onDie()
{
  Pickupable* e = new Rock(game, getPosition());
  e->throwMe(unitVec(qrandIn(0, pi * 2)) * qrandIn(1.0f, 3.0f));
  game->addEntity(e);
}

void Boulder::onTick() { updateThrown(); }

void Boulder::onHit() { rsprite.setAnim("damaged"); }

void Boulder::onEffect(std::string name)
{
  if (name == "shrink")
  {
    addTag("pickupable");
    addTag("pushable");
  }
}

void Boulder::onEffectEnd(std::string name)
{
  if (name == "shrink")
  {
    remTag("pickupable");
    remTag("pushable");
  }
}
