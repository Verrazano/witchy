#include "Bones.h"
#include "../../Game.h"
#include "../../Util/Maths.h"

Bones::Bones(Game* game, sf::Vector2f pos) : Pickupable(game, sf::Vector2f(11, 11), pos)
{
  rsprite.setSize(sf::Vector2f(16, 16));
  rsprite.setOrigin(sf::Vector2f(8, 8));
  rsprite.setTexture(game->textures.getTexture("res/scenary/bones.png"));

  int index = 0;//rand() % 4;

  rsprite.addAnim("main", {index}, 0.1f, false);

  props.setInt("potionColor", 0xdfefd7);
  carriedAnim = "main";
  addTag("ingredient");
}

std::string Bones::getName() { return "bones"; }

void Bones::onTick() { updateThrown(); }
