#include "Readable.h"
#include "../Game.h"
#include "Player/Player.h"

Readable::Readable(Entity* e) : e(e), reading(false), playerId(0), index(0)
{
  if (e != NULL)
  {
    e->addTag("readable");
    bubble.setSize(sf::Vector2f(16, 16));
    bubble.setFillColor(sf::Color::Transparent);
    bubble.setTexture(e->game->textures.getTexture("res/bubble.png"));
    bubble.setTextureRect(sf::IntRect(0, 0, 8, 8));
    bubble.setOrigin(sf::Vector2f(8, 26));
  }
  setReadRadius(16.0f);

  current_state = "start";
  tree["start"].texts.push_back("Where am I?");
}

void Readable::set(std::map<std::string, Dialog> tree, std::string name)
{
  this->tree = tree;
  this->name = name;

  if (e != NULL)
  {
    e->props.setString("readable:text", text);
    e->props.setString("readable:name", name);
  }
}

void Readable::setBubbleOrigin(sf::Vector2f origin) { bubble.setOrigin(origin); }

void Readable::onTick()
{
  if (e != NULL)
  {
    if (reading)
    {
      Player* player = (Player*)(e->game->getEntityById(playerId));
      if (player == NULL)
      {
        reading = false;
        return;
      }

      bool action1 = player->c.justPressed("action1");
      bool action2 = player->c.justPressed("action2");

      if (index < text.size() - 1)
      {
        if (timer.getElapsedTime().asSeconds() > 0.001)
        {
          index += 4;
          // while(dialog.at(index) != ' ' && index < dialog.size()-1) index++;
          timer.restart();
          if (index >= text.size())
            index = text.size() - 1;
        }

        if (action1 || action2)
        {
          index = text.size() - 1;
        }
      }
      else if (action1 || action2)
      {
        Dialog d = tree[current_state];
        dIndex++;
        if(dIndex >= d.texts.size())
        {
          if(d.endState == 4)
          {
            reading = false;
            player->lockControls = false;
            e->game->playCutscene(d.target);
          }
          else if(d.endState == 3)
          {
            reading = false;
            player->lockControls = false;
          }
          else if(d.endState == 2)
          {
            current_state = d.target;
            reading = false;
            player->lockControls = false;
          }
          else if(d.endState == 1)
          {
            current_state = d.target;
            dIndex = 0;
            text = tree[current_state].texts[dIndex];
            index = 1;
            timer.restart();

          }
          else if(d.endState == 0)
          {
            if(waitQuestion > 0)
            {
              if(action1)
              {
                current_state = d.target1;
                dIndex = 0;
                text = tree[current_state].texts[dIndex];
                index = 1;
                timer.restart();
                waitQuestion = 0;
              }
              else if(action2)
              {
                current_state = d.target2;
                dIndex = 0;
                text = tree[current_state].texts[dIndex];
                index = 1;
                timer.restart();
                waitQuestion = 0;
              }
            }
            else
            {
              waitQuestion++;

            }

          }

        }
        else
        {
          text = d.texts[dIndex];
          index = 1;
          timer.restart();

        }

      }

      if(waitQuestion > 0)
      {
        Dialog d = tree[current_state];
        e->game->displayQuestionDialog(text, name, d.option1, d.option2);

      }
      else if (name != "")
      {
        e->game->displayNamedDialog(text.substr(0, index + 1), name);
      }
      else
      {
        e->game->displayDialog(text.substr(0, index + 1));
      }

    }
    else
    {
      bubble.setFillColor(sf::Color::Transparent);
      if (e->game->getEntitiesInRadius(e->getPosition(), radius, Game::withName({"player"})).size() != 0)
      {
        bubble.setFillColor(sf::Color::White);
        bubble.setPosition(e->getPosition());
        e->game->topLayer.push_back(&bubble);
      }
    }
  }
}

void Readable::onMessage(Entity::Message msg)
{
  if (msg.name == "read")
  {
    waitQuestion = 0;
    bubble.setFillColor(sf::Color::Transparent);
    reading = true;
    playerId = msg.sender->id;
    Player* player = (Player*)(msg.sender);
    player->lockControls = true;
    timer.restart();
    index = 1;
    dIndex = 0;
    text = tree[current_state].texts[dIndex];
  }
}

void Readable::setReadRadius(float r)
{
  radius = r;
  e->props.setFloat("read_distance", r);
}
