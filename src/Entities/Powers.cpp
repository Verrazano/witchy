#include "../Game.h"
#include "CrystalBullet.h"
#include "Player/Player.h"
#include "Shard.h"

void noPower(Player* p) {}

void redPower(Player* p)
{
  sf::Vector2f d = p->getDirVec();
  p->game->addEntity(new CrystalBullet(p, p->getPosition(), d));
}