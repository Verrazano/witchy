#ifndef CRYSTAL_H_
#define CRYSTAL_H_

#include "../Entity.h"

class Crystal : public Entity
{
public:
  Crystal(Game* game, sf::Vector2f pos);

  std::string getName();

  void onDie();

  void onCollideMap();

  void onTick();

  int state; // 0=hovering,1=idle,2=attack
  sf::Clock actionTime;
  sf::Vector2f target;
  bool shot;
};

#endif /*CRYSTAL*/