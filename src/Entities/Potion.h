#ifndef POTION_H_
#define POTION_H_

#include "Pickupable.h"

class Potion : public Pickupable
{
public:
  Potion(Game* game, sf::Vector2f pos, std::string potion);

  std::string getName();

  void onTick();

  void onDie();

  void onPickup();
  void onThrown();
  void onLand();

  std::string potion;

};

/*
Shrink - shrinks sprites and hitboxes  - done
Speed - speeds up movement - done
Slow - slows down movement
Poison - does damage over time - done
Blackhole - sucks this towards the location of impact
Invisible - enemies can't see and target you
Strength - knock back and throw power increased (maybe do damage when you
normally can't)
Grow - grows sprites and hitboxes
Blast - push things away - done (needs to look better)
Explode - Blast + damage
Animorph - turn things into harmless creatures
Teleport - go to impact location
Invulnderability - don't take damage
Turn to stone - stuff becomes stone and is frozen in place
Berserk - enemies attack other enemies
Confuse - enemies go in the wrong direction
Sleep - enemies fall asleep
Wall - summon a wall at location
Beam - everything shoots and energy beam
Pathfinder - show the way
Fire - things become perpeturally on fire and take damage if they aren't immune
Melt - melt hard/metal things
Portal - first use, takes things away, second use brings them back.
Slime in a bottle - Creates a slime
electricty - make everything electrical charged and shoot out electrictity
shield - make a shield in front of you
Levitate - make it so you (or enemies) can walk over pits
Summon dead - all corpses come back to life
Clone - create copies of things
Curse - attacking causes damage to self
magic missile - magic bolts appear that home and attack enemey
Light - creates cool ligth effects (maybe for dark areas)
Pull - whatever is at place of impact comes to place of cast
Water - blast things back in a line and leave slippery trail
Incopreal - walk through certain walls
Love - makes enemy follow you around and attack other enemies
*/

#endif /*POTION*/
