#ifndef SOUNDEFFECT_H_
#define SOUNDEFFECT_H_

#include <SFML/Audio.hpp>
#include <map>
#include <string>

class SoundEffect : public sf::Sound
{
public:
  SoundEffect();
  SoundEffect(std::string path);

  void play(float minDist, sf::Vector2f pos);

  void play();

  float baseVolume; // 0-1

  static void cleanup();

  static std::map<std::string, sf::SoundBuffer*> buffers;

  static float globalVolume;
  static sf::Vector2f listener;
};

#endif /*SOUNDEFFECT*/
