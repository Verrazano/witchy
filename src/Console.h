#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>

//#include <iomanip>

class Console
{
public:
  Console();
  Console(sf::Vector2f size);

  ~Console();

  void update();

  bool consoleOpen;

  char* buffer;

  std::ostringstream* oss;
  sf::Vector2f size;

  std::vector<std::string> lines;
};

#endif /*CONSOLE*/
