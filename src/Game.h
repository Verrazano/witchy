#ifndef GAME_H_
#define GAME_H_

#include "Console.h"
#include "EngineConfig.h"
#include "Map/Map.h"
#include "ParticleManager.h"
#include "TextureManager.h"
#include "Util/Maths.h"
#include "Util/WrappedText.h"
#include "Camera.h"
#include <SFML/Graphics.hpp>
#include <list>
#include <map>
#include <functional>

class Cutscene;
class Entity;

class Game
{
public:
  Game();

  ~Game();

  bool isRunning();

  void tick();

  bool isCollidingAt(Entity* entity, sf::Vector2f pos);

  void resolveIntersect(Entity* entity);

  std::vector<Entity*> getEntitiesInBox(sf::FloatRect rect, std::function<bool(Entity*)> selector = [](Entity*){ return true; });
  std::vector<Entity*> getEntitiesInRadius(sf::Vector2f pos, float range, std::function<bool(Entity*)> selector = [](Entity*){ return true; });

  static std::function<bool(Entity*)> withName(std::vector<std::string> names);
  static std::function<bool(Entity*)> withTag(std::vector<std::string> tags);

  Entity* getPlayer();

  void displayDialog(std::string text);
  void displayNamedDialog(std::string text, std::string name);
  void displayQuestionDialog(std::string text, std::string name, std::string option1, std::string option2);

  void displayAreaTitle(std::string title);

  Entity* getEntityById(unsigned int id);
  void removeEntityWithId(unsigned int id); // does not call onDie
  unsigned int addEntity(Entity* entity);

  void registerListener(Entity* e);
  void unregisterListener(Entity* e);

  void addParticle(sf::Vector2f pos, sf::Vector2f vel, unsigned int frame, float life = 0.3f);

  void addParticles(sf::Vector2f pos, unsigned int frame, unsigned int frames, unsigned count, float life = 0.3f,
                    float powerMin = 0.2f, float powerMax = 1.0f, float angleMin = 0.0f, float angleMax = 2 * pi);

  void addParticlesInCloud(sf::Vector2f pos, float radius, unsigned int frame, unsigned int frames, unsigned count,
                           float life = 0.3f, float powerMin = 0.2f, float powerMax = 1.0f);

  void addAnimatedParticles(sf::Vector2f pos, unsigned int frame, unsigned int frames, unsigned count,
                            float life = 0.3f, float powerMin = 0.2f, float powerMax = 1.0f, float angleMin = 0.0f,
                            float angleMax = 2 * pi);

  std::string currentMapPath();

  void switchMap(std::string nextMap);

  void playCutscene(std::string name);

  EngineConfig config;

  sf::RenderWindow window;

  sf::View guiView;
  Camera camera;

  sf::Font consolas;
  sf::Font pixeltype;
  sf::Font alagard;

  sf::Clock frameTimer;
  int framerate;
  sf::Text framerateText;

  TextureManager textures;
  sf::Texture worldTex;

  Map map;
  std::string nextMap;
  std::string baseMap;
  std::map<std::string, Map> maps;
  std::vector<std::string> mapsToLoad;

  std::vector<Entity*> entities;
  std::list<Entity*> listeners;

  std::vector<sf::Drawable*> topLayer;
  std::vector<sf::Drawable*> guiLayer;

  sf::Texture* textbox;
  sf::Texture* textboxname;
  sf::Texture* textboxquestion;
  sf::RectangleShape dialogBack;
  WrappedText dialogText;
  sf::Text dialogNameText;
  sf::Text dialogOption1;
  sf::Text dialogOption2;
  sf::RectangleShape dialogDebug;

  bool cutscenePlaying;
  Cutscene* cutscene;
  std::map<std::string, Cutscene*> cutscenes;

  sf::Text areaTitleText;
  int areaTitleTick;
  std::string lastAreaTitle;

  ParticleManager particles;
  sf::Vector2u particleTexSize;

  std::vector<Entity*> entitiesToAdd;
  bool removeAtEnd;
  std::vector<Entity*> listenersToUnregister;

  sf::Clock deltaClock;
  Console console;

  unsigned int nextId;

  bool hasFocus;
  bool invalidateZ;

  bool shadersAvailable;

  unsigned int ticks;
};

#endif /*GAME*/
