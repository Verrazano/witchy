#include "Entity.h"
#include "Game.h"
#include "Map/Map.h"
#include "Util/Maths.h"
#include "Util/Sprites.h"
#include "Util/Strings.h"

Entity::Entity(Game* game, sf::Vector2f size, sf::Vector2f pos) : game(game)
{
  useCorrections = true;
  bounds.setSize(sf::Vector2f(size.x - 0.5f, size.y - 0.5f));

  sf::Vector2f origin = size / 2.0f;
  bounds.setOrigin(origin.x - 0.25f, origin.y - 0.25f);

  bounds.setFillColor(sf::Color::Transparent);
  bounds.setOutlineThickness(0.25f);
  bounds.setOutlineColor(sf::Color::Red);

  solid = true;
  noDelete = false;

  bounds.setPosition(pos);
  sprite.setPosition(pos);
  rsprite = Sprite(size, sf::Vector2u(8, 8));
  rsprite.setPosition(pos);

  health = 100;
  maxHealth = 100;

  collidingWith = NULL;

  speedFactor = 1.0f;

  setZ(1);

  showHearts = false;
  heartsAlpha = 0;
  hitRed = 0;

  id = 0;

  noOnDie = false;

  registered = false;

  collideMap = true;

  solidFilter = Tile::Flag::Solid | Tile::Flag::Death | Tile::Flag::Ledge;

  didMove = false;
  lastPos = getPosition();

  destroyOnSwitch = false;
  keepOnSwitch = false;

  // tileMarker.setRadius(1);
  // tileMarker.setOrigin(1, 1);
  // tileMarker.setFillColor(sf::Color::Black);
}

Entity::~Entity() {}

void Entity::onTick() {}

void Entity::onDie() {}

bool Entity::doesCollide(Entity* e) { return true; }

void Entity::onCollide(Entity* e) {}

void Entity::onCollideMap() {}

void Entity::onEffect(std::string name) {}

void Entity::onEffectEnd(std::string name) {}

void Entity::onDeath(Entity* other) {}

void Entity::onCreate(Entity* other) {}

void Entity::onHit() {}

bool Entity::move(sf::Vector2f m) { return move(m.x, m.y); }

bool Entity::move(float x, float y)
{
  lastPos = getPosition();

  collidingWith = NULL;
  Map& map = game->map;

  sf::Vector2f f = bounds.getPosition();

  bool success = true;

  if (useCorrections)
  {
    sf::Vector2f tilePos = map.toWorldSpace(map.toTileSpace(f));

    float correction = 0.5f;

    if (y == 0)
    {
      if (tilePos.y < f.y)
      {
        y -= correction;
      }
      else if (tilePos.y > f.y)
      {
        y += correction;
      }
    }
    else if (x == 0)
    {
      if (tilePos.x < f.x)
      {
        x -= correction;
      }
      else if (tilePos.x > f.x)
      {
        x += correction;
      }
    }
  }

  rem.x += x;

  int mX = (int)rem.x;
  int signX = mX < 0 ? -1 : 1;

  if (mX != 0)
  {
    rem.x -= mX;
    while (mX != 0)
    {
      if (!game->isCollidingAt(this, sf::Vector2f(f.x + signX, f.y)))
      {
        f.x += signX;
        mX -= signX;
      }
      else
      {
        // TODO: add a call back
        success = false;
        break;
      }
    }
  }

  rem.y += y;

  int mY = (int)rem.y;
  int signY = mY < 0 ? -1 : 1;

  if (mY != 0)
  {
    rem.y -= mY;
    while (mY != 0)
    {
      if (!game->isCollidingAt(this, sf::Vector2f(f.x, f.y + signY)))
      {
        f.y += signY;
        mY -= signY;
      }
      else
      {
        // TODO: add a callback
        success = false;
        break;
      }
    }
  }

  bounds.setPosition(f);
  sprite.setPosition(bounds.getPosition());
  rsprite.setPosition(bounds.getPosition());

  if (collidingWith != NULL)
  {
    onCollide(collidingWith);
    collidingWith->onCollide(this);
  }

  // sf::Vector2i t = map.toTileSpace(f);
  // tileMarker.setPosition(map.toWorldSpace(t));
  if (!success)
  {
    onCollideMap();
  }

  didMove = lastPos != getPosition();

  return success;
}

void Entity::resolveIntersect()
{
  game->resolveIntersect(this);
  setPosition(getPosition());

}

void Entity::setPosition(sf::Vector2f pos)
{
  pos.x = (int)pos.x;
  pos.y = (int)pos.y;

  bounds.setPosition(pos);
  sprite.setPosition(pos);
  rsprite.setPosition(pos);
}

sf::Vector2f Entity::getPosition() { return bounds.getPosition(); }

sf::FloatRect Entity::getBounds() { return bounds.getGlobalBounds(); }

void Entity::addTag(std::string tag)
{
  if (!hasTag(tag))
  {
    tags.push_back(tag);
  }
}

bool Entity::hasTag(std::string tag)
{
  for (int i = 0; i < tags.size(); i++)
  {
    if (tags[i] == tag)
    {
      return true;
    }
  }

  return false;
}

bool Entity::hasTag(std::vector<std::string> tag)
{
  for(int i = 0; i <tags.size(); i++)
  {
    for(int j = 0; j< tag.size(); j++)
    {
      if(tags[i] == tag[j])
      {
        return true;
      }
    }

  }

  return false;

}

void Entity::remTag(std::string tag)
{
  std::vector<std::string>::iterator it;
  for (it = tags.begin(); it != tags.end();)
  {
    if (*it == tag)
    {
      tags.erase(it);
      return;
    }

    it++;
  }
}

void Entity::update()
{
  rsprite.update();

  onTick();

  dispatchMessages();
}

void Entity::setZ(int nz)
{
  if (nz != z)
  {
    z = nz;
    game->invalidateZ = true;
  }
}

void Entity::dispatchMessages()
{
  for (Message& msg : messages)
  {
    onMessage(msg);
  }

  messages.clear();
}

void Entity::sendMessage(Entity* sender, std::string name, Properties props)
{
  Message msg = makeMessage(sender, name, props);
  messages.push_back(msg);
}

Entity::Message Entity::makeMessage(Entity* sender, std::string name, Properties props)
{
  Message msg;
  msg.sender = sender;
  msg.name = name;
  msg.props = props;

  return msg;
}

void Entity::onMessage(Message msg) {}

void Entity::draw(sf::RenderWindow& window, sf::RenderStates states)
{
  /*if(showHearts)
  {
          drawHearts();

  }*/

  window.draw(sprite, states);
  rsprite.draw(window, states);

  if (game->config.debug)
  {
    // window.draw(tileMarker);
    window.draw(bounds);
  }
}

void Entity::hit(int damage)
{
  // heartsAlpha = 255;
  //hitRed = 119;
  health -= damage;

  /*sf::Texture* texture = game->textures.getTexture("res/icons.png");
  sf::Vector2u size = texture->getSize();

  for (int i = hearts.size() - 1; i >= 0; i--)
  {
    if (health <= i)
    {
      hearts[i].setTextureRect(getFrame(11, sf::Vector2u(8, 8), size));
    }
  }*/

  onHit();
}

void Entity::setMaxHealth(int max)
{
  maxHealth = max;
  health = maxHealth;
  /*hearts.clear();

  sf::Texture* texture = game->textures.getTexture("res/icons.png");
  sf::Vector2u size = texture->getSize();

  sf::RectangleShape heart(sf::Vector2f(8, 8));
  heart.setOrigin(4, 4);
  heart.setTexture(texture);
  heart.setTextureRect(getFrame(10, sf::Vector2u(8, 8), size));

  for (int i = 0; i < maxHealth; i++)
  {
    hearts.push_back(heart);
  }

  showHearts = true;*/
}

void Entity::drawHearts()
{
  if (heartsAlpha > 0)
  {
    sf::Vector2f mid = sprite.getPosition();
    mid.y -= sprite.getSize().y / 2.0f + 4.0f;

    float start = mid.x - maxHealth * 10.0f / 2.0f + 5.0f;

    for (int i = 0; i < hearts.size(); i++)
    {
      hearts[i].setFillColor(sf::Color(255, 255, 255, heartsAlpha));
      hearts[i].setPosition(start, mid.y);
      start += 10;
      game->topLayer.push_back(&hearts[i]);
    }

    heartsAlpha -= 5;
  }

  if (hitRed > 0)
  {
    rsprite.sprite.setFillColor(sf::Color(hitRed + 136, 255 - hitRed, 255 - hitRed));
    hitRed -= 17;

    if (hitRed == 0)
    {
      rsprite.sprite.setFillColor(sf::Color::White);
    }
  }
}

void Entity::onMapSwitch() {}

void Entity::kill()
{
  health = -1;

}
