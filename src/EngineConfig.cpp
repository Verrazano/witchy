#include "EngineConfig.h"
#include <fstream>
#include <iostream>
#include <math.h>

EngineConfig::EngineConfig(std::string path) : configPath(path)
{
  // default values
  scale = 4;
  debug = false;
  baseMapPath = "mapeditor/maps/";
  mapPath = "map.txt";

  width = 160;
  height = 144;

  volume = 100.0f;

  std::fstream f;
  f.open(configPath.c_str());

  if (f)
  {
    f >> width;
    f >> height;
    f >> scale;
    f >> debug;
    f >> baseMapPath;
    f >> mapPath;
    f >> volume;
  }
  else
  {
    std::cout << "couldn't open engine config: " << configPath << "\n";
    return;
  }

  wSize.x = width;
  wSize.y = height;

  //calculate scale and viewport position
  float windowRatio = width/(float)height;
  float viewRatio = 160.0f/144.0f; //fixed because we want to display a specific room size

  int wScale = width/160.0f;
  int hScale = height/144.0f;

  scale = fmin(wScale, hScale);

  viewPort = sf::FloatRect(0, 0, 1, 1);

  viewPort.width = 160*scale/(float)width;
  viewPort.left = (1 - viewPort.width)/2.0f;
  viewPort.height = 144*scale/(float)height;
  viewPort.top = (1 - viewPort.height)/2.0f;

}
