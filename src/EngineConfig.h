#ifndef ENGINECONFIG_H_
#define ENGINECONFIG_H_

#include <SFML/Graphics.hpp> //TODO: only include the vectors header
#include <string>

class EngineConfig
{
public:
  EngineConfig(std::string path = "settings.cfg");

  std::string configPath;
  bool debug;
  int scale;

  int width;
  int height;

  sf::Vector2f wSize;
  sf::FloatRect viewPort;

  std::string baseMapPath;
  std::string mapPath;

  float volume;
};

#endif /*ENGINECONFIG*/
