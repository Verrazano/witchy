#include "Controller.h"

bool Controller::captureInput = true;

Controller::Controller() {}

void Controller::addButton(std::string name, std::function<bool(void)> button)
{
  names.push_back(name);
  buttons.push_back(button);
  newPresses.push_back(false);
  oldPresses.push_back(false);
}

void Controller::update()
{
  for (int i = 0; i < buttons.size(); i++)
  {
    oldPresses[i] = newPresses[i];
    if (captureInput)
    {
      newPresses[i] = buttons[i]();
    }
    else
    {
      newPresses[i] = false;

    }
  }

  for(int i = 0; i < fakeInputs.size(); i++)
  {
    newPresses[fakeInputs[i]] = true;

  }

  fakeInputs.clear();

}

int Controller::getButtonIndex(std::string name)
{
  for (int i = 0; i < names.size(); i++)
  {
    if (names[i] == name)
    {
      return i;
    }
  }

  return -1;
}

bool Controller::pressing(std::string name)
{
  int index = getButtonIndex(name);
  return (index != -1 && newPresses[index]);
}

bool Controller::justPressed(std::string name)
{
  int index = getButtonIndex(name);

  bool i = index != -1 && newPresses[index] && !oldPresses[index];
  return i;
}

bool Controller::justReleased(std::string name)
{
  int index = getButtonIndex(name);

  bool i = index != -1 && !newPresses[index] && oldPresses[index];
  return i;
}

void Controller::press(std::string name)
{
  int index = getButtonIndex(name);
  fakeInputs.push_back(index);

}
