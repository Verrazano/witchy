#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <SFML/Graphics.hpp>
#include <functional>

class Controller
{
public:
  Controller();

  void addButton(std::string name, std::function<bool(void)> button);

  void update();

  int getButtonIndex(std::string name);

  bool pressing(std::string name);
  bool justPressed(std::string name);
  bool justReleased(std::string name);

  void press(std::string name);

  std::vector<int> fakeInputs;

  std::vector<std::string> names;
  std::vector<std::function<bool(void)>> buttons;
  std::vector<bool> newPresses;
  std::vector<bool> oldPresses;

  static bool captureInput;
};

#endif /*CONTROLLER*/
