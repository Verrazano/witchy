#include "SoundEffect.h"
#include "Util/Maths.h"
#include <cstddef>
#include <iostream>

float SoundEffect::globalVolume = 100.0f;
sf::Vector2f SoundEffect::listener;
std::map<std::string, sf::SoundBuffer*> SoundEffect::buffers;

SoundEffect::SoundEffect() {}

SoundEffect::SoundEffect(std::string path)
{
  baseVolume = 1.0f;

  auto it = buffers.find(path);
  if (it != buffers.end())
  {
    auto buffer = it->second;
    if (buffer)
    {
      setBuffer(*buffer);
    }
  }
  else
  {
    sf::SoundBuffer* buffer = new sf::SoundBuffer();
    if (buffer->loadFromFile(path))
    {
      buffers.emplace(path, buffer);
      setBuffer(*buffer);
      std::cout << "loaded audio: " << path << "\n";
    }
    else
    {
      delete buffer;
      buffers.emplace(path, nullptr);
    }
  }
}

void SoundEffect::play(float minDist, sf::Vector2f pos)
{
  float d = dist(pos, listener);
  float factor = fmax(fmin(1 - (d / minDist), 1.0f), 0.0f);
  float v = globalVolume * factor * baseVolume;
  if (v > 0.001f)
  {
    setVolume(v);
    Sound::play();
  }
}

void SoundEffect::play()
{
  float v = globalVolume * baseVolume;
  if (v > 0.001f)
  {
    setVolume(v);
    Sound::play();
  }
}

void SoundEffect::cleanup()
{
  for (const auto& pair : buffers)
  {
    delete pair.second;
  }

  buffers.clear();
}
