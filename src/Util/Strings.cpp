#include "Strings.h"
#include <sstream>

std::string toString(int i)
{
  std::stringstream ss;
  ss << i;
  std::string str;
  ss >> str;
  return str;
}

std::string toString(float f)
{
  std::stringstream ss;
  ss << f;
  std::string str;
  ss >> str;
  return str;
}

std::string toString(sf::Vector2f k)
{
  std::string f = toString(k.x) + "," + toString(k.y);
  return f;
}

float toFloat(std::string str)
{
  std::stringstream ss;
  ss << str;
  float f;
  ss >> f;
  return f;
}

int toInt(std::string str)
{
  std::stringstream ss;
  ss << str;
  int i;
  ss >> i;
  return i;
}

sf::Vector2f toVec2f(std::string str)
{
  std::string str2 = str.substr(0, str.find(","));
  str = str.substr(str.find(",") + 1);

  sf::Vector2f k;
  k.x = toFloat(str2);
  k.y = toFloat(str);

  return k;
}

std::string trim(std::string str)
{
  int first = str.find_first_not_of(' ');
  if (first == std::string::npos)
    return "";
  int last = str.find_last_not_of(' ');
  return str.substr(first, (last - first + 1));
}
