#ifndef MATHS_H_
#define MATHS_H_

// TODO: change this to only include what's necessary, same for similar usages
#include <SFML/Graphics.hpp>
#include <math.h>

typedef unsigned long ulong;

extern float pi;

float dist(sf::Vector2f a, sf::Vector2f b);

float mag(sf::Vector2f a);

sf::Vector2f unitVec(sf::Vector2f a);

// takes radians
sf::Vector2f unitVec(float r);

float chop(float f, int x);

// period 2^96-1
ulong qrand();

float qrandIn(float min, float max);
float randIn(float min, float max);

float angleTo(sf::Vector2f from, sf::Vector2f to);

// returns degrees, use only with graphical coords
float angleOf(sf::Vector2f vec);

float toDegrees(float r);
float toRadians(float d);

bool aprox(float a, float b, float tol = 0.01f);

template <typename T>
T chooseFrom(const std::vector<T> v)
{
  return v[qrand()%v.size()];

}

#endif /*MATHS*/
