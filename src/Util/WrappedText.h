#include <SFML/Graphics.hpp>

class WrappedText : public sf::Drawable
{
public:
  WrappedText();

  void setString(std::string str);

  void setFont(sf::Font& font);

  void setBounds(sf::FloatRect bounds);

  void setPosition(sf::Vector2f pos);

  void setFillColor(sf::Color color);

  void setCharacterSize(int size);

  void setPadding(int padding);

  sf::String getString();

  void splitBySpace(bool split);

  void allowResize(bool resize);

  void drawBounds(bool debug);

  void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
  void update();

  void update(int adhocSize);

  sf::String m_str;
  sf::Font* m_font;
  sf::FloatRect m_bounds;
  sf::Color m_color;
  int m_size;
  int m_padding;
  bool m_split;
  bool m_resize;
  bool m_debug;

  std::vector<sf::Text> m_texts;
};