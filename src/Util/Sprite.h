#ifndef SPRITE_H_
#define SPRITE_H_

#include "Anim.h"
#include <SFML/Graphics.hpp>

class Sprite
{
public:
  Sprite();

  Sprite(sf::Vector2f size, sf::Vector2u frameSize, sf::Texture* tex = NULL);

  void setTexture(sf::Texture* tex);

  void addAnim(Anim a);

  void addAnim(std::string title, std::vector<int> frames, float speed = 0.2f, bool looping = true);

  Anim& setAnim(std::string name);

  Anim& getAnim();

  bool hasAnim(std::string name);

  void update();

  void setSize(sf::Vector2f size);

  void setOffset(sf::Vector2u off);

  void setOrigin(sf::Vector2f orig);

  void setOrigin(float x, float y);

  sf::Vector2f getOrigin();

  void draw(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

  void setFacingLeft(bool left);

  void resetTextureRect();

  void setPosition(sf::Vector2f pos);

  sf::Vector2f getPosition();

  void pickRandomFrame();

  void setFrameSize(sf::Vector2u frameSize);

  sf::Texture* tex;
  sf::RectangleShape sprite;
  sf::Vector2u frameSize;
  sf::Vector2u offset;
  std::vector<Anim> anims;
  Anim current;
  bool facingLeft;
};

#endif /*SPRITE*/
