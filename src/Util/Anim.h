#ifndef ANIM_H_
#define ANIM_H_

#include "Sprites.h"

class Anim
{
public:
  Anim(std::string name, float speed = 0.2f, bool looping = true);

  Anim();

  std::string getName();

  float getSpeed();
  void setSpeed(float speed);

  bool isLooping();
  void setLooping(bool looping);

  // returns true if the frame changed
  bool update();

  int getFrame();
  int getIndex();
  void setIndex(int i);
  void reset();

  void addFrame(int frame);
  void setFrames(std::vector<int> frames);

  bool isPlaying();
  void setPlaying(bool playing);

  void pickRandomFrame();

  void play();

  void setOffset(sf::Vector2u offset);
  sf::Vector2u getOffset();

private:
  std::string name;
  sf::Clock timer;
  float speed;
  std::vector<int> frames;
  int index;
  bool looping;
  bool playing;
  sf::Vector2u offset;
};

#endif /*ANIM*/