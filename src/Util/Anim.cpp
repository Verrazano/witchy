#include "Anim.h"
#include "Maths.h"

Anim::Anim(std::string name, float speed, bool looping) : name(name), speed(speed), looping(looping)
{
  index = 0;
  playing = false;
}

Anim::Anim() : name("unassigned"), speed(0.2f), looping(true)
{
  index = 0;
  playing = false;
}

std::string Anim::getName() { return name; }

float Anim::getSpeed() { return speed; }

void Anim::setSpeed(float speed) { this->speed = speed; }

bool Anim::isLooping() { return looping; }

void Anim::setLooping(bool looping) { this->looping = looping; }

bool Anim::update()
{
  if (playing && timer.getElapsedTime().asSeconds() > speed)
  {
    index++;
    if (!looping && index == frames.size())
    {
      index--;
      playing = false;
    }

    index %= frames.size();

    timer.restart();

    return true;
  }

  return false;
}

int Anim::getFrame()
{
  if (index >= frames.size())
  {
    return 0;
  }

  return frames[index];
}

int Anim::getIndex() { return index; }

void Anim::setIndex(int i)
{
  index = i % frames.size();
  timer.restart();
}

void Anim::reset()
{
  index = 0;
  timer.restart();
}

void Anim::addFrame(int frame) { frames.push_back(frame); }

void Anim::setFrames(std::vector<int> frames) { this->frames = frames; }

bool Anim::isPlaying() { return playing; }

void Anim::setPlaying(bool playing)
{
  this->playing = playing;
  timer.restart();
}

void Anim::play()
{
  playing = true;
  reset();
}

void Anim::pickRandomFrame() { index = qrand() % frames.size(); }

void Anim::setOffset(sf::Vector2u offset) { this->offset = offset; }

sf::Vector2u Anim::getOffset() { return offset; }