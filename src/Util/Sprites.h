#ifndef SPRITES_H_
#define SPRITES_H_

#include <SFML/Graphics.hpp>

sf::IntRect getFrame(unsigned int frame, sf::Vector2u frameSize, sf::Vector2u size,
                     sf::Vector2u offset = sf::Vector2u(0, 0));

sf::Color getPixel(sf::Image& img, int x, int y);

sf::Color hexToColor(unsigned int hex);

unsigned int colorToHex(sf::Color color);

#endif /*SPRITES*/
