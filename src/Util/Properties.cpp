#include "Properties.h"
#include <utility>

Properties::Properties() {}

bool& Properties::setBool(const std::string& key, const bool& value)
{
  bool& v = bools[key];
  v = value;
  return v;
}

bool& Properties::getBool(const std::string& key, const bool& defaultValue)
{
  auto it = bools.find(key);
  if (it != bools.end())
  {
    return it->second;
  }

  return setBool(key, defaultValue);
}

bool Properties::existsBool(const std::string& key) { return bools.find(key) != bools.end(); }

int32_t& Properties::setInt(const std::string& key, const int32_t& value)
{
  int32_t& v = ints[key];
  v = value;
  return v;
}

int32_t& Properties::getInt(const std::string& key, const int32_t& defaultValue)
{
  auto it = ints.find(key);
  if (it != ints.end())
  {
    return it->second;
  }

  return setInt(key, defaultValue);
}

bool Properties::existsInt(const std::string& key) { return ints.find(key) != ints.end(); }

float& Properties::setFloat(const std::string& key, const float& value)
{
  float& v = floats[key];
  v = value;
  return v;
}

float& Properties::getFloat(const std::string& key, const float& defaultValue)
{
  auto it = floats.find(key);
  if (it != floats.end())
  {
    return it->second;
  }

  return setFloat(key, defaultValue);
}

bool Properties::existsFloat(const std::string& key) { return floats.find(key) != floats.end(); }

std::string& Properties::setString(const std::string& key, const std::string& value)
{
  std::string& v = strings[key];
  v = value;
  return v;
}

std::string& Properties::getString(const std::string& key, const std::string& defaultValue)
{
  auto it = strings.find(key);
  if (it != strings.end())
  {
    return it->second;
  }

  return setString(key, defaultValue);
}

bool Properties::existsString(const std::string& key) { return strings.find(key) != strings.end(); }
