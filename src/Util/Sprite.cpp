#include "Sprite.h"
#include "Sprites.h"
#include <iostream>

Sprite::Sprite() : tex(NULL) {}

Sprite::Sprite(sf::Vector2f size, sf::Vector2u frameSize, sf::Texture* tex) : frameSize(frameSize)
{
  // std::cout << "size: x: " << size.x << " y: " << size.y << "\n";
  setSize(size);

  // sprite.setFillColor(sf::Color::White);
  // sprite.setOutlineColor(sf::Color::Blue);
  // sprite.setOutlineThickness(1);

  setTexture(tex);
  facingLeft = true;
}

void Sprite::setTexture(sf::Texture* tex)
{
  this->tex = tex;
  sprite.setTexture(tex);
  resetTextureRect();
}

void Sprite::addAnim(Anim a)
{
  anims.push_back(a);
  if (anims.size() == 1)
  {
    setAnim(a.getName());
  }
}

void Sprite::addAnim(std::string title, std::vector<int> frames, float speed, bool looping)
{
  Anim a(title, speed, looping);
  a.setFrames(frames);
  addAnim(a);
}

Anim& Sprite::setAnim(std::string name)
{
  if (current.getName() != "unassigned")
  {
    for (int i = 0; i < anims.size(); i++)
    {
      if (current.getName() == anims[i].getName())
      {
        anims[i] = current;
        break;
      }
    }
  }

  for (int i = 0; i < anims.size(); i++)
  {
    if (name == anims[i].getName())
    {
      current = anims[i];
    }
  }

  resetTextureRect();
  return current;
}

Anim& Sprite::getAnim() { return current; }

bool Sprite::hasAnim(std::string name)
{
  for (int i = 0; i < anims.size(); i++)
  {
    if (name == anims[i].getName())
    {
      return true;
    }
  }

  return false;
}

void Sprite::update()
{
  if (tex && current.update())
  {
    sprite.setTextureRect(getFrame(current.getFrame(), frameSize, tex->getSize(), offset + current.getOffset()));
  }
}

void Sprite::setSize(sf::Vector2f size)
{
  sprite.setSize(size);
  sprite.setOrigin(size / 2.0f);
}

void Sprite::setOffset(sf::Vector2u off)
{
  offset = off;
  resetTextureRect();
}

void Sprite::setOrigin(sf::Vector2f orig) { sprite.setOrigin(orig); }

void Sprite::setOrigin(float x, float y) { setOrigin(sf::Vector2f(x, y)); }

sf::Vector2f Sprite::getOrigin() { return sprite.getOrigin(); }

void Sprite::draw(sf::RenderWindow& window, sf::RenderStates states) { window.draw(sprite, states); }

void Sprite::setFacingLeft(bool left)
{
  if (left != facingLeft)
  {
    facingLeft = left;
    sprite.scale(-1, 1);
  }
}

void Sprite::resetTextureRect()
{
  if (tex)
  {
    sprite.setFillColor(sf::Color::White);
    sf::IntRect frame = getFrame(current.getFrame(), frameSize, tex->getSize(), offset);
    sprite.setTextureRect(frame);
  }
  else
  {
    sprite.setFillColor(sf::Color::Transparent);
  }
}

void Sprite::setPosition(sf::Vector2f pos) { sprite.setPosition(pos); }

sf::Vector2f Sprite::getPosition() { return sprite.getPosition(); }

void Sprite::pickRandomFrame()
{
  current.pickRandomFrame();
  resetTextureRect();
}

void Sprite::setFrameSize(sf::Vector2u frameSize)
{
  this->frameSize = frameSize;
  resetTextureRect();
}
