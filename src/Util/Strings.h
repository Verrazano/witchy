#ifndef STRINGS_H_
#define STRINGS_H_

#include <SFML/Graphics.hpp> //TODO: only include the vectors file
#include <iostream>
#include <string>

std::string toString(int i);

std::string toString(float f);

std::string toString(sf::Vector2f k);

float toFloat(std::string str);

int toInt(std::string str);

sf::Vector2f toVec2f(std::string str);

std::string trim(std::string str);

#endif /*STRINGS*/