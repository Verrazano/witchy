#ifndef PROPERTIES_H_
#define PROPERTIES_H_

#include <map>
#include <stdint.h>
#include <string>

class Properties
{
public:
  Properties();

  bool& setBool(const std::string& key, const bool& value);
  bool& getBool(const std::string& key, const bool& defaultValue = false);
  bool existsBool(const std::string& key);

  int32_t& setInt(const std::string& key, const int32_t& value);
  int32_t& getInt(const std::string& key, const int32_t& defaultValue = 0);
  bool existsInt(const std::string& key);

  float& setFloat(const std::string& key, const float& value);
  float& getFloat(const std::string& key, const float& defaultValue = 0.0f);
  bool existsFloat(const std::string& key);

  std::string& setString(const std::string& key, const std::string& value);
  std::string& getString(const std::string& key, const std::string& defaultValue = "");
  bool existsString(const std::string& key);

private:
  std::map<std::string, bool> bools;
  std::map<std::string, int32_t> ints;
  std::map<std::string, float> floats;
  std::map<std::string, std::string> strings;
};

#endif /*PROPERTIES*/
