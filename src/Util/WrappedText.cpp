#include "WrappedText.h"

WrappedText::WrappedText()
    : m_font(NULL), m_color(sf::Color::Black), m_size(14), m_padding(4), m_split(true), m_debug(false)
{
}

void WrappedText::setString(std::string str)
{
  m_str = str;
  update();
}

void WrappedText::setFont(sf::Font& font)
{
  m_font = &font;
  update();
}

void WrappedText::setBounds(sf::FloatRect bounds)
{
  m_bounds = bounds;
  update();
}

void WrappedText::setPosition(sf::Vector2f pos)
{
  m_bounds.left = pos.x;
  m_bounds.top = pos.y;
  update();
}

void WrappedText::setFillColor(sf::Color color)
{
  m_color = color;
  for (unsigned int i = 0; i < m_texts.size(); i++)
  {
    m_texts[i].setFillColor(m_color);
  }
}

void WrappedText::setCharacterSize(int size)
{
  m_size = size;
  update();
}

void WrappedText::setPadding(int padding)
{
  m_padding = padding;
  update();
}

sf::String WrappedText::getString() { return m_str; }

void WrappedText::splitBySpace(bool split) { m_split = split; }

void WrappedText::allowResize(bool resize)
{
  m_resize = resize;
  update();
}

void WrappedText::drawBounds(bool debug) { m_debug = debug; }

void WrappedText::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  for (unsigned int i = 0; i < m_texts.size(); i++)
  {
    target.draw(m_texts[i]);

    if (m_debug)
    {
      sf::FloatRect bounds = m_texts[i].getGlobalBounds();
      sf::RectangleShape r(sf::Vector2f(bounds.width, bounds.height));
      r.setPosition(bounds.left, bounds.top);
      r.setFillColor(sf::Color::Transparent);
      r.setOutlineColor(sf::Color::Red);
      r.setOutlineThickness(1);
      target.draw(r);
    }
  }

  if (m_debug)
  {
    sf::RectangleShape b(sf::Vector2f(m_bounds.width, m_bounds.height));
    b.setPosition(m_bounds.left, m_bounds.top);
    b.setFillColor(sf::Color::Transparent);
    b.setOutlineColor(sf::Color::Green);
    b.setOutlineThickness(1);
    target.draw(b);
  }
}

void WrappedText::update() { update(m_size); }

void WrappedText::update(int adhocSize)
{
  m_texts.clear();

  sf::Text t;
  if (m_font != NULL)
  {
    t.setFont(*m_font);
  }

  std::string full = m_str;

  t.setFillColor(m_color);
  t.setCharacterSize(adhocSize);
  t.setString(full);

  sf::Vector2f pos(m_bounds.left + m_padding, m_bounds.top + m_padding - t.getLocalBounds().height);
  t.setPosition(pos);

  sf::Vector2f maxSize(m_bounds.left + m_bounds.width, m_bounds.top + m_bounds.height);

  bool toBig = false;

  for (int i = 1; i < full.size();)
  {
    sf::Vector2f tPos = t.findCharacterPos(i);
    sf::FloatRect tBounds = t.getGlobalBounds();

    if (tBounds.top + tBounds.height + m_padding >= maxSize.y)
    {
      toBig = true;
    }

    if (tPos.x + (adhocSize + 1) / 2 + m_padding >= maxSize.x)
    {
      if (m_split)
      {
        int index = full.rfind(" ", i);
        if (index != std::string::npos)
        {
          i = index;
        }
      }

      std::string part = full.substr(0, i);
      t.setString(part);
      pos.y += tBounds.height + m_padding;

      if (m_split)
      {
        full = full.substr(i + 1);
      }
      else
      {
        full = full.substr(i);
      }

      m_texts.push_back(t);

      t.setString(full);
      t.setPosition(pos);

      i = 0;
      continue;
    }

    i++;
  }

  m_texts.push_back(t);

  if (m_resize && toBig && adhocSize > 10)
  {
    update(adhocSize - 1);
  }
}