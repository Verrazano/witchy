#include "Sprites.h"

sf::Color hexToColor(unsigned int hex)
{
  sf::Color color;
  color.r = ((hex >> 16) & 0xff);
  color.g = ((hex >> 8) & 0xff);
  color.b = ((hex)&0xff);
  return color;
}

unsigned int colorToHex(sf::Color color)
{
  unsigned int hex = 0;
  hex |= (color.r << 16);
  hex |= (color.g << 8);
  hex |= color.b;
  return hex;
}

sf::IntRect getFrame(unsigned int frame, sf::Vector2u frameSize, sf::Vector2u size, sf::Vector2u offset)
{
  sf::IntRect rect(0, 0, frameSize.x, frameSize.y);

  if (frameSize.x == 0 || frameSize.y == 0 || size.x == 0 || size.y == 0)
  {
    return rect;
  }

  unsigned fx = size.x / frameSize.x;
  unsigned fy = size.y / frameSize.y;

  unsigned int count = fx * fy;

  if (frame > count)
  {
    return rect;
  }

  unsigned int x = frame % fx;
  unsigned int y = frame / fx;

  x *= frameSize.x;
  y *= frameSize.y;

  rect.left = x + offset.x;
  rect.top = y + offset.y;

  return rect;
}

sf::Color getPixel(sf::Image& img, int x, int y)
{
  sf::Vector2u size = img.getSize();
  if (x < 0 || x >= size.x || y < 0 || y >= size.y)
  {
    return sf::Color::White;
  }

  return img.getPixel(x, y);
}
