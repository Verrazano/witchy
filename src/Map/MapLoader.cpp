#include "MapLoader.h"
#include "../Entities/Entities.h"
#include "../Game.h"
#include "../Util/Strings.h"
#include "MapFactory.h"

void registerTiles(Map& map)
{
  map.registerWallTile(6);  // tree
  map.registerWallTile(11); // well
  map.registerWallTile(13); // roof front
  map.registerWallTile(14); // house window
  map.registerWallTile(16); // house wall
  map.registerWallTile(27); // wall top
  map.registerWallTile(29); // bed top blue
  map.registerWallTile(30); // bed bottom blue
  map.registerWallTile(31); // chest
  map.registerWallTile(41); // bed top purple
  map.registerWallTile(42); // bed bottom purple
  map.registerWallTile(43); // table with potions
  map.registerWallTile(45); // dresser
  map.registerWallTile(46); // blue bed horizontal left
  map.registerWallTile(47); // blue bed horizontal right
  map.registerWallTile(51); // slime statue

  map.registerWallTile(69); // dungeon wall top decorative 1
  map.registerWallTile(70); // dungeon wall top decorative 2
  map.registerWallTile(72); // dungeon wall top

  map.registerWallTile(93); // dirt wall

  map.registerWallTile(96);
  map.registerWallTile(98);

  map.registerTile(7, sf::Vector2f(8, 8));                                           // stump
  map.registerTile(8, sf::Vector2f(8, 8));                                           // fence
  map.registerTile(12, sf::Vector2f(16, 10), sf::Vector2f(8, 2));                    // roof back
  map.registerTile(20, sf::Vector2f(6, 16), sf::Vector2f(8, 8));                     // chimmney mid
  map.registerTile(21, sf::Vector2f(6, 8), sf::Vector2f(8, 8));                      // chimmney bottom
  map.registerTile(22, sf::Vector2f(16, 10), sf::Vector2f(8, 2), Tile::Flag::Ledge); // grass ledge
  map.registerTile(36, sf::Vector2f(16, 10), sf::Vector2f(8, 2), Tile::Flag::Death); // pit edge
  map.registerTile(37, sf::Vector2f(16, 16), Tile::Flag::Death);                     // pit
  map.registerTile(50, sf::Vector2f(16, 6), sf::Vector2f(8, 8));                     // statue bottom

  map.registerTile(57, sf::Vector2f(12, 16), sf::Vector2f(8, 8)); // dungeon wall left
  map.registerTile(58, sf::Vector2f(12, 16), sf::Vector2f(4, 8)); // dungeon wall right
  map.registerTile(56, sf::Vector2f(12, 16), sf::Vector2f(8, 8)); // dungeon pillar left
  map.registerTile(59, sf::Vector2f(12, 16), sf::Vector2f(4, 8)); // dungeon pillar right
  map.registerTile(60, sf::Vector2f(12, 14), sf::Vector2f(8, 8)); // dungeon top left corner
  map.registerTile(61, sf::Vector2f(16, 14), sf::Vector2f(8, 8)); // dungeon wall decorative 1
  map.registerTile(62, sf::Vector2f(16, 14), sf::Vector2f(8, 8)); // dungeon wall decorative 2
  map.registerTile(63, sf::Vector2f(16, 16), sf::Vector2f(8, 8)); // dungeon pillar
  map.registerTile(64, sf::Vector2f(16, 14), sf::Vector2f(8, 8)); // dungeon wall
  map.registerTile(65, sf::Vector2f(12, 14), sf::Vector2f(4, 8)); // dungeon top right corner
  map.registerTile(66, sf::Vector2f(12, 14), sf::Vector2f(8, 8)); // dungeon corner bottom right
  map.registerTile(67, sf::Vector2f(12, 14), sf::Vector2f(4, 8)); // dungeon corner bottom left
  map.registerTile(73, sf::Vector2f(12, 16), sf::Vector2f(4, 8)); // dungeon wall right decorative

  map.registerMetaTile(23, 2, Tile::Flag::Wood);  // wood floor
  map.registerMetaTile(38, 2, Tile::Flag::Stone); // mossy stone
  map.registerBoundedMetaTile(54, 2, sf::Vector2f(16, 16), sf::Vector2f(8, 8),
                              Tile::Flag::Solid); // stone wall on anything

  map.registerBoundedMetaTile(74, 2, sf::Vector2f(16, 16), sf::Vector2f(8, 8), Tile::Flag::Solid); // stream tiles
  map.registerMetaTile(76, 1, Tile::Flag::Stone);

  // cheating tiles for just sound set count to 1 to represent itself
  map.registerMetaTile(1, 1, Tile::Flag::Leaves); // grass
  map.registerMetaTile(2, 1, Tile::Flag::Leaves);
  map.registerMetaTile(17, 1, Tile::Flag::Leaves);

  map.registerMetaTile(3, 1, Tile::Flag::Gravel); // sand
  map.registerMetaTile(4, 1, Tile::Flag::Gravel);
  map.registerMetaTile(5, 1, Tile::Flag::Gravel); // grass/sand path
  map.registerMetaTile(9, 1, Tile::Flag::Gravel); // dungeon sand stone (maybe make this stone sound)
  map.registerMetaTile(10, 1, Tile::Flag::Gravel);
  map.registerMetaTile(18, 1, Tile::Flag::Gravel);

  map.registerMetaTile(28, 1, Tile::Flag::Wood); // wood exit
  map.registerMetaTile(31, 1, Tile::Flag::Wood); // carpet (maybe find a different sound)
  map.registerMetaTile(32, 1, Tile::Flag::Wood);
  map.registerMetaTile(33, 1, Tile::Flag::Wood); // books
  map.registerMetaTile(34, 1, Tile::Flag::Wood);
  map.registerMetaTile(44, 1, Tile::Flag::Wood); // stray potion
  map.registerMetaTile(48, 1, Tile::Flag::Wood); // red carpet
  map.registerMetaTile(49, 1, Tile::Flag::Wood);

  map.registerMetaTile(40, 1, Tile::Flag::Stone); // grass/stone path

  map.registerBoundedMetaTile(81, 1, sf::Vector2f(32, 4), sf::Vector2f(8, 12), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(82, 1, sf::Vector2f(32, 4), sf::Vector2f(16, 12), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(83, 1, sf::Vector2f(4, 32), sf::Vector2f(-8, 8), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(84, 1, sf::Vector2f(4, 32), sf::Vector2f(12, 24), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(85, 1, sf::Vector2f(32, 4), sf::Vector2f(16, -8), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(86, 1, sf::Vector2f(32, 4), sf::Vector2f(24, -8), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(87, 1, sf::Vector2f(4, 32), sf::Vector2f(12, 16), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerBoundedMetaTile(88, 1, sf::Vector2f(4, 32), sf::Vector2f(-8, 16), (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));
  map.registerMetaTile(89, 1, (Tile::Flag)((int)Tile::Flag::Stone | (int)Tile::Flag::Puzzle));

  // map.registerBoundedMetaTile(25, 2, sf::Vector2f(16, 12), sf::Vector2f(8,
  // 8), Tile::Flag::Solid); //stone wall on
  // grass
}

void registerEntities(Map& map)
{
  map.registerEntity("player", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string abilities;
    f >> abilities;
    bool hasAbilities = abilities == "yes";
    return new Player(game, pos, hasAbilities);

  });

  map.registerEntity("gate", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Gate(game, pos);

  });

  map.registerEntity("staff", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Staff(game, pos);
  });

  map.registerEntity("zone", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    int x = 1;
    int y = 1;
    f >> x >> y;
    std::string enterScript;
    std::string leaveScript;
    std::string scriptData;
    f >> enterScript >> leaveScript;
    std::getline(f, scriptData);

    sf::Vector2f a(x, y);
    return new Zone(game, pos, a, enterScript, leaveScript, scriptData);

  });

  map.registerEntity("dahlia", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Flower(game, pos, "dahlia");

  });

  map.registerEntity("nightshade", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Flower(game, pos, "nightshade");

  });

  map.registerEntity("daisy", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Flower(game, pos, "daisy");

  });

  map.registerEntity("bluebell", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Flower(game, pos, "bluebell");

  });

  map.registerEntity("slime", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Slime(game, pos);

  });

  map.registerEntity("slime_cube", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new SlimeCube(game, pos);

  });

  map.registerEntity("gnome", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Gnome(game, pos);

  });

  map.registerEntity("honeycomb", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new HoneyComb(game, pos);

  });

  map.registerEntity("hive", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Hive(game, pos);

  });

  map.registerEntity("rock", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Rock(game, pos);

  });

  map.registerEntity("boulder", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Boulder(game, pos);

  });

  map.registerEntity("bush", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Bush(game, pos);
  });

  map.registerEntity("furrow", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Furrow(game, pos);
  });


  /*map.registerEntity("king_slime", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new KingSlime(game, pos);

  });*/

  map.registerEntity("frog", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Frog(game, pos);

  });

  map.registerEntity("puzzle_block", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new PuzzleBlock(game, pos);

  });

  map.registerEntity("marker", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string tags;
    std::getline(f, tags);
    tags = trim(tags);
    return new Marker(game, pos, tags);

  });

  map.registerEntity("map_teleport", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string map;
    float x = 0;
    float y = 0;
    std::string tag;
    f >> map >> x >> y >> tag;

    map = game->config.baseMapPath + map;

    game->mapsToLoad.push_back(map);

    sf::Vector2f k(x * 16 + 8, y * 16 + 8);

    return new MapTeleport(game, pos, map, k, tag);

  });

  map.registerEntity("turtle", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Turtle(game, pos);

  });

  map.registerEntity("crystal", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Crystal(game, pos);

  });

  map.registerEntity("small_chest", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string item;
    f >> item;
    return new Chest(game, pos, false, item);

  });

  map.registerEntity("big_chest", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string item;
    f >> item;
    return new Chest(game, pos, true, item);

  });

  map.registerEntity("locked_door", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new LockedDoor(game, pos);

  });

  map.registerEntity("red_shard", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Shard(game, pos, "red");

  });

  map.registerEntity("yellow_shard", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Shard(game, pos, "yellow");

  });

  map.registerEntity("blue_shard", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Shard(game, pos, "blue");

  });

  map.registerEntity("green_shard", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Shard(game, pos, "green");

  });

  map.registerEntity("purple_shard", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Shard(game, pos, "purple");

  });

  map.registerEntity("npc", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string name;
    f >> name;
    return new NPC(game, pos, name);

  });

  map.registerEntity("sign", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    std::string text;
    std::getline(f, text);
    return new Sign(game, pos, text);

  });

  map.registerEntity("statue", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    int index = 0;
    f >> index;
    std::string text;
    std::getline(f, text);
    return new Statue(game, pos, text, index);

  });

  map.registerEntity("fence", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Fence(game, pos);

  });

  map.registerEntity("pot", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Pot(game, pos);

  });

  map.registerEntity("bones", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Bones(game, pos);

  });

  map.registerEntity("torch", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Torch(game, pos);

  });

  map.registerEntity("pressure_plate", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new PressurePlate(game, pos);

  });

  map.registerEntity("mushroom", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Mushroom(game, pos);

  });

  map.registerEntity("beetle", [](Game* game, sf::Vector2f& pos, std::fstream& f) {
    return new Beetle(game, pos);

  });
}
