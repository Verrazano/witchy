#include "Map.h"
#include "../Entity.h"
#include "../Game.h"
#include "../Util/Sprites.h"
#include <fstream>
#include <iostream>

uint Map::tileSize = 16;
std::string Map::texturePath = "res/world.png";

Map::Map() : game(NULL), width(0), height(0), worldTexture(NULL) {}

Map::Map(Game* game, std::string path) : game(game), path(path), width(0), height(0), worldTexture(NULL) {}

void Map::load()
{
  std::fstream f;
  f.open(path.c_str());

  if (f)
  {
    f >> width;
    f >> height;

    uint size = width * height;

    tiles.resize(size);

    verts = sf::VertexArray(sf::Quads, size * 4);

    worldTexture = game->textures.getTexture(texturePath);

    uint frameSize = 8;
    sf::Vector2u texSize = worldTexture->getSize();

    // load tiles
    for (uint i = 0; i < size; i++)
    {
      uint frameIndex = 0;
      f >> frameIndex;

      sf::Vector2u coord = getCoords(i);
      sf::Vector2f pos = toWorldSpace(coord);

      MapFactory& factory = tileFactory[frameIndex];
      Tile tile = factory.makeTile(pos, i);

      sf::Vertex* quad = &verts[i * 4];

      if ((factory.type != MapFactory::Type::MetaTile && factory.type != MapFactory::Type::BoundedMeta) &&
          frameIndex != tile.frame)
      {
        tile.frame = frameIndex;
      }

      sf::IntRect frame = getFrame(tile.frame, sf::Vector2u(frameSize, frameSize), texSize);

      uint x = coord.x;
      uint y = coord.y;

      quad[0] = sf::Vertex(sf::Vector2f(x * tileSize, y * tileSize), sf::Vector2f(frame.left, frame.top));

      quad[1] =
          sf::Vertex(sf::Vector2f((x + 1) * tileSize, y * tileSize), sf::Vector2f(frame.left + frameSize, frame.top));

      quad[2] = sf::Vertex(sf::Vector2f((x + 1) * tileSize, (y + 1) * tileSize),
                           sf::Vector2f(frame.left + frameSize, frame.top + frameSize));

      quad[3] = sf::Vertex(sf::Vector2f(x * 16, (y + 1) * tileSize), sf::Vector2f(frame.left, frame.top + frameSize));

      tile.quad = quad;
      tiles[i] = tile;
    }

    entitiesAdded = false;

    bool isBase = game->config.baseMapPath + game->baseMap == path;

    /*if (isBase)
    {
      game->addEntity(new SlimeRespawn(game));
    }*/

    uint count = 0;
    f >> count;

    for (uint i = 0; i < count; i++)
    {
      uint x = 0;
      uint y = 0;
      std::string name;

      f >> x >> y >> name;

      sf::Vector2f pos = toWorldSpace(sf::Vector2u(x, y));

      Entity* entity = entityFactory[name].makeEntity(game, pos, f);
      if(!isBase && name == "player") {
        delete entity;
        continue;
      }
      entities.push_back(entity);
      // game->addEntity(entity);
    }
  }
}

void Map::registerTile(uint frame, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags)
{
  tileFactory[frame] = MapFactory(frame, size, origin, flags);
}

void Map::registerTile(uint frame, sf::Vector2f size, Tile::Flag flags)
{
  registerTile(frame, size, size / 2.0f, flags);
}

void Map::registerMetaTile(uint frame, uint count, Tile::Flag flags)
{
  for (uint i = 0; i < count; i++)
  {
    uint dup = frame + i;
    tileFactory[dup] = MapFactory(frame, count, flags);
  }
}

void Map::registerBoundedMetaTile(uint frame, uint count, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags)
{
  for (uint i = 0; i < count; i++)
  {
    uint dup = frame + i;
    tileFactory[dup] = MapFactory(frame, count, size, origin, flags);
  }
}

void Map::registerWallTile(uint frame, Tile::Flag flags)
{
  sf::Vector2f size(tileSize, tileSize);
  registerTile(frame, size, size / 2.0f, flags);
}

void Map::registerEntity(std::string name, MapFactory::EntityFactory factory)
{
  entityFactory[name] = MapFactory(name, factory);
}

sf::Vector2u Map::toTileSpace(sf::Vector2f pos)
{
  sf::Vector2u t;
  t.x = (uint)(pos.x) / tileSize;
  t.y = (uint)(pos.y) / tileSize;

  return t;
}

uint Map::getTileIndex(uint x, uint y)
{
  uint index = x + y * width;
  if (index >= width * height)
  {
    return 0;
  }

  return index;
}

uint Map::getTileIndex(sf::Vector2u t) { return getTileIndex(t.x, t.y); }

sf::Vector2u Map::getCoords(uint index)
{
  sf::Vector2u t;
  t.x = index % width;
  t.y = index / width;

  return t;
}

sf::Vector2f Map::toWorldSpace(sf::Vector2u t)
{
  sf::Vector2f w;
  float halfTile = tileSize / 2.0f;
  w.x = (float)(t.x * tileSize) + halfTile;
  w.y = (float)(t.y * tileSize) + halfTile;

  return w;
}

Tile& Map::getTile(uint index)
{
  if (index >= tiles.size())
  {
    return mock;
  }

  return tiles[index];
}

Tile& Map::getTile(sf::Vector2f world)
{
  return getTile(getTileIndex(toTileSpace(world)));

}


bool Map::isCollidingAt(sf::FloatRect rect, sf::Vector2f pos, Tile::Flag filter)
{
  uint index = getTileIndex(toTileSpace(pos));

  uint up = index - width;
  uint down = index + width;
  uint left = index - 1;
  uint right = index + 1;

  uint tl = up - 1;
  uint tr = up + 1;
  uint bl = down - 1;
  uint br = down + 1;

  bool mid = isCollidingWith(rect, index, filter);

  bool card = isCollidingWith(rect, up, filter) || isCollidingWith(rect, down, filter) ||
              isCollidingWith(rect, left, filter) || isCollidingWith(rect, right, filter);

  bool ord = isCollidingWith(rect, tl, filter) || isCollidingWith(rect, tr, filter) ||
             isCollidingWith(rect, bl, filter) || isCollidingWith(rect, br, filter);

  return mid || card || ord;
}

bool Map::isCollidingWith(sf::FloatRect rect, uint index, Tile::Flag filter)
{
  Tile& tile = getTile(index);
  return tile.flags & filter && tile.getBounds().intersects(rect);
}

void Map::resolveCollision(Entity* entity, Tile::Flag filter)
{
  sf::Vector2f pos = entity->getPosition();
  uint index = getTileIndex(toTileSpace(pos));

  uint up = index - width;
  uint down = index + width;
  uint left = index - 1;
  uint right = index + 1;

  uint tl = up - 1;
  uint tr = up + 1;
  uint bl = down - 1;
  uint br = down + 1;

  resolveCollisionWith(entity, index, filter);

  resolveCollisionWith(entity, up, filter);
  resolveCollisionWith(entity, down, filter);
  resolveCollisionWith(entity, left, filter);
  resolveCollisionWith(entity, right, filter);

  resolveCollisionWith(entity, tl, filter);
  resolveCollisionWith(entity, tr, filter);
  resolveCollisionWith(entity, br, filter);
  resolveCollisionWith(entity, bl, filter);
}

void Map::resolveCollisionWith(Entity* entity, uint index, Tile::Flag filter)
{
  sf::FloatRect rect = entity->getBounds();
  Tile& tile = getTile(index);

  if (tile.flags & filter)
  {
    sf::FloatRect intersect;
    if (tile.getBounds().intersects(rect, intersect))
    {
      sf::Vector2f pos = entity->bounds.getPosition();
      if (intersect.width > intersect.height)
      {
        if (pos.y > intersect.top)
        {
          entity->bounds.move(0, intersect.height);
        }
        else
        {
          entity->bounds.move(0, -intersect.height);
        }
      }
      else
      {
        if (pos.x > intersect.left)
        {
          entity->bounds.move(intersect.width, 0);
        }
        else
        {
          entity->bounds.move(-intersect.width, 0);
        }
      }

      entity->setPosition(entity->getPosition());
    }
  }
}

bool Map::isOnLedge(sf::FloatRect rect, sf::Vector2f pos)
{
  Tile::Flag flag = Tile::Flag::Ledge;
  uint index = getTileIndex(toTileSpace(pos));

  uint down = index + width;
  uint bl = down - 1;
  uint br = down + 1;

  bool inc = getTile(index).flags & flag && isCollidingWith(rect, index, flag);
  bool dc = getTile(down).flags & flag && isCollidingWith(rect, down, flag);
  //bool blc = getTile(bl).flags & flag && isCollidingWith(rect, bl, flag);
  //bool brc = getTile(br).flags & flag && isCollidingWith(rect, br, flag);

  return inc || dc;
}

void Map::draw(sf::RenderWindow& window, sf::RenderStates states)
{
  states.texture = worldTexture;

  window.draw(verts, states);

  if (game->config.debug)
  {
    for (Tile& tile : tiles)
    {
      window.draw(tile.bounds);
    }
  }
}
