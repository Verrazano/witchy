#ifndef MAPLOADER_H_
#define MAPLOADER_H_

#include "Map.h"

void registerTiles(Map& map);
void registerEntities(Map& map);

#endif /*MAPLOADER*/