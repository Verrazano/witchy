#include "Tile.h"

Tile::Tile() : frame(0), flags(Flag::None) {}

Tile::Tile(uint frame, Flag flags) : frame(frame), flags(flags) {}

Tile::Tile(uint frame, sf::RectangleShape bounds, Flag flags) : frame(frame), bounds(bounds), flags(flags)
{
  setDebugBorder();
}

sf::FloatRect Tile::getBounds() { return bounds.getGlobalBounds(); }

void Tile::setDebugBorder()
{
  sf::Vector2f size = bounds.getSize();
  sf::Vector2f origin = bounds.getOrigin();

  bounds.setSize(size - sf::Vector2f(0.5, 0.5));
  bounds.setOrigin(origin - sf::Vector2f(0.25f, 0.25f));

  bounds.setFillColor(sf::Color::Transparent);
  bounds.setOutlineThickness(0.25f);
  bounds.setOutlineColor(sf::Color::Red);
}