#ifndef MAP_H_
#define MAP_H_

#include "MapFactory.h"
#include "Tile.h"
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/System/Vector2.hpp>
#include <map>
#include <string>
#include <vector>

class Game;
class Entity;

class Map
{
public:
  Map();

  Map(Game* game, std::string path);

  void load();

  void registerTile(uint frame, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags = Tile::Flag::Solid);

  void registerTile(uint frame, sf::Vector2f size, Tile::Flag flags = Tile::Flag::Solid);

  void registerMetaTile(uint frame, uint count = 2, Tile::Flag flags = Tile::Flag::None);

  void registerBoundedMetaTile(uint frame, uint count, sf::Vector2f size, sf::Vector2f origin,
                               Tile::Flag flags = Tile::Flag::Solid);

  void registerWallTile(uint frame, Tile::Flag flags = Tile::Flag::Solid);

  void registerEntity(std::string name, MapFactory::EntityFactory factory);

  // Legacy code that should probably be rewritten but wont be right now
  sf::Vector2u toTileSpace(sf::Vector2f pos);

  uint getTileIndex(uint x, uint y);

  uint getTileIndex(sf::Vector2u t);

  sf::Vector2u getCoords(uint index);

  sf::Vector2f toWorldSpace(sf::Vector2u t);

  Tile& getTile(uint index);

  Tile& getTile(sf::Vector2f world);

  bool isCollidingAt(sf::FloatRect rect, sf::Vector2f pos, Tile::Flag filter = Tile::Flag::Solid);

  bool isCollidingWith(sf::FloatRect rect, uint index, Tile::Flag filter = Tile::Flag::Solid);

  void resolveCollision(Entity* entity, Tile::Flag filter = Tile::Flag::Solid);

  void resolveCollisionWith(Entity* entity, uint index, Tile::Flag filter = Tile::Flag::Solid);

  void draw(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

  bool isOnLedge(sf::FloatRect rect, sf::Vector2f pos);

  Game* game;

  std::string path;

  uint width;
  uint height;

  sf::Texture* worldTexture;

  Tile mock;
  std::vector<Tile> tiles;
  std::vector<Entity*> entities;
  bool entitiesAdded;
  sf::VertexArray verts;

  std::map<uint, MapFactory> tileFactory;
  std::map<std::string, MapFactory> entityFactory;

  static uint tileSize;
  static std::string texturePath;
};

#endif /*MAP*/
