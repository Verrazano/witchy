#ifndef MAPFACTORY_H_
#define MAPFACTORY_H_

#include "Tile.h"
#include <SFML/System/Vector2.hpp>
#include <fstream>
#include <functional>
#include <string>

class Game;
class Entity;

class MapFactory
{
public:
  enum class Type
  {
    BoundedTile,
    MetaTile,
    BoundedMeta,
    Entity,
    None

  };

  // TODO: change the fstream to the base stream type
  typedef std::function<Entity*(Game*, sf::Vector2f&, std::fstream&)> EntityFactory;

  MapFactory();

  // bounded tile
  MapFactory(uint frame, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags = Tile::Flag::None);

  // meta tile
  MapFactory(uint frame, uint count, Tile::Flag flags = Tile::Flag::None);

  MapFactory(uint frame, uint count, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags = Tile::Flag::Solid);

  // entity
  MapFactory(std::string name, EntityFactory factory);

  Tile makeTile(sf::Vector2f& pos, uint index);

  Entity* makeEntity(Game* game, sf::Vector2f& pos, std::fstream& f);

  Type type;

  // tiles
  uint frame;
  Tile::Flag flags;

  // bounded tile
  sf::Vector2f size;
  sf::Vector2f origin;

  // meta tile
  uint count;

  // entity
  std::string name;
  EntityFactory factory;
};

#endif /*MAPFACTORY*/