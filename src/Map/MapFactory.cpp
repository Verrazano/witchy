#include "MapFactory.h"

MapFactory::MapFactory() : type(Type::None), frame(0), flags(Tile::Flag::None), count(0) {}

MapFactory::MapFactory(uint frame, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags)
    : type(Type::BoundedTile), frame(frame), flags(flags), size(size), origin(origin), count(0)
{
}

MapFactory::MapFactory(uint frame, uint count, Tile::Flag flags)
    : type(Type::MetaTile), frame(frame), flags(flags), count(count)
{
}

MapFactory::MapFactory(uint frame, uint count, sf::Vector2f size, sf::Vector2f origin, Tile::Flag flags)
    : type(Type::BoundedMeta), frame(frame), flags(flags), size(size), origin(origin), count(count)
{
}

MapFactory::MapFactory(std::string name, EntityFactory factory)
    : type(Type::Entity), frame(0), flags(Tile::Flag::None), count(0), name(name), factory(factory)
{
}

Tile MapFactory::makeTile(sf::Vector2f& pos, uint index)
{
  if (type == Type::BoundedTile)
  {
    sf::RectangleShape bounds(size);
    bounds.setOrigin(origin);
    bounds.setPosition(pos);

    return Tile(frame, bounds, flags);
  }
  else if (type == Type::BoundedMeta)
  {
    sf::RectangleShape bounds(size);
    bounds.setOrigin(origin);
    bounds.setPosition(pos);

    return Tile(frame + index % count, bounds, flags);
  }

  if (count > 0)
  {
    return Tile(frame + index % count, flags);
  }

  return Tile(frame, flags);
}

Entity* MapFactory::makeEntity(Game* game, sf::Vector2f& pos, std::fstream& f)
{
  if (type == Type::Entity)
  {
    return factory(game, pos, f);
  }

  return NULL;
}
