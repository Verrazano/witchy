#ifndef TILE_H_
#define TILE_H_

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Vertex.hpp>

typedef unsigned int uint;

class Tile
{
public:
  // this can't be enum class because it needs |,& operators
  typedef enum Flag {
    None = 1 << 0,
    Solid = 1 << 1,
    Ledge = 1 << 2,
    Death = 1 << 3,

    // Flags for sound effects
    Gravel = 1 << 4,
    Leaves = 1 << 5,
    Stone = 1 << 6,
    Wood = 1 << 7,

    Puzzle = 1 << 8

  } Flag;

  Tile();

  Tile(uint frame, Flag flags = Flag::None);

  Tile(uint frame, sf::RectangleShape bounds, Flag flags = Flag::None);

  sf::FloatRect getBounds();

  void setDebugBorder();

  uint frame;
  sf::RectangleShape bounds;
  Flag flags;

  sf::Vertex* quad;
};

#endif /*TILE*/
