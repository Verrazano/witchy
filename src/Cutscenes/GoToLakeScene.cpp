#include "GoToLakeScene.h"
#include "../Game.h"
#include "../Entities/Player/Player.h"
#include "../Entities/NPC/Character.h"
#include "../Entities/NPC/NPC.h"

GoToLakeScene::GoToLakeScene(Game* game) : Cutscene(game)
{
  ticks = 0;
  player = NULL;
  controller = NULL;
  vivian = NULL;

  state = State::StartFadeOut;
  movevivian = false;

}

void GoToLakeScene::init()
{
  player = game->getPlayer();
  Controller::captureInput = false;
  controller = &((Player*)player)->c;

  std::vector<Entity*> entity = game->getEntitiesInRadius(player->getPosition(), 32.0f, Game::withName({"npc_Vivian"}));

  sf::Vector2f vpos = entity[0]->getPosition();
  entity[0]->kill();

  vivian = new Character(game, vpos, "res/npcs/vivian.png");
  game->addEntity(vivian);

}

bool GoToLakeScene::update()
{
  if(state == State::StartFadeOut)
  {
    if(fadeOut())
    {
      state = State::StartFadeIn;
      player->setPosition(sf::Vector2f(17*16, 15*16));

    }

  }
  else if(state == State::StartFadeIn)
  {
    if(fadeIn())
    {
      state = State::WalkToLake1;
      ticks = 0;
    }
  }
  else if(state == State::WalkToLake1)
  {
    if(ticks <= 32)
    {
      vivian->walk(sf::Vector2f(1, 0));
    }
    else
    {
      vivian->walk(sf::Vector2f(0, -1));

    }

    if(ticks > 6)
    {
      controller->press("right");
      if(ticks == 32)
      {
        state = State::WalkToLake2;
        ticks = 0;
      }

    }

  }
  else if(state == State::WalkToLake2)
  {
    if(vivian != NULL)
    {
      if(ticks < 194)
      {
        vivian->walk(sf::Vector2f(0, -1));
      }
      else
      {
        vivian->setFacing(Character::Facing::Down);
        sf::Vector2f vpos = vivian->getPosition();
        vivian->kill();
        vivian = NULL;

        game->addEntity(new NPC(game, vpos, "vivian_lake"));

      }
    }

    if(!movevivian && game->map.path.find("irislake") != std::string::npos)
    {
      movevivian = true;
      vivian->setPosition(vivian->getPosition() + sf::Vector2f(0, -32));

    }

    controller->press("up");
    if(ticks == 212)
    {
      controller->press("action1");
      return true;

    }

  }

  ticks++;

  return false;

}

void GoToLakeScene::cleanup()
{
  Controller::captureInput = true;

}
