#include "ScriptedScene.h"
#include "../Game.h"
#include "../Entities/Player/Player.h"
#include <fstream>
#include <sstream>

ScriptedScene::ScriptedScene(Game* game, std::string str, bool path) : Cutscene(game)
{
  player = NULL;
  controller = NULL;

  lineNum = 0;
  state = State::None;

  if(path)
  {
    std::fstream f;
    f.open(str.c_str());

    std::string line;
    while(std::getline(f, line))
    {
      lines.push_back(line);

    }

  }
  else
  {
    std::istringstream ss(str);

    std::string line;
    while(std::getline(ss, line))
    {
      lines.push_back(line);

    }

  }

}

void ScriptedScene::init()
{
  player = game->getPlayer();
  controller = &((Player*)player)->c;
  Controller::captureInput = false;

}

bool ScriptedScene::update()
{
  if(state == State::None)
  {
    if(lineNum == lines.size())
    {
      return true;

    }
    else
    {
      std::string line = lines[lineNum];
      lineNum++;

      std::string op;
      std::istringstream ss(line);
      ss >> op;

      if(op == "input")
      {
        int ticks = 0;
        ss >> inputName >> ticks;
        inputTick = game->ticks + ticks;
        state = State::Input;

      }

    }

  }

  if(state == State::Input)
  {
    controller->press(inputName);
    if(game->ticks == inputTick)
    {
      state = State::None;

    }

  }

  return false;

}

void ScriptedScene::cleanup()
{
  Controller::captureInput = true;
}
