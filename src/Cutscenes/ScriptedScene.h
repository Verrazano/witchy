#ifndef SCRIPTEDSCENE_H_
#define SCRIPTEDSCENE_H_

#include "../Cutscene.h"
#include <string>
#include <vector>

class Controller;
class Entity;

class ScriptedScene : public Cutscene
{
public:
  ScriptedScene(Game* game,
    std::string str, bool path = false);

  void init();

  bool update();

  void cleanup();

  enum class State
  {
    None,
    Input

  };

  State state;

  std::string inputName;
  int inputTick;

  Entity* player;
  Controller* controller;

  int lineNum;
  std::vector<std::string> lines;

};

#endif /*SCRIPTEDSCENE*/
