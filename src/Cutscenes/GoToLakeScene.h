#ifndef GOTOLAKESCENE_H_
#define GOTOLAKESCENE_H_

#include "../Cutscene.h"

class Controller;
class Entity;
class Character;

class GoToLakeScene : public Cutscene
{
public:
  GoToLakeScene(Game* game);

  void init();

  bool update();

  void cleanup();

  enum class State
  {
    StartFadeOut,
    StartFadeIn,
    WalkToLake1,
    WalkToLake2

  };

  State state;

  Entity* player;
  Controller* controller;
  Character* vivian;
  bool movevivian;

  int ticks;

};

#endif /*COOKINGSCENE*/
