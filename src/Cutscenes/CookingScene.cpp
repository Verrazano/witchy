#include "CookingScene.h"
#include "../Game.h"
#include "../Entities/Player/Player.h"

CookingScene::CookingScene(Game* game) : Cutscene(game)
{
  ticks = 0;
  player = NULL;
  controller = NULL;
  state = State::WalkChest1;
}

void CookingScene::init()
{
  player = game->getPlayer();
  Controller::captureInput = false;
  controller = &((Player*)player)->c;

}

bool CookingScene::update()
{
  if(state == State::WalkChest1)
  {
    controller->press("right");
    if(ticks == 15)
    {
      ticks = 0;
      state = State::WalkChest2;

    }

  }
  else if(state == State::WalkChest2)
  {
    controller->press("up");
    if(ticks == 20)
    {
      return true;

    }

  }

  ticks++;
  return false;

}

void CookingScene::cleanup()
{
  Controller::captureInput = true;
}
