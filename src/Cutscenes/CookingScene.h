#ifndef COOKINGSCENE_H_
#define COOKINGSCENE_H_

#include "../Cutscene.h"

class Controller;
class Entity;

class CookingScene : public Cutscene
{
public:
  CookingScene(Game* game);

  void init();

  bool update();

  void cleanup();

  enum class State
  {
    WalkChest1,
    WalkChest2

  };

  State state;

  Entity* player;
  Controller* controller;

  int ticks;

};

#endif /*COOKINGSCENE*/
