#ifndef CUTSCENE_H_
#define CUTSCENE_H_

#include <SFML/Graphics.hpp>

class Game;

class Cutscene
{
public:
  Cutscene(Game* game);

  virtual void init();

  virtual bool update();

  virtual void cleanup();

  bool inited;
  Game* game;

protected:
  bool fadeOut();
  bool fadeIn();

  sf::RectangleShape blackout;

};

#endif /*CUTSCENE*/
