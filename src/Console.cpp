#include "Console.h"
#include "imgui/imgui.h"
#include "imgui/imgui-SFML.h"
#include "Util/Strings.h"

Console::Console()
{
}

Console::Console::Console(sf::Vector2f size)
{
  oss = new std::ostringstream;
  //std::cout.rdbuf(oss->rdbuf());
  this->size = size;

  consoleOpen = false;

  buffer = new char[256];
  strcpy(buffer, "");
}

Console::~Console()
{
}

void Console::update()
{
  //getting back stuff
  std::string str = oss->str();
  if (!str.empty())
  {
    lines.push_back(trim(str));
  }
  oss->str("");
  oss->clear();

  ImGui::Begin("Console", &consoleOpen, size);

  ImGui::BeginChild("output", (size - sf::Vector2f(0, 60)));
  for(int i = 0; i < lines.size(); i++)
  {
    ImGui::Text(lines[i].c_str());

  }

  ImGui::EndChild();

  if(ImGui::InputText("", buffer, 256, ImGuiInputTextFlags_EnterReturnsTrue))
  {
    std::string line(buffer);
    if(line != "")
    {
      std::cout << line << "\n";
      strcpy(buffer, "");

    }

  }

  ImGui::End();


}
